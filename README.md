# General Purpose Entity Component System (GPECS)
![](https://tokei.rs/b1/gitlab/BGluth/pipelined-entity-component-system)

**NOTE:**

- **GPECS is a work in progress, and at present is not yet functional (currently do not have time to work on this, but will finish when I get a chance)**
- **Documentation is currently sparse, but will improve well before the release**

## Overview

GPECS is an experimental [ECS](https://www.gamedev.net/articles/programming/general-and-gameplay-programming/understanding-component-entity-systems-r3013/) with a strong focus on two aspects:

- Generic enough to be applicable outside of games/simulations
- High performance

##### Generic-ness

GPECS is "experimental" only in the sense of it being general purpose. While ECSs are known to be a good fit for games/simulations in general, their use outside of these areas has not really been explored very much (yet!). While there have been some attempts to incorporate ECS-like concepts into other domains of software (ie. [GUIs](https://raphlinus.github.io/personal/2018/05/08/ecs-ui.html)), it's still a very rare occurrence.

This framework makes **no** assumptions of when systems should be scheduled to run. For example, almost all ECS implementations treat their systems as part of one or more update loops (ie. schedule all systems to run every 1/60th of a second). This however doesn't fit well with applications that don't have an inherent update cycle. GPECS instead requires you to define the conditions that need to be met *before* systems are able to be scheduled. For example, you may want system `A` to only get scheduled on user mouse clicks and then have multiple systems downstream that run when `A` outputs some data (like a pipeline).

Despite this generic-ness focus, GPECS should still be well suited for games/simulations.

##### High Performance

GPECS attempts to optimize runtime performance mainly through the following:

- Guaranteeing that the next component a system needs is (almost always) adjacent to the previous component in memory (takes advantage of the prefetcher to minimize CPU cache misses)
- Concurrently running multiple systems on n threads
- Allowing n threads to process SDTs (see below) from a single system concurrently
- Implementing system iteration such that no concurrency mechanisms are needed once a system is scheduled

It's also important to note that system iteration performance is the primary target of optimization. For example, in order to achieve the first bullet above, component inserts/deletes are somewhat more expensive compared to most ECSs as a trade-off. This is ok, however, as inserts/deletes can be considered uncommon events in many use cases.

### Key Structural Differences from Other ECSs

As a result of the focus on performance and general applicability, there are some notable structural differences from the traditional ECS setup:

##### More Than Just Components

It was realized early on that components alone have trouble representing many program designs. Instead of components, there are three types of SDTs (System Data Type):

- Components --> Identical to traditional ECS implementations
- ISDs (Intermediate System Data) --> Serve as a way for handling "events" on entities (ie. physics collisions)
- Tags --> Like components except have no fields

##### Full Control Over System Run Conditions

As mentioned above, in order to remain flexible for varying application designs, it's up to the user to define the conditions that trigger a system to run. An API is provided where whatever logic is needed can be defined as a run condition.

##### Automatic Efficient Cache Line Utilization

Components are stored in large contiguous blocks of memory. They are also ordered such that when iterated over, the component at the next element in the array is the next one to be processed. In other words, systems are not randomly jumping all over memory during iteration when iterating over just components. There are two exceptions to this however:

- A combination boundary is hit during iteration (very rare)
- The SDTs that a system iterates over contains an ISD

##### Boilerplate Code Handled With YAML Config

There are a fair amount of registration calls needed to hook types/systems up to GPECS, likely more than most ECSs. However, Rust's syntax extensions are used to read in a configuration file at compile time to generate these setup calls automatically. As a bonus from moving work to compile time, a few other optimizations become available, such as baking SDT ids into call sites instead of looking them up at runtime.

### Other Cool ECSs

- [SPECS](https://github.com/amethyst/specs)
- [Unity's ECS](https://docs.unity3d.com/Packages/com.unity.entities@0.14/manual/index.html)

## License

Dual licensed under either:

- [Apache License, Version 2.0](LICENSE-Apache)
- [MIT license](LICENSE-MIT)

at your option.

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in time by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.