extern crate yaml_rust;
use self::yaml_rust::{Yaml, YamlLoader};

use std::fs::File;
use std::io::Read;
use std::error::Error;

pub fn load_in_yaml(path: &str) -> Result<Yaml, String>
{
	let mut yaml_file = File::open(path)
		.map_err(|err| format!("Failed to open the PECS registered data yaml file ({}) with the error \"{}\".", path, err))?;
	
	let mut yaml_str = String::new();
	yaml_file.read_to_string(&mut yaml_str)
		.map_err(|err| format!("Failed to read the registered data yaml file ({}) into a string with the error \"{}\"", path, err))?;
	
	let mut registered_data_yaml = YamlLoader::load_from_str(&yaml_str)
		.map_err(|err| format!("Failed to parse the yaml file ({}) with the error \"{:?}|\"", path, err))?;
	
	Ok(registered_data_yaml.pop().unwrap())
}