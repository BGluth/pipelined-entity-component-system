use std::cell::UnsafeCell;
use std::collections::HashMap;

use crate::types::{SectId, SectIdx, CompItemSectIdAndIdx};
use crate::specialized_collections::system_sdt_containers::{ComponentContainerFuncs, ComponentContainerInitDataProvider, CompContainerInitData, SdtItemIterator, SdtItemMutIterator, SdtSectPtrAndLen};
use crate::specialized_collections::system_sdt_containers::{ComponentContainerAddNewSysCombFunc, ComponentContainerDeleteCompFunc, ComponentContainerGetIterFunc, ComponentContainerGetMutIterFunc, ComponentContainerInsertCompFunc, ComponentContainerMoveSdtToNewSectFunc};

pub struct CompLinAccSectContainers<T>
{
    sects: Vec<CompSectContainer<T>>,
    sect_id_to_sect_idx: HashMap<SectId, SectIdx>,
    sect_ids_to_sect_idxs: HashMap<Box<[SectId]>, Vec<SectIdx>>,
}

impl<T> CompLinAccSectContainers<T>
{
    fn new() -> CompLinAccSectContainers<T>
    {
        CompLinAccSectContainers
        {
            sects: Vec::new(),
            sect_id_to_sect_idx: HashMap::new(),
            sect_ids_to_sect_idxs: HashMap::new(),
        }
    }

    pub fn lin_acc_cont_get_iter_for_sect_ids(cont: &CompLinAccSectContainers<T>, comb_sect_ids: &[SectId]) -> SdtItemIterator<T>
    {
        SdtItemIterator::new(cont.get_iter_ptr_info_for_sect_ids(comb_sect_ids))
    }

    pub fn lin_acc_cont_get_mut_iter_for_sect_ids(cont: &CompLinAccSectContainers<T>, comb_sect_ids: &[SectId]) -> SdtItemMutIterator<T>
    {
        SdtItemMutIterator::new(cont.get_iter_ptr_info_for_sect_ids(comb_sect_ids))
    }

    pub fn lin_acc_cont_move_sdt_to_new_sect(cont: &CompLinAccSectContainers<T>, old_pos: CompItemSectIdAndIdx, new_sect: SectId)
    {
        let old_idx = cont.sect_id_to_sect_idx[&old_pos.sect_id];
        let new_idx = cont.sect_id_to_sect_idx[&new_sect];

        let item_to_move = cont.sects[old_idx].del_comp(old_pos.item_idx);
        cont.sects[new_idx].insert_comp(item_to_move);
    }

    pub fn lin_acc_cont_insert_comp(cont: &CompLinAccSectContainers<T>, sect_id: SectId, comp: T)
    {
        let sect_idx = cont.sect_id_to_sect_idx[&sect_id];
        cont.sects[sect_idx].insert_comp(comp);
    }

    pub fn lin_acc_cont_delete_comp(cont: &CompLinAccSectContainers<T>, loc_info: CompItemSectIdAndIdx)
    {
        let sect_idx = cont.sect_id_to_sect_idx[&loc_info.sect_id];
        cont.sects[sect_idx].del_comp(loc_info.item_idx);
    }

    pub fn lin_acc_cont_add_new_sys_comb_sects(cont: &mut CompLinAccSectContainers<T>, sys_comb_sects: Box<[SectId]>)
    {
        cont.create_sects_for_any_new_sects_in_sys_comb_sects(&sys_comb_sects);

        let sys_comb_sect_idxs = cont.get_sect_idxs_for_comb_sect_ids(&sys_comb_sects);
        cont.sect_ids_to_sect_idxs.insert(sys_comb_sects, sys_comb_sect_idxs);
    }

    fn get_iter_ptr_info_for_sect_ids(&self, comb_sect_ids: &[SectId]) -> Box<[SdtSectPtrAndLen<T>]>
    {
        let sect_idxs = &self.sect_ids_to_sect_idxs[comb_sect_ids];
        let sect_ptr_info = sect_idxs.into_iter().map(|sect_idx| self.sects[*sect_idx].get_iter_ptr_info()).filter_map(|s_info| s_info).collect();

        sect_ptr_info
    }

    fn create_sects_for_any_new_sects_in_sys_comb_sects(&mut self, sys_comb_sects: &Box<[SectId]>)
    {
        let new_sects: Vec<&SectId> = sys_comb_sects.into_iter().filter(|comb| !self.sect_id_to_sect_idx.contains_key(comb)).collect();
        for new_sect_id in new_sects
        {
            let sect_idx = self.sects.len();
            self.sects.push(CompSectContainer::new());
            self.sect_id_to_sect_idx.insert(*new_sect_id, sect_idx);
        }
    }

    fn get_sect_idxs_for_comb_sect_ids(&self, sect_ids: &Box<[SectId]>) -> Vec<SectIdx>
    {
        let mut sect_idxs = Vec::new();
        for sect_id in sect_ids.into_iter()
        {
            let sect_idx = self.sect_id_to_sect_idx[sect_id];
            sect_idxs.push(sect_idx);
        }

        sect_idxs
    }
}

impl<T: 'static> ComponentContainerInitDataProvider for CompLinAccSectContainers<T>
{
    fn provide_cont_init_data() -> CompContainerInitData
    {
        let funcs = ComponentContainerFuncs
        {
            get_iter_for_comb_func: Box::new(Self::lin_acc_cont_get_iter_for_sect_ids as ComponentContainerGetIterFunc<T>),
            get_mut_iter_for_comb_func: Box::new(Self::lin_acc_cont_get_mut_iter_for_sect_ids as ComponentContainerGetMutIterFunc<T>),
            move_sdt_to_new_comb_func: Box::new(Self::lin_acc_cont_move_sdt_to_new_sect as ComponentContainerMoveSdtToNewSectFunc<T>),
            insert_comp_func: Box::new(Self::lin_acc_cont_insert_comp as ComponentContainerInsertCompFunc<T>),
            delete_comp_func: Box::new(Self::lin_acc_cont_delete_comp as ComponentContainerDeleteCompFunc<T>),
            add_new_sys_comb_func: Box::new(Self::lin_acc_cont_add_new_sys_comb_sects as ComponentContainerAddNewSysCombFunc<T>),
        };

        CompContainerInitData
        {
            comp_container: Box::new(Self::new()),
            funcs,
        }
    }
}

struct CompSectContainer<T>
{
    sect: UnsafeCell<Vec<T>>
}

impl<T> CompSectContainer<T>
{
    pub fn new() -> CompSectContainer<T>
    {
        CompSectContainer
        {
            sect: UnsafeCell::new(Vec::new()),
        }
    }

    pub fn insert_comp(&self, comp: T)
    {
        unsafe
        {
            self.sect.get().as_mut().unwrap().push(comp);
        }
    }

    pub fn del_comp(&self, comp_idx: usize) -> T
    {
        unsafe
        {
            self.sect.get().as_mut().unwrap().swap_remove(comp_idx)
        }
    }

    pub fn get_iter_ptr_info(&self) -> Option<SdtSectPtrAndLen<T>>
    {
        let sect_vec_ptr = self.sect.get();
        unsafe
        {
            let num_comps = (*sect_vec_ptr).len();
            if num_comps == 0 { return None; }

            let sect_ptr = (*sect_vec_ptr).as_mut_ptr();

            Some(SdtSectPtrAndLen::new(sect_ptr, num_comps))
        }
    }
}


#[cfg(test)]
mod linear_access_cont_tests
{
    use crate::types::{SectId, CompItemSectIdAndIdx};
    use crate::specialized_collections::comp_lin_acc_sect_containers::{CompLinAccSectContainers};
    use crate::specialized_collections::system_sdt_containers::{ComponentContainerInitDataProvider, CompContainerInitData, SdtItemIterator, SdtItemMutIterator};

    use crate::specialized_collections::system_sdt_containers::{ComponentContainerAddNewSysCombFunc, ComponentContainerGetIterFunc, ComponentContainerGetMutIterFunc, ComponentContainerInsertCompFunc, ComponentContainerDeleteCompFunc, ComponentContainerMoveSdtToNewSectFunc};

    use std::any::Any;

    type CompType = i32;

    const sect1_id: SectId = 0;
    const sect2_id: SectId = 5;

    const comp1: CompType = 2;
    const comp2: CompType = 4;

    fn setup_and_init_containers(sys_combs: &[&[SectId]]) -> CompContainerInitData
    {
        let mut data = CompLinAccSectContainers::<CompType>::provide_cont_init_data();
        for sys_comb in sys_combs
        {
            add_sys_comb(&mut data, (*sys_comb).into());
        }

        data
    }

    fn insert_comp(data: &CompContainerInitData, sect_id: SectId, comp: CompType)
    {
        let cont = data.comp_container.downcast_ref::<CompLinAccSectContainers<CompType>>().unwrap();
        let ins_func = data.funcs.insert_comp_func.downcast_ref::<ComponentContainerInsertCompFunc<CompType>>().unwrap();
        (ins_func)(cont, sect_id, comp);
    }

    fn delete_comp(data: &CompContainerInitData, pos_info: CompItemSectIdAndIdx,)
    {
        let cont = data.comp_container.downcast_ref::<CompLinAccSectContainers<CompType>>().unwrap();
        let del_func = data.funcs.delete_comp_func.downcast_ref::<ComponentContainerDeleteCompFunc<CompType>>().unwrap();
        (del_func)(cont, pos_info);
    }

    fn move_comp_to_new_comb(data: &CompContainerInitData, old_pos_info: CompItemSectIdAndIdx, new_sect_id: SectId)
    {
        let cont = data.comp_container.downcast_ref::<CompLinAccSectContainers<CompType>>().unwrap();
        let move_func = data.funcs.move_sdt_to_new_comb_func.downcast_ref::<ComponentContainerMoveSdtToNewSectFunc<CompType>>().unwrap();
        (move_func)(cont, old_pos_info, new_sect_id);
    }

    fn get_comp_iter(data: &CompContainerInitData, sects: &[SectId]) -> SdtItemIterator<CompType>
    {
        let cont = data.comp_container.downcast_ref::<CompLinAccSectContainers<CompType>>().unwrap();
        let get_iter_func = data.funcs.get_iter_for_comb_func.downcast_ref::<ComponentContainerGetIterFunc<CompType>>().unwrap();
        (get_iter_func)(cont, sects)
    }

    fn get_mut_comp_iter(data: &CompContainerInitData, sects: &[SectId]) -> SdtItemMutIterator<CompType>
    {
        let cont = data.comp_container.downcast_ref::<CompLinAccSectContainers<CompType>>().unwrap();
        let get_mut_iter_func = data.funcs.get_mut_iter_for_comb_func.downcast_ref::<ComponentContainerGetMutIterFunc<CompType>>().unwrap();
        (get_mut_iter_func)(cont, sects)
    }

    fn add_sys_comb(data: &mut CompContainerInitData, comb_sects: Box<[SectId]>)
    {
        let cont = data.comp_container.downcast_mut::<CompLinAccSectContainers<CompType>>().unwrap();
        let add_comb_func = data.funcs.add_new_sys_comb_func.downcast_ref::<ComponentContainerAddNewSysCombFunc<CompType>>().unwrap();
        (add_comb_func)(cont, comb_sects)
    }

    fn comps_appear_in_both_iter_types(data: &CompContainerInitData, sects: &[SectId], comps: &[CompType]) -> bool
    {
        let immut_iter = get_comp_iter(data, sects).map(|x| *x);
        let mut_iter = get_mut_comp_iter(data, sects).map(|x| *x);

        comp_iter_contains_elems(immut_iter, comps) && comp_iter_contains_elems(mut_iter, comps)
    }

    fn comp_iter_contains_elems<I>(actual_comp_iter: I, expected_comps: &[CompType]) -> bool
        where I: Iterator<Item=CompType>
    {     
        let actual_comps: Vec<CompType> = actual_comp_iter.into_iter().collect();
        if actual_comps.len() != expected_comps.len() { return false; }

        for comp in actual_comps
        {
            if !expected_comps.contains(&comp)
            {
                return false;
            }
        }

        true
    }

    #[test]
    fn an_item_inserted_should_appear_in_the_iterators()
    {
        let data = setup_and_init_containers(&[&[sect1_id]]);
        insert_comp(&data, sect1_id, comp1);

        assert!(comps_appear_in_both_iter_types(&data, &[sect1_id], &[comp1]));
    }

    #[test]
    fn a_deleted_item_should_not_appear_in_any_iterators()
    {
        let data = setup_and_init_containers(&[&[sect1_id]]);
        insert_comp(&data, sect1_id, comp1);
        delete_comp(&data, CompItemSectIdAndIdx::new(sect1_id, 0));

        assert!(!comps_appear_in_both_iter_types(&data, &[sect1_id], &[comp1]));
    }

    #[test]
    fn iterating_over_items_in_multiple_sects_should_include_all_sects()
    {
        let data = setup_and_init_containers(&[&[sect1_id, sect2_id]]);
        insert_comp(&data, sect1_id, comp1);
        insert_comp(&data, sect2_id, comp2);

        assert!(comps_appear_in_both_iter_types(&data, &[sect1_id, sect2_id], &[comp1, comp2]));
    }

    #[test]
    fn moving_eid_to_new_sect_should_cause_the_eid_to_not_appear_in_old_sect()
    {
        let data = setup_and_init_containers(&[&[sect1_id], &[sect1_id, sect2_id]]);
        insert_comp_and_move_to_other_sect(&data, CompItemSectIdAndIdx::new(sect1_id, 0), sect2_id, comp1);
        assert!(!comps_appear_in_both_iter_types(&data, &[sect1_id], &[comp1]));
    }

    #[test]
    fn moving_eid_to_new_sect_should_cause_the_eid_to_appear_in_new_sect()
    {
        let data = setup_and_init_containers(&[&[sect2_id], &[sect1_id, sect2_id]]);
        insert_comp_and_move_to_other_sect(&data, CompItemSectIdAndIdx::new(sect1_id, 0), sect2_id, comp1);
        assert!(comps_appear_in_both_iter_types(&data, &[sect2_id], &[comp1]));
    }

    fn insert_comp_and_move_to_other_sect(data: &CompContainerInitData, init_sect_and_pos: CompItemSectIdAndIdx, final_sect: SectId, comp: CompType)
    {
        insert_comp(&data, init_sect_and_pos.sect_id, comp);
        move_comp_to_new_comb(&data, init_sect_and_pos, final_sect);
    }
}