use crate::types::{SectId, CompItemSectIdAndIdx, SectIdx};
use crate::specialized_collections::comp_lin_acc_sect_containers::CompLinAccSectContainers;

use std::any::Any;

pub type CompContainerType<T> = CompLinAccSectContainers<T>; // Todo: Hardcode type for now. Change later...
pub type ComponentContainerInitFunc = Box<dyn Fn() -> CompContainerInitData>;

pub type ComponentContainerInsertCompFunc<T> = fn(&CompContainerType<T>, SectId, T);
pub type ComponentContainerDeleteCompFunc<T> = fn(&CompContainerType<T>, CompItemSectIdAndIdx);
pub type ComponentContainerMoveSdtToNewSectFunc<T> = fn(&CompContainerType<T>, CompItemSectIdAndIdx, SectId);
pub type ComponentContainerGetIterFunc<T> = fn(&CompContainerType<T>, &[SectId]) -> SdtItemIterator<T>;
pub type ComponentContainerGetMutIterFunc<T> = fn(&CompContainerType<T>, &[SectId]) -> SdtItemMutIterator<T>;
pub type ComponentContainerAddNewSysCombFunc<T> = fn(&mut CompLinAccSectContainers<T>, Box<[SectId]>);

// We need a concrete iterator type unfortunately...
pub struct SdtItemIterator<T>
{
    iter_state: SdtIterCommon<T>,
}

impl<T> SdtItemIterator<T>
{
    pub fn new(sects_info: Box<[SdtSectPtrAndLen<T>]>) -> SdtItemIterator<T>
    {
        SdtItemIterator
        {
            iter_state: SdtIterCommon::new(sects_info),
        }
    }
}

impl<T: 'static> Iterator for SdtItemIterator<T>
{
    type Item = &'static T;
    
    fn next(&mut self) -> Option<Self::Item>
    {
        if self.iter_state.curr_sect_idx >= self.iter_state.sects_info.len() { return None }
        let sect_info = &self.iter_state.sects_info[self.iter_state.curr_sect_idx];

        let res;
        unsafe
        {
            let sdt = sect_info.sect_ptr.add(self.iter_state.curr_comp_idx).as_ref().unwrap();
            res = Some(sdt);
        }

        self.iter_state.advance_iter_state();
        res
    }
}

pub struct SdtItemMutIterator<T>
{
    iter_state: SdtIterCommon<T>,
}

impl<T> SdtItemMutIterator<T>
{
    pub fn new(sects_info: Box<[SdtSectPtrAndLen<T>]>) -> SdtItemMutIterator<T>
    {
        SdtItemMutIterator
        {
            iter_state: SdtIterCommon::new(sects_info),
        }
    }
}

impl<T: 'static> Iterator for SdtItemMutIterator<T>
{
    type Item = &'static mut T;
    
    fn next(&mut self) -> Option<Self::Item>
    {
        if self.iter_state.curr_sect_idx >= self.iter_state.sects_info.len() { return None }
        let sect_info = &self.iter_state.sects_info[self.iter_state.curr_comp_idx];

        let res;
        unsafe
        {
            let sdt = sect_info.sect_ptr.add(self.iter_state.curr_comp_idx).as_mut().unwrap();
            res = Some(sdt);
        }

        self.iter_state.advance_iter_state();
        res
    }
}

pub struct SdtSectPtrAndLen<T>
{
    sect_ptr: *mut T,
    num_elems: usize,
}

impl<T> SdtSectPtrAndLen<T>
{
    pub fn new(sect_ptr: *mut T, num_elems: usize) -> SdtSectPtrAndLen<T>
    {
        SdtSectPtrAndLen
        {
            sect_ptr,
            num_elems
        }
    }
}

struct SdtIterCommon<T>
{
    sects_info: Box<[SdtSectPtrAndLen<T>]>,
    curr_sect_idx: SectIdx,
    curr_comp_idx: usize,
}

impl<T> SdtIterCommon<T>
{
    fn new(sects_info: Box<[SdtSectPtrAndLen<T>]>) -> SdtIterCommon<T>
    {
        SdtIterCommon
        {
            sects_info,
            curr_sect_idx: 0,
            curr_comp_idx: 0,
        }
    }

    fn advance_iter_state(&mut self)
    {
        self.curr_comp_idx += 1;
        if self.curr_comp_idx >= self.sects_info[self.curr_sect_idx].num_elems
        {
            self.curr_comp_idx = 0;
            self.curr_sect_idx += 1;
        }
    }
}

pub struct CompContainerInitData
{
    pub comp_container: Box<dyn Any>,
    pub funcs: ComponentContainerFuncs,
}

pub struct ComponentContainerFuncs
{
    pub get_iter_for_comb_func: Box<dyn Any>,
    pub get_mut_iter_for_comb_func: Box<dyn Any>,
    pub move_sdt_to_new_comb_func: Box<dyn Any>,
    pub insert_comp_func: Box<dyn Any>,
    pub delete_comp_func: Box<dyn Any>,
    pub add_new_sys_comb_func: Box<dyn Any>,
}

pub trait ComponentContainerInitDataProvider
{
    fn provide_cont_init_data() -> CompContainerInitData;
}