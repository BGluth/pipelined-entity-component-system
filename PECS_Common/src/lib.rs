#![feature(specialization)]

extern crate quote;
extern crate proc_macro2;

pub mod specialized_collections;

pub mod util_functions;
pub mod types;