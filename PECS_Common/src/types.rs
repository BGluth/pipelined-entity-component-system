use std::ops::Range;
use std::marker::PhantomData;

use proc_macro2::{TokenTree, TokenNode, Span, Term};
use quote::{Tokens, ToTokens};

pub type EID = usize;

pub type SysId = usize;
pub type RunCondId = usize;

pub type SdtId = usize;
pub type CombId = usize;
pub type CollId = usize;

pub type CompId = usize;
pub type IsdId = usize;
pub type TagId = usize;

pub type SectId = usize;
pub type SectIdx = usize;
pub type SectItemIdx = usize;

pub fn convert_type_to_tokens(type_str: &str) -> TokenTree
{
	TokenTree
	{
		span: Span::def_site(),
		kind: TokenNode::Term(Term::intern(type_str))
	}
}

#[derive(PartialEq, Clone, Debug)]
pub enum SdtType
{
    Component,
    Isd,
    Tag
}

pub enum SysSdtMutability
{
    Immutable,
    Mutable,
}

impl ToTokens for SdtType
{
	fn to_tokens(&self, tokens: &mut Tokens)
	{
		match self
		{
			&SdtType::Component => tokens.append(convert_type_to_tokens("SdtType::Component")),
			&SdtType::Isd => tokens.append(convert_type_to_tokens("SdtType::Isd")),
			&SdtType::Tag => tokens.append(convert_type_to_tokens("SdtType::Tag"))
		}
	}
}


#[derive(PartialEq, Clone, Debug)]
pub enum SystemType
{
	Both = 0,
	OnlyInput = 1,
	OnlyOutput = 2,
	NoInputOrOutput = 3
}

impl ToTokens for SystemType
{
	fn to_tokens(&self, tokens: &mut Tokens)
	{
		match self
		{
			&SystemType::Both => tokens.append(convert_type_to_tokens("SystemType::Both")),
			&SystemType::OnlyInput => tokens.append(convert_type_to_tokens("SystemType::OnlyInput")),
			&SystemType::OnlyOutput => tokens.append(convert_type_to_tokens("SystemType::OnlyOutput")),
			&SystemType::NoInputOrOutput => tokens.append(convert_type_to_tokens("SystemType::NoInputOrOutput"))
		}
	}
}

#[derive(PartialEq, Eq, Clone, Hash)]
pub enum SystemInputOutput
{
	Input,
	Output
}

impl ToTokens for SystemInputOutput
{
	fn to_tokens(&self, tokens: &mut Tokens)
	{
		match self
		{
			&SystemInputOutput::Input => tokens.append(convert_type_to_tokens("SystemInputOutput::Input")),
			&SystemInputOutput::Output => tokens.append(convert_type_to_tokens("SystemInputOutput::Output"))
		}
	}
}

pub struct RegisteredData<'a>
{
	pub registered_system_data_types: Vec<RegisteredSystemDataType<'a>>,
	pub registered_combinations: Vec<RegisteredCombination<'a>>,
	pub registered_systems: Vec<RegisteredSystem<'a>>,
	pub system_data_type_usage_instances: Vec<SystemDataTypeCombinationUsageInstance>,
	pub system_combination_usage_instances: Vec<SystemCombinationUsageInstance>
}

// One of these is created for each component/ISD
#[derive(Clone, Debug)]
pub struct RegisteredSystemDataType<'a>
{
	pub name: &'a str,
	pub data_type: SdtType,
	pub id: usize
}

#[derive(Clone)]
pub struct RegisteredCombination<'a>
{
	pub name: &'a str,
	pub id: usize
}

#[derive(Clone)]
pub struct RegisteredSystem<'a>
{
	pub name: &'a str,
	pub system_type: SystemType,
	pub id: usize
}

#[derive(Clone, PartialEq)]
pub struct SystemCombinationUsageInstance
{
	pub combination_id: usize,
	pub system_id: usize,
	pub system_input_output: SystemInputOutput,
	pub group_id: usize
}

#[derive(Clone, Debug)]
pub struct SystemDataTypeCombinationUsageInstance
{
	pub system_data_type_id: usize,
	pub combination_id: usize
}

pub struct SystemOrderInfo
{
	pub init_system_ids: Vec<usize>,
	pub system_data_type_orgin_system: Vec<SystemDataTypeSystemId>,
	pub system_output_and_intake_system_vec: Vec<SysDataTypeOutputAndIntakeSystemInstance>
}

pub struct SystemDataTypeSystemId
{
	pub system_data_type_id: usize,
	pub system_id: usize
}

pub struct SysDataTypeOutputAndIntakeSystemInstance
{
	pub output_system_id: usize,
	pub intake_system_id: usize,
	pub system_data_type_id: usize
}

#[derive(Debug)]
pub struct RunTaskWorkRequest
{
	num_items_requested: usize,
	start_idx: usize
}

impl RunTaskWorkRequest
{
	pub fn new(num_items_requested: usize, start_idx: usize) -> RunTaskWorkRequest
	{
		RunTaskWorkRequest
		{
			num_items_requested,
			start_idx
		}
	}

	pub fn get_idx_range_and_update_rqst(self) -> Range<usize>
	{
		Range
		{
			start: self.start_idx,
			end: self.start_idx + self.num_items_requested
		}
	}
}

#[derive(Clone, Copy, Debug)]
pub struct CompItemSectIdAndIdx
{
    pub sect_id: SectId,
    pub item_idx: usize
}

impl CompItemSectIdAndIdx
{
    pub fn new(sect_id: SectId, item_idx: usize) -> CompItemSectIdAndIdx
    {
        CompItemSectIdAndIdx
        {
            sect_id: sect_id,
            item_idx,
        }
    }
}

pub trait RegisteredSDT: 'static
{
	fn get_sdt_id() -> usize;
	fn get_container_type_id() -> usize;
	fn get_sdt_type() -> SdtType;
}

impl<T: 'static> RegisteredSDT for T
{
	default fn get_sdt_id() -> usize { panic!("Can't get the SDT id for a type that is not registered as an SDT!"); }
	default fn get_container_type_id() -> usize { panic!("Can't get the container type id for a type that is not registered as an SDT!"); }
	default fn get_sdt_type() -> SdtType { panic!("Can't get the sdt type for a type that is not registered as an SDT!"); }
}