use alloc::raw_vec::RawVec;
use std::ptr;

// Assuming that T is a POD without a destructor.
pub struct SlotAllocator<T>
{
    buffer: RawVec<T>,
    next_index: usize
}

impl<T> SlotAllocator<T>
{
    pub fn new(starting_size: usize) -> SlotAllocator<T>
    {
        SlotAllocator { buffer: RawVec::with_capacity(starting_size), next_index: 0 }
    }
    
    pub fn add(&mut self, item: T)
    {
        if self.next_index >= self.buffer.cap()
        {
            self.buffer.double();
        }
        
        unsafe
        {
            let writePointer = self.buffer.ptr().offset(self.next_index as isize);
            ptr::write(writePointer, item);
        }
        
        self.next_index += 1;
    }
    
    pub fn remove(&mut self, index: usize)
    {
        // TODO: Do boundary check on debug build!
        // Could easily cause undefined behavior by calling remove when count is zero!!
        unsafe
        {
            let endItemPointer = self.buffer.ptr().offset(self.next_index as isize);
            let removeItemPointer = self.buffer.ptr().offset(index as isize);
            ptr::write(removeItemPointer, ptr::read(endItemPointer));
        }
        
        self.next_index -= 1;
    }
    
    pub fn index(&self, index: usize) -> &T
    {
        unsafe
        {
            let read_value = self.buffer.ptr().offset(index as isize).as_ref().unwrap(); // Can't see how this fail randomly...
            read_value
        }
    }
    
    pub fn mut_index(&mut self, index: usize) -> &mut T
    {
        unsafe
        {
            let read_value = self.buffer.ptr().offset(index as isize).as_mut().unwrap();
            read_value
        }
    }
    
    pub fn clear()
    {
        
    }
    
//  pub fn iter() -> Iterator<Item=T>
//  {
//      
//  }
}