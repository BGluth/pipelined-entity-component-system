use alloc::raw_vec::RawVec;

use std::ptr;

const OUTPUT_BUFFER_STARTING_CAPACITY: usize = 4;

// TODO: Give a better name
/// A very efficent container that systems can write their output into and other systems can read in as their input.
/// Only supports POD types, as old data is not "destroyed" (any destructors are not run) and is instead overwritten.
struct OutputBuffer<T>
{
    items: RawVec<T>,
    last_element_index: usize
}

pub trait OutputBuf<T>
{
    fn clear(&mut self);
    fn length(&self) -> usize;
    fn read_iter(&self) -> ReadIter<T>;
    fn write_iter(&mut self) -> WriteIter<T>;
}

impl<T> OutputBuffer<T>
{
    pub fn new() -> OutputBuffer<T>
    {
        OutputBuffer
        {
            items: RawVec::with_capacity(OUTPUT_BUFFER_STARTING_CAPACITY),
            last_element_index: 0
        }
    }
}

impl<T> OutputBuf<T> for OutputBuffer<T>
{
    fn clear(&mut self)
    {
        self.last_element_index = 0;
    }

    fn length(&self) -> usize
    {
        self.last_element_index
    }

    fn read_iter(&self) -> ReadIter<T>
    {
        ReadIter { buffer: &self.items, next_index: 0, end_index: self.last_element_index }
    }

    fn write_iter(&mut self) -> WriteIter<T>
    {
        WriteIter { buffer: &mut self.items, end_index: &mut self.last_element_index}
    }
}

pub struct ReadIter<'a, T: 'a>
{
    buffer: &'a RawVec<T>,
    next_index: usize,
    end_index: usize
}

impl<'a, T> Iterator for ReadIter<'a, T>
{
    type Item = T;

    fn next(&mut self) -> Option<T>
    {
        let current_index = self.next_index;
        if current_index <= self.end_index
        {
            self.next_index += 1;
            unsafe
            {
                let read_value = ptr::read(self.buffer.ptr().offset(current_index as isize));
                Some(read_value)
            }
        }
        else
        {
            None
        }
    }
}

pub struct WriteIter<'a, T: 'a>
{
    buffer: &'a mut RawVec<T>,
    end_index: &'a mut usize
}

// Need for some necessary runtime polymorphism in registered_systems...
pub trait WriteIterator
{
    type Item;
    fn write(&mut self, item: Self::Item);
}

impl<'a, T> WriteIterator for WriteIter<'a, T>
{
    type Item = T;
    
    fn write(&mut self, item: T)
    {
        let current_index = *self.end_index;
        *self.end_index += 1;

        if *self.end_index == self.buffer.cap()
        {
            self.buffer.double();
        }
        unsafe
        {
            let pointer = self.buffer.ptr().offset(current_index as isize);
            ptr::write(pointer, item);
        }
    }
}



// #[cfg(test)]
// mod tests
// {
//  use super::OutputBuffer;

//  describe! tests_adding_four_items
//  {
//      before_each
//      {
//          let mut buffer = OutputBuffer::new(4)::<i32>;
//          add_four_items(buffer);
//      }

//      it "has a length of 4"
//      {
//          assert!(buffer.length == 4)
//      }

//      it "can read the elements correctly"
//      {
//          let iter = buffer.iter();
//          let x = 0;
//          for item in iter
//          {
//              assert!(item == x);
//              x += 1;
//          }
//      }
//  }

//  fn add_four_items(buffer: OutputBuffer)
//  {
//      for x in 0..3
//      {
//          buffer.write(x);
//      }
//  }
// }
