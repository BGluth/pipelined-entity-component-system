//#![feature(trace_macros)]
#![feature(plugin)]
#![feature(raw_vec_internals)]
#![feature(integer_atomics)]
#![feature(vec_remove_item)]

#[cfg(test)]
extern crate speculate;

extern crate cpu_time;
extern crate log;
extern crate parking_lot;
extern crate pecs_common;
extern crate crossbeam;
extern crate yaml_rust;

//trace_macros!(true);

extern crate alloc;

pub mod pecs_core;

//mod specialized_collections;
//mod slot_allocator;

