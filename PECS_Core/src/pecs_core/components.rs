use specialized_collections;
use pecs_common::types::EID;
use slot_allocator;

use std::collections::HashMap;
use slot_allocator::SlotAllocator;

const COMPONENT_VECTOR_STARTING_CAPACITY: usize = 4;

////static REGISTERED_COMPONENTS: HashMap<String, ComponentVector> = hash_map::
////static REGISTERED_OUTPUTS: HashMap<String, Output
//
////pub fn register_component<T>(name: String)
////{
////    
////}
////
////pub fn register_output<T>(name: String)
////{
////    
////}
//
////struct RegiesteredComponent<T>
////{
////    static index: i32
////}
//
//pub trait ComponentVectorMarker {}
//
pub struct ComponentVector<T>
{
    components: SlotAllocator<T>,
    eid_index: HashMap<EID, usize>
}

impl<T> ComponentVector<T>
{
    pub fn new() -> ComponentVector<T>
    {
        ComponentVector { components: SlotAllocator::new(COMPONENT_VECTOR_STARTING_CAPACITY), eid_index: HashMap::new() }
    }
}

pub trait ComponentVec<T>
{
    fn add(&mut self, eid: EID, initialized_component: T);
    fn remove(&mut self, eid: EID);
    fn index(&self, eid: EID) -> &T;
    fn mut_index(&mut self, eid: EID) -> &mut T;
}

impl<T> ComponentVec<T> for ComponentVector<T>
{
    fn add(&mut self, eid: EID, initialized_component: T)
    {
        
    }
    
    fn remove(&mut self, eid: EID)
    {
        
    }
    
    fn index(&self, eid: EID) -> &T
    {
        unimplemented!()
    }
    
    fn mut_index(&mut self, eid: EID) -> &mut T
    {
        unimplemented!()
    }
}
//
//pub trait IteratorGenerator<T>
//{
//  fn generate_iterator() -> Iterator<Item=T>;
//}
//
////pub trait tagIterable<T>
////{
////    fn getElementsForTag(tagNumber: isize) -> Iterator<T>;
////}
//
//
//

//pub trait InputIteratorGenerator<T>
//{
//  fn generate_iterator() -> Iterator<Item = T>;
//}

