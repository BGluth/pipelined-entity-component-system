#[cfg(test)]
use speculate::speculate;

use std::cmp;
use std::collections::VecDeque;
use std::default::Default;
use std::iter::repeat;
use std::ops::Range;
use std::time::Duration;
use std::sync::atomic::{AtomicBool, Ordering};

use num_traits::int::PrimInt;
use num_traits::cast::FromPrimitive;
use parking_lot::{Condvar, Mutex, WaitTimeoutResult};

pub struct ThreadEvent
{
    cond_var: Condvar,
    fired: Mutex<bool>,
}

impl ThreadEvent
{
    pub fn new(init_state: bool) -> ThreadEvent
    {
        ThreadEvent
        {
            cond_var: Condvar::new(),
            fired: Mutex::new(init_state),
        }
    }

    pub fn clear(&self)
    {
        *self.fired.lock() = false;
    }

    pub fn set(&self)
    {
        *self.fired.lock() = true;
        self.cond_var.notify_all();
    }

    pub fn wait(&self)
    {
        let mut fired = self.fired.lock();
        while !*fired
        {
            self.cond_var.wait(&mut fired);
        }
    }

    pub fn wait_for(&self, dur: Duration) -> WaitTimeoutResult
    {
        let mut fired = self.fired.lock();
        self.cond_var.wait_for(&mut fired, dur)
    }

    pub fn is_set(&self) -> bool
    {
        *self.fired.lock()
    }
}


pub struct RollingAvg
{
    curr_elems: usize,
    max_elems: usize,
    curr_avg: f32
}

impl RollingAvg
{
    pub fn new(max_elems: usize) -> RollingAvg
    {
        RollingAvg
        {
            curr_elems: 0,
            max_elems,
            curr_avg: 0.0
        }
    }

    pub fn add_new_val(&mut self, new_val: f32)
    {
        self.curr_elems = cmp::min(self.max_elems, self.curr_elems + 1);
        self.curr_avg = (self.curr_avg * (self.curr_elems - 1) as f32 + new_val ) / self.curr_elems as f32;
    }

    pub fn get_curr_avg(&self) -> f32
    {
        self.curr_avg
    }

    pub fn clear(&mut self)
    {
        self.curr_elems = 0;
        self.curr_avg = 0.0;
    }
}

pub struct ReuseableIds<T>
    where T: PrimInt + FromPrimitive
{
    next_id: T,
    ids_to_be_recycled: VecDeque<T>
}

impl<T> ReuseableIds<T>
    where T: PrimInt + FromPrimitive
{
    pub fn new() -> ReuseableIds<T>
    {
        ReuseableIds
        {
            next_id: T::zero(),
            ids_to_be_recycled: VecDeque::new()
        }
    }

    pub fn get_next_id(&mut self) -> T
    {
        if !self.ids_to_be_recycled.is_empty()
        {
            return self.ids_to_be_recycled.pop_front().unwrap();
        }

        let id = self.next_id;
        self.next_id.add(T::from_usize(1).unwrap()); // Option feels overkill here...
        id
    }

    pub fn notify_id_unused(&mut self, unused_id: T)
    {
        self.ids_to_be_recycled.push_back(unused_id);
    }

    pub fn next_id_is_recycled(&self) -> bool
    {
        self.ids_to_be_recycled.is_empty()
    }
}

// ... What to call this thing?
pub struct JaggedVec<T>
{
    intern_vecs: Vec<Vec<T>>
}

impl<T> JaggedVec<T>
    where T: Clone // Needed?
{
    pub fn new() -> JaggedVec<T>
    {
        JaggedVec
        {
            intern_vecs: Vec::new()
        }
    }

    pub fn init_intern_vec_size(&mut self, arr_size: usize)
    {
        if self.intern_vecs.len() < arr_size
        {
            self.intern_vecs.resize(arr_size, Vec::new());
        }
    }

    pub fn push(&mut self, val: &[T])
    {
        self.intern_vecs[Self::get_index_for_arr_size(val.len())].extend_from_slice(val);
    }

    pub fn push_default(&mut self, arr_size: usize)
        where T: Default
    {
        let val = repeat(T::default()).take(arr_size);
        self.intern_vecs[Self::get_index_for_arr_size(arr_size)].extend(val);
    }

    pub fn get<'a>(&'a self, arr_size: usize, offset_key: usize) -> &'a [T]
    {
        &self.intern_vecs[Self::get_index_for_arr_size(arr_size)][Self::get_range_for_slice(arr_size, offset_key)]
    }

    pub fn get_mut<'a>(&'a mut self, arr_size: usize, offset_key: usize) -> &'a mut [T]
    {
        &mut self.intern_vecs[Self::get_index_for_arr_size(arr_size)][Self::get_range_for_slice(arr_size, offset_key)]
    }

    pub fn swap_remove(&mut self, arr_size: usize, offset_key: usize)
    {
        let slice_end_idx = (offset_key * arr_size) + arr_size - 1;
        let arr_idx = Self::get_index_for_arr_size(arr_size);
        for i in 0..arr_size
        {
            self.intern_vecs[arr_idx].swap_remove(slice_end_idx - i);
        }
    }

    pub fn len(&self, arr_size: usize) -> usize
    {
        // TODO: Not loving that division...
        self.intern_vecs[Self::get_index_for_arr_size(arr_size)].len() / arr_size
    }

    fn get_range_for_slice(arr_size: usize, offset_key: usize) -> Range<usize>
    {
        let slice_beg = arr_size * offset_key;
        let slice_end = slice_beg + arr_size;
        Range{ start: slice_beg, end: slice_end }
    }

    fn get_index_for_arr_size(arr_size: usize) -> usize
    {
        arr_size - 1
    }
}

#[cfg(test)]
mod jagged_vec_tests
{
    use crate::pecs_core::utils::JaggedVec;

    type ItemType = i32;

    const UNINIT_ARR_SIZE: usize = 4;
    const ARR_SIZE_TO_TEST_WITH: usize = 3;

    const TEST_ITEM_1: [ItemType; ARR_SIZE_TO_TEST_WITH] = [1, 2, 3];
    const TEST_ITEM_2: [ItemType; ARR_SIZE_TO_TEST_WITH] = [4, 5, 6];
    const TEST_ITEM_3: [ItemType; UNINIT_ARR_SIZE] = [7, 8, 9, 10];

    fn init_jagged_vec() -> JaggedVec<ItemType>
    {
        let mut j_vec = JaggedVec::new();
        j_vec.init_intern_vec_size(ARR_SIZE_TO_TEST_WITH);
        j_vec
    }

    fn add_test_item(j_vec: &mut JaggedVec<ItemType>, test_item: &[ItemType]) -> usize
    {
        j_vec.push(&test_item);
        j_vec.len(test_item.len()) - 1
    }

    fn init_vec_and_add_test_item() -> (JaggedVec<ItemType>, usize)
    {
        let mut j_vec = init_jagged_vec();
        let test_item_offset_key = add_test_item(&mut j_vec, &TEST_ITEM_1);
        (j_vec, test_item_offset_key)
    }

    fn init_vec_and_add_two_test_items() -> (JaggedVec<ItemType>, usize, usize)
    {
        let (mut j_vec, item_1_offset_key) = init_vec_and_add_test_item();
        let item_2_offset_key = add_test_item(&mut j_vec, &TEST_ITEM_2);
        (j_vec, item_1_offset_key, item_2_offset_key)
    }

    #[test]
    fn adding_an_item_should_succeed()
    {
        init_vec_and_add_test_item();
        assert!(true);
    }

    #[test]
    fn removing_an_item_should_succeed()
    {
        let (mut j_vec, offset_key) = init_vec_and_add_test_item();
        j_vec.swap_remove(ARR_SIZE_TO_TEST_WITH, offset_key);
        assert!(true);
    }

    #[test]
    fn adding_an_item_should_be_queryable()
    {
        let (j_vec, offset_key) = init_vec_and_add_test_item();
        let item = j_vec.get(ARR_SIZE_TO_TEST_WITH, offset_key);
        assert_eq!(item, TEST_ITEM_1)
    }

    #[test]
    fn an_item_thats_removed_should_no_longer_be_queryable()
    {
        let (mut j_vec, item_1_offset_key, _) = init_vec_and_add_two_test_items();
        j_vec.swap_remove(ARR_SIZE_TO_TEST_WITH, item_1_offset_key);
        let item_at_removed_key = j_vec.get(ARR_SIZE_TO_TEST_WITH, item_1_offset_key);

        assert_ne!(item_at_removed_key, TEST_ITEM_1);
    }

    #[test]
    fn removing_an_item_should_decrement_the_length()
    {
        let (mut j_vec, offset_key) = init_vec_and_add_test_item();
        j_vec.swap_remove(ARR_SIZE_TO_TEST_WITH, offset_key);
        assert_eq!(j_vec.len(ARR_SIZE_TO_TEST_WITH), 0);
    }

    #[test]
    fn swap_removing_an_item_should_replace_the_item_with_the_item_at_the_end_of_the_arr()
    {
        let (mut j_vec, item_1_offset_key, _) = init_vec_and_add_two_test_items();
        j_vec.swap_remove(ARR_SIZE_TO_TEST_WITH, item_1_offset_key);
        let new_item_in_item_1_pos = j_vec.get(ARR_SIZE_TO_TEST_WITH, item_1_offset_key);

        assert_eq!(TEST_ITEM_2, new_item_in_item_1_pos);
    }

    #[test]
    #[should_panic]
    fn querying_an_uninitialized_array_should_panic()
    {
        let j_vec = init_jagged_vec();
        j_vec.get(UNINIT_ARR_SIZE, 0);
    }

    #[test]
    #[should_panic]
    fn removing_an_item_in_an_uninitialized_array_should_panic()
    {
        let mut j_vec = init_jagged_vec();
        j_vec.swap_remove(UNINIT_ARR_SIZE, 0);
    }

    #[test]
    #[should_panic]
    fn adding_an_item_in_to_an_uninitialized_array_should_panic()
    {
        let mut j_vec = init_jagged_vec();
        j_vec.push(&TEST_ITEM_3);
    }
}

#[cfg(test)]
speculate!
{
    extern crate pretty_env_logger;
    use log::trace;

    use std::sync::Arc;
    use std::sync::atomic::{AtomicUsize, Ordering};
    use std::time::Duration;

    const NUM_THREADS: usize = 8;

    const THREAD_COMPLETE_WAIT_MS: u64 = 200;
    const THREAD_START_WAIT_MS: u64 = 50;
    const WAIT_DUR_SLEEP_INT_MS: u64 = 20;

    fn start_threads_and_count_num_completed(num_finished: Arc<AtomicUsize>, evt: Arc<ThreadEvent>, max_wait: u64)
    {
        for i in 0..NUM_THREADS
        {
            let num_finished_clone = num_finished.clone();
            let evt_clone = evt.clone();

            trace!("Spawning thread {}...", i);
            std::thread::spawn(move || run_and_inc_fin_counter(evt_clone, num_finished_clone));
        }

        trace!("Waiting for threads to finish...");
        wait_until_atomic_is_val_or_timeout(num_finished, NUM_THREADS, max_wait);
        trace!("All {} threads finished!", NUM_THREADS);
    }

    fn wait_until_atomic_is_val_or_timeout(val: Arc<AtomicUsize>, trgt_val: usize, timeout: u64) -> bool
    {
        let num_times_to_sleep_for_max_wait = timeout / WAIT_DUR_SLEEP_INT_MS;
        let mut curr_num_sleeps = 0;

        // Gross spin impl because we can't use events
        while val.load(Ordering::Relaxed) != trgt_val
        {
            std::thread::sleep(Duration::from_millis(WAIT_DUR_SLEEP_INT_MS));
            curr_num_sleeps += 1;

            if curr_num_sleeps == num_times_to_sleep_for_max_wait { return false; }
        }

        return true;
    }

    fn run_and_inc_fin_counter(evt: Arc<ThreadEvent>, num_finished: Arc<AtomicUsize>)
    {
        trace!("Thread entered func!");
        evt.wait();
        let prev_val = num_finished.fetch_add(1, Ordering::Relaxed);
        trace!("{} thread(s) finished", prev_val + 1);
    }

    describe "ThreadEvent behaviour"
    {
        before
        {
            // Mutliple tests will init this logger, and calling this more than once will return an error
            let _ = pretty_env_logger::try_init();

            let evt = Arc::new(ThreadEvent::new(false));
            let num_finished = Arc::new(AtomicUsize::new(0));
        }

        describe "setting"
        {
            before
            {
                evt.set();
            }

            it "should not block any threads that wait on it"
            {
                start_threads_and_count_num_completed(num_finished.clone(), evt, THREAD_COMPLETE_WAIT_MS);
                assert_eq!(num_finished.load(Ordering::Relaxed), NUM_THREADS);
            }
        }

        describe "clearing"
        {
            before
            {
                evt.clear();
            }

            it "should block threads that wait on it"
            {
                start_threads_and_count_num_completed(num_finished.clone(), evt, THREAD_COMPLETE_WAIT_MS);
                assert_eq!(num_finished.load(Ordering::Relaxed), 0);
            }

            it "setting should unblock any waiting threads"
            {
                start_threads_and_count_num_completed(num_finished.clone(), evt.clone(), THREAD_START_WAIT_MS);
                assert_eq!(num_finished.load(Ordering::Relaxed), 0);
                evt.set();
                wait_until_atomic_is_val_or_timeout(num_finished.clone(), NUM_THREADS, THREAD_COMPLETE_WAIT_MS);
                assert_eq!(num_finished.load(Ordering::Relaxed), NUM_THREADS);
            }
        }
    }
}


#[cfg(test)]
speculate!
{
    fn compare_avg_between_rolling_average_and_true_average(r_avg: &mut RollingAvg, vals: Vec<f32>)
    {
        let vals_that_should_be_used_in_avg = vals.as_slice().into_iter().rev().take(vals.len());
        let true_avg: f32 = vals_that_should_be_used_in_avg.sum::<f32>() / vals.len() as f32;

        for val in vals.iter()
        {
            r_avg.add_new_val(*val);
        }

        assert_eq!(true_avg, r_avg.get_curr_avg());
    }

    describe "adding values to the average"
    {
        before
        {
            let mut avg = RollingAvg::new(10);
        }

        it "has an average of 0 before we add any values"
        {
            assert_eq!(avg.get_curr_avg(), 0.0);
        }

        it "properly calculates the average of values below the maximum number of elements"
        {
            compare_avg_between_rolling_average_and_true_average(&mut avg, vec![1.0,7.0,5.0,3.0,12.0]);
        }

        it "properly calculates the average of values when we hit maximum number of elements"
        {
            compare_avg_between_rolling_average_and_true_average(&mut avg, vec![5.0,3.0,8.0,2.0,7.0,20.0,14.0,8.0]);
        }
    }
}
