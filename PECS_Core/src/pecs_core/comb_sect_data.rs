use crate::pecs_common::types::{CombId, SdtId, SectId};

use std::collections::HashMap;

pub struct CombSectData
{
    combs_to_sect_ids: HashMap<CombId, Box<[SectId]>>
}

impl CombSectData
{
    pub fn get_sect_ids_for_comb_id(&self, comb_id: CombId) -> &[SectId]
    {
        unimplemented!();
    }
}

pub struct CombSectDataBuilder
{
    sys_combs: Vec<Box<[SdtId]>>,
}

impl CombSectDataBuilder
{
    pub fn new() -> CombSectDataBuilder
    {
        unimplemented!();
    }

    pub fn reg_sys_comb(self, sdt_ids: Box<[SdtId]>) -> CombSectDataBuilder
    {
        unimplemented!();
    }

    pub fn build(self) -> CombSectData
    {
        unimplemented!()
    }
}