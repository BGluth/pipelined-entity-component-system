use std::slice::Iter;
use yaml_rust::Yaml;

use super::run_condition::{RunCondition, RunCondStaticCallbackRegistration};
use crate::pecs_core::scheduler::RcNotificationState;

pub struct UpdateCycle<'a>
{
    sys_ids_to_cycle_id: Vec<usize>,
    cycles: Vec<Cycle>,
    notify_func: Box<dyn Fn(usize) + 'a>
}

impl<'a> UpdateCycle<'a>
{
    // TODO: Replace new function with non-dummy function
    fn new(run_condition_yaml: Yaml, call_back_signup: &'a mut dyn RunCondStaticCallbackRegistration <'a>) -> Box<dyn RunCondition + 'a>
    {
        Box::new(UpdateCycle
        {
            sys_ids_to_cycle_id: Vec::new(),
            cycles: Vec::new(),
            notify_func: call_back_signup.get_static_run_cond_met_callback()
        })
    }

    fn on_system_completed(&mut self, sys_id: usize)
    {
        let cycle_id = self.sys_ids_to_cycle_id[sys_id];
        self.cycles[cycle_id].sys_ids_rem_for_cycle -= 1;

        if self.cycles[cycle_id].sys_ids_rem_for_cycle == 0
        {
            (self.notify_func)(cycle_id);
        }
    }
}

impl<'a> RunCondition for UpdateCycle<'a>
{
    fn reg_task(&mut self, task_id: usize, notif_state: &mut RcNotificationState)
    {
        unimplemented!();
    }

    fn unreg_task(&mut self, task_id: usize)
    {
        unimplemented!();
    }
}

struct Cycle
{
    time_between_cycles: u64,
    time_of_last_cycle_start: u64,
    sys_ids_in_cycle: Vec<usize>,
    sys_ids_rem_for_cycle: usize,
}

impl Cycle
{
    pub fn new() -> Cycle
    {
        unimplemented!();
    }
}
