use yaml_rust::Yaml;
use super::run_condition::{RunCondition, RunCondStaticCallbackRegistration};
use crate::pecs_core::scheduler::RcNotificationState;

pub struct SystemTimer
{
    system_timers: Vec<Timer>,
    notify_sched_that_timer_run_condit_satisfied_func: Box<dyn Fn(usize)>
}

impl SystemTimer
{
    fn new(run_condition_yaml: Yaml, call_back_signup: &mut dyn RunCondStaticCallbackRegistration)
    {
        unimplemented!();
    }

    fn on_system_completed(&mut self, sys_id: usize)
    {
        (self.notify_sched_that_timer_run_condit_satisfied_func)(sys_id);
        self.system_timers[sys_id].restart();
    }
}

impl RunCondition for SystemTimer
{
    fn reg_task(&mut self, task_id: usize, notif_state: &mut RcNotificationState)
    {
        unimplemented!();
    }

    fn unreg_task(&mut self, task_id: usize)
    {
        unimplemented!();
    }
}

struct Timer
{
    timer_length: u64,
    last_fire_time: u64
}

impl Timer
{
    pub fn new() -> Timer
    {
        unimplemented!();
    }

    pub fn restart(&mut self)
    {
        unimplemented!();
    }
}
