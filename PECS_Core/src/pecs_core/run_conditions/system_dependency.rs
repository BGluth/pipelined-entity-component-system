use yaml_rust::Yaml;

use super::run_condition::{RunCondition, RunCondDynamicCallbackRegistration};
use crate::pecs_core::scheduler::RcNotificationState;

pub struct SystemDependency<'a>
{
    system_ids_dependant_on_system: Vec<Vec<usize>>,
    rem_sys_dep_counts: Vec<usize>,
    strting_sys_dep_counts: Vec<usize>,
    notify_run_cond_met_for_task_func: Box<dyn Fn(usize) + 'a>,
    notify_run_cond_unmet_for_task_func: Box<dyn Fn(usize) + 'a>
}

impl<'a> SystemDependency<'a>
{
    fn new(run_condition_yaml: Yaml, call_back_signup: &'a mut dyn RunCondDynamicCallbackRegistration<'a>) -> Box<dyn RunCondition + 'a>
    {
        Box::new(SystemDependency
        {
            system_ids_dependant_on_system: Vec::new(),
            rem_sys_dep_counts: Vec::new(),
            strting_sys_dep_counts: Vec::new(),
            notify_run_cond_met_for_task_func: call_back_signup.get_dyn_run_cond_met_callback(),
            notify_run_cond_unmet_for_task_func: call_back_signup.get_dyn_run_cond_unmet_callback()
        })
    }

    fn on_system_completed(&mut self, sys_id: usize)
    {
        self.update_and_check_rem_dep_counts_for_dep_systems_when_system_completes(sys_id);
        self.reset_sys_dep_counts_for_system(sys_id);
    }

    fn notify_sched_that_sys_is_now_avail(&self, sys_id: usize)
    {
        (self.notify_run_cond_met_for_task_func)(sys_id);
    }

    fn update_and_check_rem_dep_counts_for_dep_systems_when_system_completes(&mut self, completed_sys_id: usize)
    {
        for sys_id in self.system_ids_dependant_on_system[completed_sys_id].iter()
        {
            self.rem_sys_dep_counts[*sys_id] -= 1;
            if self.rem_sys_dep_counts[*sys_id] == 0
            {
                self.notify_sched_that_sys_is_now_avail(*sys_id);
            }
        }
    }

    fn reset_sys_dep_counts_for_system(&mut self, sys_id: usize)
    {
        self.rem_sys_dep_counts[sys_id] = self.strting_sys_dep_counts[sys_id];
    }
}

impl<'a> RunCondition for SystemDependency<'a>
{
    fn reg_task(&mut self, task_id: usize, notif_state: &mut RcNotificationState)
    {
        unimplemented!();
    }

    fn unreg_task(&mut self, task_id: usize)
    {
        unimplemented!();
    }
}
