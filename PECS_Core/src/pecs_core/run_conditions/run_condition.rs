use std::collections::HashMap;
use crate::pecs_core::scheduler::{RcCallback, RcNotificationState};

// Probably could be named better...
pub trait RunCondSchedulerCallbackRegistration<'a>
{
    fn reg_task_scheduled_callback(&mut self);
    fn reg_task_starting_callback(&mut self);
    fn reg_task_completed_callback(&mut self);
}

// TODO: Remove once we port existing Rcs over to new system
pub trait RunCondStaticCallbackRegistration<'a> : RunCondSchedulerCallbackRegistration<'a>
{
    fn get_static_run_cond_met_callback(&'a self) -> Box<dyn Fn(usize) + 'a>;
}

// TODO: Remove once we port existing Rcs over to new system
pub trait RunCondDynamicCallbackRegistration<'a>
{
    // TODO: Once impl Trait becomes usable inside trait impls, switch to that
    fn get_dyn_run_cond_met_callback(&'a self) -> Box<dyn Fn(usize) + 'a>;
    fn get_dyn_run_cond_unmet_callback(&'a self) -> Box<dyn Fn(usize) + 'a>;
}

pub trait RunCondition
{
    fn reg_task(&mut self, task_id: usize, notif_state: &mut RcNotificationState);
    fn unreg_task(&mut self, task_id: usize);

    fn task_scheduled(&mut self, task_id: usize, notif_state: &mut RcNotificationState) { panic_with_rc_notif_not_reged_err("was scheduled"); }
    fn task_starting(&mut self, task_id: usize, notif_state: &mut RcNotificationState) { panic_with_rc_notif_not_reged_err("was starting"); }
    fn task_completed(&mut self, task_id: usize, notif_state: &mut RcNotificationState) { panic_with_rc_notif_not_reged_err("has completed"); }
    fn task_deps_satisfied(&mut self, task_id: usize, notif_state: &mut RcNotificationState) { panic_with_rc_notif_not_reged_err("has it dependencies satisfied"); }
}

// Gross, inheiritance... But I think for now this is the nicest solution
pub trait YamlInitRunCondition : RunCondition
{
    fn load_from_yaml(&mut self, proc_yaml: HashMap<String, String>);
}

fn panic_with_rc_notif_not_reged_err(varying_err_str_bit: &str)
{
    panic!(format!("Sched tried notifying an RC that a task {} that it did not register! This is a GPECs bug!", varying_err_str_bit));
}