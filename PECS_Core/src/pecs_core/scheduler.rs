use std::cmp::{max, Reverse};
use std::error::Error;
use std::fmt;
use std::panic;
use std::thread;
use std::sync::{Arc, Mutex};
use std::sync::atomic::{AtomicBool, AtomicU64, Ordering};
use std::time::{Duration, Instant};
use std::collections::{BinaryHeap, HashMap, VecDeque};

use log::{trace, debug, info, warn, error};

use crossbeam::channel::{Receiver, Sender, TryRecvError, RecvTimeoutError};
use crossbeam::atomic::AtomicCell;
use crossbeam::deque::{Worker, Steal, Stealer};

use cpu_time;

use pecs_common::types::{RunCondId, RunTaskWorkRequest};

use crate::pecs_core::common::{DynamicRunCondNewFunc, RunConditionType, StaticRunCondNewFunc};
use crate::pecs_core::run_conditions::run_condition::{RunCondition, RunCondSchedulerCallbackRegistration};
use crate::pecs_core::utils;

pub type TaskRunFunc = Box<dyn Fn(RunTaskWorkRequest) + 'static + Send + Sync>;
pub type TaskGetNumItemsFunc = Box<dyn Fn() -> usize + 'static + Send + Sync>;
pub type SchedulerResult<T> = Result<T, SchedulerError>;

pub type RcCallback = Box<dyn Fn(usize, RcNotificationState) + Send>;

type TaskTime = u64;

const NUM_TASKS_RUNS_AVG_ITEM_TIMES_TO_OBSERVE: usize = 1000;
const INITIAL_TIME_GUESS_OF_WRKLD_PER_TASK_ITEM: TaskTime = 10000;

// These constants here should probably later be determined at runtime and live in the TaskState struct or something
const EXTRA_EXTRA_WRKLD_THRESHOLD_NS: TaskTime = 1000000; // 1 ms
const SHOULD_TAKE_TASKS_THRESHOLD_NS: TaskTime = 1000000;
const SHOULD_STEAL_IF_T_WRKLD_BELOW_TRGT_RATIO: f32 = 0.5;
const TRGT_WRKLD_OVERSHOOT_THRESHOLD_PERC: f32 = 1.1;
const MIN_THREAD_WRLKD_REQ_TO_STEAL_NS: TaskTime = 1000000;
const MIN_WRKLD_THRESH_TO_ENTER_CRIT_SECT_NS: TaskTime = 100000;

const MAX_CLIENT_WAIT_FOR_TERM_ACK_MS: u64 = 1000;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum SchedulerError
{
    InternalError(SchedulerInternalError),
    LocalError(LocalSchedulerError),
    MultiError(SchedulerInternalError, LocalSchedulerError)
}

impl fmt::Display for SchedulerError
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        match self
        {
            SchedulerError::InternalError(intern_err) => intern_err.fmt(f),
            SchedulerError::LocalError(loc_err) => loc_err.fmt(f),
            SchedulerError::MultiError(intern_err, loc_err) => f.write_fmt(format_args!("intern: {}\n local: {}", intern_err, loc_err))
        }
    }
}
impl Error for SchedulerError {}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum LocalSchedulerError
{
    SchedulerTerminated,
    ChannelRecvError(RecvTimeoutError),
    SchedThreadSpawn
}

impl fmt::Display for LocalSchedulerError
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        match self
        {
            LocalSchedulerError::SchedulerTerminated => f.write_str("Tried performing an action on a scheduler after a termination message was sent!"),
            LocalSchedulerError::ChannelRecvError(recv) => recv.fmt(f),
            LocalSchedulerError::SchedThreadSpawn => f.write_str("An error occurred when spawning the worker threads for the scheduler.")
        }
    }
}
impl Error for LocalSchedulerError {}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum SchedulerInternalError
{
    InvalidTaskId(usize),
    InvalidRunCondId(usize),
    RunCondDupTaskId(usize, usize),
    TaskPanicked(usize),
    ClientChannelClosed
}

impl fmt::Display for SchedulerInternalError
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        match self
        {
            SchedulerInternalError::InvalidRunCondId(run_cond_id) => f.write_fmt(format_args!("Tried adding a task to a run condition that doesn't exist: (Got RunCondId: {})", run_cond_id)),
            SchedulerInternalError::InvalidTaskId(task_id) => f.write_fmt(format_args!("Tried removing a task or registering a task to a run condition that does not exist (Got {})", task_id)),
            SchedulerInternalError::RunCondDupTaskId(run_cond_id, task_id) => f.write_fmt(format_args!("Tried adding a task to a run condition that already exists on the run condition: (Got task_id: {}, run_cond_id: {}", task_id, run_cond_id)),
            SchedulerInternalError::TaskPanicked(task_id) => f.write_fmt(format_args!("Task with id {} panicked! TODO: Somehow get panic msg here...", task_id)),
            SchedulerInternalError::ClientChannelClosed => f.write_str("Scheduler tried sending a message back to the client but the channel was closed!")
        }
    }
}

impl Error for SchedulerInternalError { }

#[derive(Clone, Copy, PartialEq)]
enum SchedulerActiveState
{
    Running,
    Paused,
    Terminated
}

#[derive(Clone, Copy, PartialEq)]
enum TaskStatus
{
    StaticUnmet,
    DynamicUnmet,
    Runnable
}

pub struct TaskRegInfo
{
    pub run_func: TaskRunFunc,
    pub get_num_items_func: TaskGetNumItemsFunc,
}

impl fmt::Debug for TaskRegInfo
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        f.write_str("TaskRegInfo")
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum SchedOutboundMsg
{
    Started,
    Paused,
    Terminated,
    Errored(SchedulerInternalError),
    MsgsProcessed(usize)
}

pub trait TaskRegistration
{
    fn register_task(&mut self, reg_info: TaskRegInfo) -> SchedulerResult<usize>;
    fn unregister_task(&mut self, task_id: usize) -> SchedulerResult<()>;

    fn register_static_run_condition(&mut self, run_cond_new_func: StaticRunCondNewFunc) -> SchedulerResult<RunCondId>;
    fn register_dynamic_run_condition(&mut self, run_cond_new_func: DynamicRunCondNewFunc) -> SchedulerResult<RunCondId>;
    fn register_task_with_run_condition(&mut self, run_cond_id: usize, task_id: usize) -> SchedulerResult<()>;

    fn register_name_for_task(&mut self, task_id: usize, name: String) -> SchedulerResult<()>;
    fn register_name_for_run_condition(&mut self, run_cond_id: usize, name: String) -> SchedulerResult<()>;
}

struct PostCritProcData
{
    prev_avg_t_wrkld: TaskTime
}

impl PostCritProcData
{
    fn new(prev_avg_t_wrkld: TaskTime) -> PostCritProcData
    {
        PostCritProcData
        {
            prev_avg_t_wrkld
        }
    }
}

pub struct RcNotificationState<'a>
{
    notif_msg_queue: &'a mut RunCondCallbackMsgQueues
}

#[derive(Debug)]
enum CritMessage
{
    Client(ClientCritMsg),
    Internal(InternalCritMsg)
}

enum ClientCritMsg
{
    RegisterTask(TaskRegInfo, usize),
    UnregisterTask(usize),
    RegTaskToRunCond(usize, usize),
    RegisterStaticRunCond(StaticRunCondNewFunc),
    RegisterDynamicRunCond(DynamicRunCondNewFunc),
    Unpause,
    Pause,
    Terminate
}

impl fmt::Debug for ClientCritMsg
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        match self
        {
            ClientCritMsg::RegisterTask(_, t_id) => f.write_fmt(format_args!("RegisterTask (task_id: {})", t_id)),
            ClientCritMsg::UnregisterTask(t_id) => f.write_fmt(format_args!("UnregisterTask (task_id: {})", t_id)),
            ClientCritMsg::RegTaskToRunCond(t_id, run_cond_id) => f.write_fmt(format_args!("RegTaskToRunCond (task_id: {}, run_cond_id: {})", t_id, run_cond_id)),
            ClientCritMsg::RegisterStaticRunCond(_) => f.write_str("RegisterStaticRunCond"),
            ClientCritMsg::RegisterDynamicRunCond(_) => f.write_str("RegisterDynamicRunCond"),
            ClientCritMsg::Unpause => f.write_str("Start"),
            ClientCritMsg::Pause => f.write_str("Pause"),
            ClientCritMsg::Terminate => f.write_str("Terminate")
        }
    }
}

#[derive(Debug)]
enum InternalCritMsg
{
    TaskStarted(usize),
    TaskCompleted(usize, TaskTime),
    TaskSplit(usize),
    TaskPanicked(usize),
    AvgTaskItemTimeUpdate(usize, TaskTime),
    ThreadPaused,
    ThreadUnpaused,
    ThreadTerminated
}

#[derive(Debug)]
enum WorkingThreadMsg
{
    TaskAssigned(ThreadTask),
    StateUpdate(WorkingThreadSchedStateUpdate)
}

#[derive(Clone, Copy, Debug)]
enum WorkingThreadSchedStateUpdate
{
    OtherThreadStateUpdate(OtherThreadUpdateInfo),
    Pause,
    Unpause,
    Terminate
}

#[derive(Clone, Copy, Debug)]
enum OtherThreadUpdateInfo
{
    ThreadAliveChange(i32),
    AvgWrkldUpdate(TaskTime)
}

pub struct SchedulerInitInfo
{
    pub num_task_threads: usize
}

#[derive(Clone)]
struct SchedulerUserSharedState
{
    pub intern_error_state: Arc<AtomicCell<Option<SchedulerInternalError>>>
}

impl SchedulerUserSharedState
{
    pub fn new() -> SchedulerUserSharedState
    {
        SchedulerUserSharedState
        {
            intern_error_state: Arc::new(AtomicCell::new(None))
        }
    }
}

struct LocalSchedulerState
{
    is_paused: bool,
    is_terminated: bool,
    local_term_msg_sent: bool,
    local_error: Option<LocalSchedulerError>,
    rem_acks_waiting_on: usize
}

impl LocalSchedulerState
{
    pub fn new() -> LocalSchedulerState
    {
        LocalSchedulerState
        {
            is_paused: false,
            is_terminated: false,
            local_term_msg_sent: false,
            local_error: None,
            rem_acks_waiting_on: 0
        }
    }
}

pub struct Scheduler
{
    sched_outbnd_msgs: Receiver<SchedOutboundMsg>,
    sched_crit_msgs_tx: CritMsgsTx,
    user_sched_shared_state: SchedulerUserSharedState,
    loc_sched_state: LocalSchedulerState,
    next_task_id: usize,
    next_run_cond_id: usize,
    debug_info: SchedulerDebugInfo
}

impl Scheduler
{
    pub fn new(init_info: SchedulerInitInfo) -> SchedulerResult<Scheduler>
    {
        let (client_msgs_tx, client_msgs_rx) = crossbeam::channel::unbounded();
        let user_sched_shared_state = SchedulerUserSharedState::new();

        let (crit_msgs_tx, crit_msgs_rx) = crossbeam::channel::unbounded();
        let mut sched = Scheduler
        {
            sched_outbnd_msgs: client_msgs_rx,
            sched_crit_msgs_tx: CritMsgsTx::new(crit_msgs_tx.clone()),
            user_sched_shared_state: user_sched_shared_state.clone(),
            loc_sched_state: LocalSchedulerState::new(),
            next_task_id: 0,
            next_run_cond_id: 0,
            debug_info: SchedulerDebugInfo::new()
        };

        let chan_init_info = SchedChanInitInfo
        {
            crit_msgs_tx,
            crit_msgs_rx,
            client_msgs_tx
        };

        // Send a pause message to prevent the sched from starting right away
        sched.pause().unwrap(); // If this fails we want a panic.
        thread::spawn(move || start_sched_main_thread(init_info, user_sched_shared_state, chan_init_info));

        Ok(sched)
    }

    pub fn start(&mut self) -> SchedulerResult<()>
    {
        self.ensure_term_msg_not_sent_and_no_err_and_update_rem_acks()?;
        self.sched_crit_msgs_tx.send_start_message();
        
        Ok(())
    }

    pub fn pause(&mut self) -> SchedulerResult<()>
    {
        self.ensure_term_msg_not_sent_and_no_err_and_update_rem_acks()?;
        self.sched_crit_msgs_tx.send_pause_message();

        Ok(())
    }

    pub fn terminate(&mut self) -> SchedulerResult<()>
    {
        self.ensure_term_msg_not_sent_and_no_err_and_update_rem_acks()?;
        self.sched_crit_msgs_tx.send_termination_message();
        self.loc_sched_state.local_term_msg_sent = true;
        self.wait_for_specific_event_for_duration(SchedOutboundMsg::Terminated, Duration::from_millis(MAX_CLIENT_WAIT_FOR_TERM_ACK_MS))
    }

    fn ensure_term_msg_not_sent_and_no_err_and_update_rem_acks(&mut self) -> SchedulerResult<()>
    {
        if let Some(err) = self.get_error()
        {
            return Err(err);
        }

        match self.loc_sched_state.local_term_msg_sent
        {
            false => 
            {
                self.loc_sched_state.rem_acks_waiting_on += 1;
                Ok(())
            }
            true => self.set_loc_err_and_return_err_as_result(LocalSchedulerError::SchedulerTerminated)
        }
    }

    fn set_loc_err_and_return_err_as_result<T>(&mut self, err: LocalSchedulerError) -> SchedulerResult<T>
    {
        self.loc_sched_state.local_error = Some(err);
        Err(SchedulerError::LocalError(err))
    }

    pub fn has_errored(&self) -> bool
    {
        self.user_sched_shared_state.intern_error_state.load() != None || self.loc_sched_state.local_error != None
    }

    pub fn is_paused(&self) -> bool
    {
        self.loc_sched_state.is_paused
    }

    pub fn is_terminated(&self) -> bool
    {
        self.loc_sched_state.is_terminated
    }

    pub fn get_error(&self) -> Option<SchedulerError>
    {
        match (self.user_sched_shared_state.intern_error_state.load(), self.loc_sched_state.local_error)
        {
            (Some(intern_err), Some(loc_err)) => Some(SchedulerError::MultiError(intern_err, loc_err)),
            (Some(intern_err), None) => Some(SchedulerError::InternalError(intern_err)),
            (None, Some(loc_err)) => Some(SchedulerError::LocalError(loc_err)),
            (None, None) => None
        }
    }

    pub fn wait_for_event(&mut self) -> SchedulerResult<SchedOutboundMsg>
    {
        // From the crossbeam docs, the only way this can fail is on a disconnect.
        let msg_res = self.sched_outbnd_msgs.recv().map_err(|_| RecvTimeoutError::Disconnected);
        self.handle_any_chan_err_and_handle_recv_msg(msg_res)
    }

    pub fn wait_for_event_for_duration(&mut self, dur: Duration) -> SchedulerResult<SchedOutboundMsg>
    {
        let msg_res = self.sched_outbnd_msgs.recv_timeout(dur);
        self.handle_any_chan_err_and_handle_recv_msg(msg_res)
    }

    pub fn wait_for_specific_event_for_duration(&mut self, evt: SchedOutboundMsg, dur: Duration) -> SchedulerResult<()>
    {
        let time_to_give_up_at = Instant::now() + dur;
        loop
        {
            let recvd_evt = self.wait_for_event_for_time_after_instant(time_to_give_up_at)?;
            if recvd_evt == evt { break; }
        }

        Ok(())
    }

    pub fn wait_for_all_msgs_to_be_processed_for_duration(&mut self, dur: Duration) -> SchedulerResult<()>
    {
        let time_to_give_up_at = Instant::now() + dur;
        trace!("Scheduler waiting on all internal msgs to be processed... ({} remain)", self.loc_sched_state.rem_acks_waiting_on);
        
        while self.loc_sched_state.rem_acks_waiting_on > 0
        {
            self.wait_for_event_for_time_after_instant(time_to_give_up_at)?;
            trace!("Remaining internal msgs waiting on: {}", self.loc_sched_state.rem_acks_waiting_on);
        }
        
        Ok(())
    }

    fn wait_for_event_for_time_after_instant(&mut self, time_to_give_up_at: Instant) -> SchedulerResult<SchedOutboundMsg>
    {
        let max_wait_dur = time_to_give_up_at - Instant::now();
        self.wait_for_event_for_duration(max_wait_dur)
    }

    fn handle_any_chan_err_and_handle_recv_msg(&mut self, msg_res: Result<SchedOutboundMsg, RecvTimeoutError>) -> SchedulerResult<SchedOutboundMsg>
    {
        let msg = self.set_err_state_of_sched_if_err_and_return_err_result(msg_res)?;
        self.handle_recv_msg(msg)
    }

    fn set_err_state_of_sched_if_err_and_return_err_result(&mut self, res: Result<SchedOutboundMsg, RecvTimeoutError>) -> SchedulerResult<SchedOutboundMsg>
    {
        match res
        {
            Ok(val) => Ok(val),
            Err(loc_err) => 
            {
                error!("A local scheduler error has occurred: {}", loc_err);
                self.set_loc_err_and_return_err_as_result(LocalSchedulerError::ChannelRecvError(loc_err))
            }
        }
    }

    fn handle_recv_msg(&mut self, msg: SchedOutboundMsg) -> SchedulerResult<SchedOutboundMsg>
    {
        match msg
        {
            SchedOutboundMsg::Started => self.loc_sched_state.is_paused = false,
            SchedOutboundMsg::Paused => self.loc_sched_state.is_paused = true,
            SchedOutboundMsg::Terminated => self.loc_sched_state.is_terminated = true,
            SchedOutboundMsg::Errored(err) => return Err(SchedulerError::InternalError(err)),
            SchedOutboundMsg::MsgsProcessed(num_msgs) => self.loc_sched_state.rem_acks_waiting_on -= num_msgs
        }

        Ok(msg)
    }

    fn get_next_run_cond_id(&mut self) -> usize
    {
        let run_cond_id = self.next_run_cond_id;
        self.next_run_cond_id += 1;
        run_cond_id
    }
}

impl Drop for Scheduler
{
    fn drop(&mut self)
    {
        if !self.loc_sched_state.local_term_msg_sent && !self.has_errored()
        {
            // It's possible that the sched could error as soon as we enter this conditional, so we need to handle this
            self.terminate().ok();
        }
    }
}

// TODO: Only include in DEBUG mode
struct SchedulerDebugInfo
{
    task_id_to_name_table: HashMap<usize, String>,
    run_cond_id_to_name: HashMap<usize, String>
}

impl SchedulerDebugInfo
{
    pub fn new() -> SchedulerDebugInfo
    {
        SchedulerDebugInfo
        {
            task_id_to_name_table: HashMap::new(),
            run_cond_id_to_name: HashMap::new()
        }
    }
}

struct SchedChanInitInfo
{
    crit_msgs_tx: Sender<CritMessage>,
    crit_msgs_rx: Receiver<CritMessage>,
    client_msgs_tx: Sender<SchedOutboundMsg>
}

fn start_sched_main_thread(init_info: SchedulerInitInfo, user_sched_shared_state: SchedulerUserSharedState, chan_init_info: SchedChanInitInfo) -> SchedulerResult<()>
{
    let num_threads = init_info.num_task_threads;

    let mut loc_tinfo_table = Vec::with_capacity(init_info.num_task_threads);
    let crit_sect_info = CriticalSectionInfo::new(chan_init_info.crit_msgs_rx, init_info.num_task_threads);
    let sched_spec_state = SchedulerSpecificState::new(user_sched_shared_state.intern_error_state.clone());

    let mut priv_tinfo_table = Vec::new();
    for _ in 0..num_threads
    {
        let local_thread_queue =crossbeam::deque::Worker::new_lifo();
        let (t_loc_tx, t_loc_rx) = crossbeam::channel::unbounded();

        loc_tinfo_table.push(LocalThreadInfo::new(t_loc_tx, local_thread_queue.stealer()));
        priv_tinfo_table.push(PrivateThreadInfo::new(t_loc_rx, local_thread_queue, chan_init_info.crit_msgs_tx.clone(), num_threads));
    }

    let client_msgs_tx = chan_init_info.client_msgs_tx.clone();
    let res = crossbeam::thread::scope(|scope|
    {
        for (i, priv_tinfo) in priv_tinfo_table.into_iter().enumerate()
        {
            let thread_args = ThreadArgs
            {
                crit_sect_info: &crit_sect_info,
                private_tinfo: priv_tinfo,
                loc_tinfo_table: &loc_tinfo_table,
                outbnd_msgs: client_msgs_tx.clone(),
                sched_spec_state: &sched_spec_state,
                t_id: i
            };

            scope.spawn(move |_|
            {
                task_thread_run(thread_args);
            });
        }
    });

    res.map_err(|_| SchedulerError::LocalError(LocalSchedulerError::SchedThreadSpawn))
}

impl TaskRegistration for Scheduler
{
    fn register_task(&mut self, reg_info: TaskRegInfo) -> SchedulerResult<usize>
    {
        self.ensure_term_msg_not_sent_and_no_err_and_update_rem_acks()?;
        
        let task_id = self.next_task_id;
        self.sched_crit_msgs_tx.send_reg_task_msg(reg_info, task_id);
        self.next_task_id += 1;

        Ok(task_id)
    }

    fn unregister_task(&mut self, task_id: usize) -> SchedulerResult<()>
    {
        self.ensure_term_msg_not_sent_and_no_err_and_update_rem_acks()?;
        // TODO: Consider validating task_id before we send the message
        self.sched_crit_msgs_tx.send_unreg_task_msg(task_id);
        Ok(())
    }

    fn register_static_run_condition(&mut self, run_cond_new_func: StaticRunCondNewFunc) -> SchedulerResult<usize>
    {
        self.ensure_term_msg_not_sent_and_no_err_and_update_rem_acks()?;
        self.sched_crit_msgs_tx.send_reg_static_run_cond_msg(run_cond_new_func);
        Ok(self.get_next_run_cond_id())
    }

    fn register_dynamic_run_condition(&mut self, run_cond_new_func: DynamicRunCondNewFunc) -> SchedulerResult<usize>
    {
        self.ensure_term_msg_not_sent_and_no_err_and_update_rem_acks()?;
        self.sched_crit_msgs_tx.send_reg_dynamic_run_cond_msg(run_cond_new_func);
        Ok(self.get_next_run_cond_id())
    }

    fn register_task_with_run_condition(&mut self, task_id: usize, run_cond_id: usize) -> SchedulerResult<()>
    {
        self.ensure_term_msg_not_sent_and_no_err_and_update_rem_acks()?;
        // TODO: Consider validating input before we send the message
        self.sched_crit_msgs_tx.send_reg_task_to_run_cond_msg(task_id, run_cond_id);
        Ok(())
    }

    fn register_name_for_task(&mut self, task_id: usize, name: String) -> SchedulerResult<()>
    {
        self.ensure_term_msg_not_sent_and_no_err_and_update_rem_acks()?;
        self.debug_info.task_id_to_name_table.insert(task_id, name);
        Ok(())
    }

    fn register_name_for_run_condition(&mut self, run_cond_id: usize, name: String) -> SchedulerResult<()>
    {
        self.ensure_term_msg_not_sent_and_no_err_and_update_rem_acks()?;
        self.debug_info.run_cond_id_to_name.insert(run_cond_id, name);
        Ok(())
    }
}

struct ThreadArgs<'a>
{
    pub private_tinfo: PrivateThreadInfo,
    pub crit_sect_info: &'a CriticalSectionInfo,
    pub loc_tinfo_table: &'a Vec<LocalThreadInfo>,
    pub outbnd_msgs: Sender<SchedOutboundMsg>,
    pub sched_spec_state: &'a SchedulerSpecificState,
    pub t_id: usize
}

impl<'a> ThreadArgs<'a>
{
    pub fn get_thread_active_info(&self) -> &'a LocalThreadInfo
    {
        &self.loc_tinfo_table[self.t_id]
    }
}

struct TaskFuncs
{
    pub run_func: Arc<TaskRunFunc>,
    pub get_num_items_func: TaskGetNumItemsFunc
}

#[derive(Debug)]
struct TaskRunCondCounts
{
    pub num_static: usize,
    pub num_dynamic: usize
}

impl TaskRunCondCounts
{
    pub fn new() -> TaskRunCondCounts
    {
        TaskRunCondCounts
        {
            num_static: 0,
            num_dynamic: 0
        }
    }

    pub fn clear(&mut self)
    {
        self.num_static = 0;
        self.num_dynamic = 0;
    }
}

struct RunCondCallbackMsgQueues
{
    pub stat_msg_queue: VecDeque<usize>,
    pub dyn_met_msg_queue: VecDeque<usize>,
    pub dyn_unmet_msg_queue: VecDeque<usize>
}

impl RunCondCallbackMsgQueues
{
    pub fn new() -> RunCondCallbackMsgQueues
    {
        RunCondCallbackMsgQueues
        {
            stat_msg_queue: VecDeque::new(),
            dyn_met_msg_queue: VecDeque::new(),
            dyn_unmet_msg_queue: VecDeque::new()
        }
    }

    fn get_rc_notif_state<'a>(&'a mut self) -> RcNotificationState<'a>
    {
        RcNotificationState
        {
            notif_msg_queue: self
        }
    }
}

enum RcCallbackType
{
    Scheduled,
    Starting,
    Completed
}

struct PendingRcNotif
{
    task_id: usize,
    rc_id: usize
}

impl PendingRcNotif
{
    fn new(task_id: usize, rc_id: usize) -> PendingRcNotif
    {
        PendingRcNotif
        {
            task_id,
            rc_id
        }
    }
}

struct TaskRcCallbacks
{
    cbs_reged_to_rcs: Vec<Vec<RcCallbackType>>, // Indexed by rc_id

    pending_sched_rc_notifs: VecDeque<PendingRcNotif>,
    pending_starting_rc_notifs: VecDeque<PendingRcNotif>,
    pending_completed_rc_notifs: VecDeque<PendingRcNotif>,

    // Layer of indirection with looking up id and then getting cb. Is this ok?
    rc_ids_reged_for_sched_cbs_per_task: Vec<Vec<usize>>,
    rc_ids_reged_for_starting_cbs_per_task: Vec<Vec<usize>>,
    rc_ids_reged_for_completed_cbs_per_task: Vec<Vec<usize>>
}

impl TaskRcCallbacks
{
    fn new() -> TaskRcCallbacks
    {
        TaskRcCallbacks
        {
            cbs_reged_to_rcs:Vec::new(),

            pending_sched_rc_notifs: VecDeque::new(),
            pending_starting_rc_notifs: VecDeque::new(),
            pending_completed_rc_notifs: VecDeque::new(),

            rc_ids_reged_for_sched_cbs_per_task: Vec::new(),
            rc_ids_reged_for_starting_cbs_per_task: Vec::new(),
            rc_ids_reged_for_completed_cbs_per_task: Vec::new()
        }
    }

    fn add_pending_sched_rc_cb_for_task(&mut self, task_id: usize)
    {
        Self::add_rc_cbs_to_pending_queue_for_task_event(task_id, &self.rc_ids_reged_for_sched_cbs_per_task, &mut self.pending_sched_rc_notifs);
    }

    fn add_pending_starting_rc_cb_for_task(&mut self, task_id: usize)
    {
        Self::add_rc_cbs_to_pending_queue_for_task_event(task_id, &self.rc_ids_reged_for_starting_cbs_per_task, &mut self.pending_starting_rc_notifs);
    }

    fn add_pending_completed_rc_cb_for_task(&mut self, task_id: usize)
    {
        Self::add_rc_cbs_to_pending_queue_for_task_event(task_id, &self.rc_ids_reged_for_completed_cbs_per_task, &mut self.pending_completed_rc_notifs);
    }

    fn add_rc_cbs_to_pending_queue_for_task_event(task_id: usize, cb_type_rc_ids: &Vec<Vec<RunCondId>>, pending_rc_cb_queue: &mut VecDeque<PendingRcNotif>)
    {
        let rc_ids_to_add_to_pending_queue = cb_type_rc_ids[task_id].iter().map(|rc_id| PendingRcNotif::new(task_id, *rc_id));
        pending_rc_cb_queue.extend(rc_ids_to_add_to_pending_queue);
    }

    fn reg_cb_type_for_rc(&mut self, rc_id: RunCondId, cb_type: RcCallbackType)
    {
        self.cbs_reged_to_rcs[rc_id].push(cb_type);
    }

    fn setup_cbs_for_new_task_for_rc(&mut self, task_id: usize, rc_id: RunCondId)
    {
        let cbs_for_rc = self.cbs_reged_to_rcs[rc_id].iter();
        for cb_type in cbs_for_rc
        {
            match cb_type
            {
                RcCallbackType::Scheduled => self.rc_ids_reged_for_sched_cbs_per_task[task_id].push(rc_id),
                RcCallbackType::Starting => self.rc_ids_reged_for_starting_cbs_per_task[task_id].push(rc_id),
                RcCallbackType::Completed => self.rc_ids_reged_for_completed_cbs_per_task[task_id].push(rc_id)
            }
        }
    }

    fn process_any_pending_rc_cbs(&mut self, run_conds: &mut Vec<Box<dyn RunCondition + Send>>, mut rc_state: RcNotificationState)
    {
        // Gross duplication... I Don't see a nice way around this though.
        for pend_notif in self.pending_sched_rc_notifs.drain(0..)
        {
            trace!("Got a sched!");
            run_conds[pend_notif.rc_id].task_scheduled(pend_notif.task_id, &mut rc_state);
        }

        for pend_notif in self.pending_starting_rc_notifs.drain(0..)
        {
            trace!("Got a started!");
            run_conds[pend_notif.rc_id].task_starting(pend_notif.task_id, &mut rc_state);
        }

        for pend_notif in self.pending_completed_rc_notifs.drain(0..)
        {
            trace!("Got a completed!");
            run_conds[pend_notif.rc_id].task_completed(pend_notif.task_id, &mut rc_state);
        }
    }  

    fn clear_task_callback_vecs(&mut self, task_idx: usize)
    {
        self.rc_ids_reged_for_sched_cbs_per_task[task_idx].clear();
        self.rc_ids_reged_for_starting_cbs_per_task[task_idx].clear();
        self.rc_ids_reged_for_completed_cbs_per_task[task_idx].clear();
    }

    fn add_new_entry_for_new_rc(&mut self)
    {
        self.cbs_reged_to_rcs.push(Vec::new());
    }

    fn append_new_vec_entries_for_task_callbacks(&mut self)
    {
        self.rc_ids_reged_for_sched_cbs_per_task.push(Vec::new());
        self.rc_ids_reged_for_starting_cbs_per_task.push(Vec::new());
        self.rc_ids_reged_for_completed_cbs_per_task.push(Vec::new());
    }
}

// TODO: Likely will need to break this struct into smaller pieces once I know how the data is going to be used better
struct TaskState
{
    registered_run_conds: Vec<Box<dyn RunCondition + Send>>,
    run_cond_types: Vec<RunConditionType>,
    pub task_indiv_task_status: Vec<TaskStatus>,
    task_parts_rem: Vec<usize>,
    pub run_cond_cb_msg_queues: RunCondCallbackMsgQueues,
    task_rc_cbs: TaskRcCallbacks,
    pub task_funcs: Vec<TaskFuncs>,
    pub task_run_cond_counts: Vec<TaskRunCondCounts>,
    pub task_rem_run_conds_counts: Vec<TaskRunCondCounts>,
    avg_ns_per_task_item: Vec<utils::RollingAvg>,
    task_idxs_tracking_state: utils::ReuseableIds<usize>,
    task_id_to_idx_table: HashMap<usize, usize>,
    run_conds_per_task: Vec<Vec<usize>>,
    tasks_with_no_run_conds_needing_bootstrap: Vec<usize>
}

impl TaskState
{
    pub fn new() -> TaskState
    {
        TaskState
        {
            registered_run_conds: Vec::new(),
            run_cond_types: Vec::new(),
            task_indiv_task_status: Vec::new(),
            task_parts_rem: Vec::new(),
            run_cond_cb_msg_queues: RunCondCallbackMsgQueues::new(),
            task_rc_cbs: TaskRcCallbacks::new(),
            task_funcs: Vec::new(),
            task_run_cond_counts: Vec::new(),
            task_rem_run_conds_counts: Vec::new(),
            avg_ns_per_task_item: Vec::new(),
            task_idxs_tracking_state: utils::ReuseableIds::new(),
            task_id_to_idx_table: HashMap::new(),
            run_conds_per_task: Vec::new(),
            tasks_with_no_run_conds_needing_bootstrap: Vec::new()
        }
    }

    pub fn notify_task_scheduled(&mut self, task_idx: usize)
    {
        self.set_task_status(task_idx, TaskStatus::Runnable);
        self.reset_rem_dynamic_run_cond_counts_for_task(task_idx);
        self.task_parts_rem[task_idx] = 1;

        self.task_rc_cbs.add_pending_sched_rc_cb_for_task(task_idx)
    }

    pub fn notify_task_starting(&mut self, task_idx: usize)
    {
        self.task_rc_cbs.add_pending_starting_rc_cb_for_task(task_idx);
    }

    pub fn notify_task_completed_and_ret_if_all_split_tasks_done(&mut self, task_idx: usize) -> bool
    {
        self.task_parts_rem[task_idx] -= 1;
        if self.task_parts_rem[task_idx] == 0
        {
            self.set_task_status(task_idx, TaskStatus::StaticUnmet);
            self.update_task_status_if_needed(task_idx);
            self.task_rc_cbs.add_pending_completed_rc_cb_for_task(task_idx);
            return true;
        }

        false
    }

    fn update_task_status_if_needed(&mut self, task_idx: usize)
    {        
        match self.get_task_status(task_idx)
        {
            TaskStatus::StaticUnmet => self.update_static_unmet_task_status(task_idx),
            TaskStatus::DynamicUnmet => self.update_dynamic_unmet_task_status(task_idx),
            TaskStatus::Runnable => ()
        }
    }

    fn update_static_unmet_task_status(&mut self, task_idx: usize)
    {
        if self.task_rem_run_conds_counts[task_idx].num_static == 0
        {
            self.set_task_status(task_idx, TaskStatus::DynamicUnmet);
            self.reset_rem_static_run_cond_counts_for_task(task_idx);
            self.update_dynamic_unmet_task_status(task_idx);
        }
    }

    fn update_dynamic_unmet_task_status(&mut self, task_idx: usize)
    {
        if self.task_rem_run_conds_counts[task_idx].num_dynamic == 0
        {
            self.set_task_status(task_idx, TaskStatus::Runnable);
            self.reset_rem_dynamic_run_cond_counts_for_task(task_idx);
        }
    }

    fn handle_task_split(&mut self, task_idx: usize)
    {
        self.task_parts_rem[task_idx] += 1;
    }

    fn handle_avg_time_update_per_task(&mut self, task_idx: usize, avg_ns_per_item_for_run: TaskTime)
    {
        self.avg_ns_per_task_item[task_idx].add_new_val(avg_ns_per_item_for_run as f32);
    }

    pub fn get_task_status(&self, task_idx: usize) -> TaskStatus
    {
        self.task_indiv_task_status[task_idx]
    }

    fn set_task_status(&mut self, task_idx: usize, new_status: TaskStatus)
    {
        self.task_indiv_task_status[task_idx] = new_status
    }

    pub fn reset_rem_static_run_cond_counts_for_task(&mut self, task_idx: usize)
    {
        self.task_rem_run_conds_counts[task_idx].num_static = self.task_run_cond_counts[task_idx].num_static;
        trace!("After static reset: {}", self.task_rem_run_conds_counts[task_idx].num_static);
    }

    pub fn reset_rem_dynamic_run_cond_counts_for_task(&mut self, task_idx: usize)
    {
        self.task_rem_run_conds_counts[task_idx].num_dynamic = self.task_run_cond_counts[task_idx].num_dynamic;
    }

    pub fn register_task(&mut self, task_reg_info: TaskRegInfo, task_id: usize, sched_is_paused: bool)
    {
        let task_funcs = TaskFuncs
        {
            run_func: Arc::new(task_reg_info.run_func),
            get_num_items_func: task_reg_info.get_num_items_func
        };

        let (should_push, task_idx) = self.map_task_id_to_task_idx(task_id);
        if should_push
        {
            self.task_funcs.push(task_funcs);
            self.push_new_task_related_vecs();
        }
        else
        {
            self.task_funcs[task_idx] = task_funcs;
        }

        if sched_is_paused
        {
            // If the sched is restarted before this task is registered with a run cond, we will need to manually start it for the first run
            self.tasks_with_no_run_conds_needing_bootstrap.push(task_id);
        }
    }

    pub fn unregister_task(&mut self, task_id: usize) -> Result<(), SchedulerInternalError>
    {
        let task_idx = self.get_task_idx_from_task_id(task_id)?;

        self.task_idxs_tracking_state.notify_id_unused(task_idx);

        self.task_indiv_task_status[task_idx] = TaskStatus::StaticUnmet;
        self.avg_ns_per_task_item[task_idx].clear();
        self.task_rc_cbs.clear_task_callback_vecs(task_idx);
        self.unreg_task_with_all_run_conds(task_idx);
        self.remove_task_id_to_task_idx_mapping(task_id);

        Ok(())
    }

    fn map_task_id_to_task_idx(&mut self, task_id: usize) -> (bool, usize)
    {
        let (should_push, task_idx) = (self.task_idxs_tracking_state.next_id_is_recycled(), self.task_idxs_tracking_state.get_next_id());

        self.task_id_to_idx_table.insert(task_id, task_idx);
        (should_push, task_idx)
    }

    fn get_task_idx_from_task_id(&self, task_id: usize) -> Result<usize, SchedulerInternalError>
    {
        if !self.task_id_to_idx_table.contains_key(&task_id)
        {
            return Err(SchedulerInternalError::InvalidTaskId(task_id));
        }

        Ok(self.task_id_to_idx_table[&task_id])
    }

    fn unreg_task_with_all_run_conds(&mut self, task_idx: usize)
    {
        for run_cond_id in self.run_conds_per_task[task_idx].iter()
        {
            self.registered_run_conds[*run_cond_id].unreg_task(task_idx);
        }

        self.run_conds_per_task[task_idx].clear();
        self.task_run_cond_counts[task_idx].clear();
        self.task_rem_run_conds_counts[task_idx].clear();
    }

    fn remove_task_id_to_task_idx_mapping(&mut self, task_id: usize)
    {
        self.task_id_to_idx_table.remove(&task_id);
    }

    pub fn register_static_run_condition_type(&mut self, run_cond_new_func: StaticRunCondNewFunc)
    {
        let mut rc_cb_reg_info = self.create_rc_cb_reg_info_for_rc();
        let run_cond = run_cond_new_func(&mut rc_cb_reg_info, StaticRcRegInfo::new());
        self.register_created_run_cond(run_cond, RunConditionType::Static);
    }

    pub fn register_dynamic_run_condition_type(&mut self, run_cond_new_func: DynamicRunCondNewFunc)
    {
        let mut rc_cb_reg_info = self.create_rc_cb_reg_info_for_rc();
        let run_cond = run_cond_new_func(&mut rc_cb_reg_info, DynamicRcRegInfo::new());
        self.register_created_run_cond(run_cond, RunConditionType::Dynamic);
    }

    fn create_rc_cb_reg_info_for_rc(&mut self) -> RcCbRegInfo
    {
        self.task_rc_cbs.add_new_entry_for_new_rc();
        let rc_id = self.registered_run_conds.len();
        RcCbRegInfo::new(&mut self.task_rc_cbs, rc_id)
    }

    fn register_created_run_cond(&mut self, run_cond: Box<dyn RunCondition + Send>, run_cond_type: RunConditionType)
    {
        self.run_cond_types.push(run_cond_type);
        self.registered_run_conds.push(run_cond);
    }

    fn push_new_task_related_vecs(&mut self)
    {
        self.task_indiv_task_status.push(TaskStatus::StaticUnmet);
        self.run_conds_per_task.push(Vec::new());
        self.task_parts_rem.push(0);
        self.task_run_cond_counts.push(TaskRunCondCounts::new());
        self.task_rem_run_conds_counts.push(TaskRunCondCounts::new());
        
        let mut avg_ns_per_item = utils::RollingAvg::new(NUM_TASKS_RUNS_AVG_ITEM_TIMES_TO_OBSERVE);
        avg_ns_per_item.add_new_val(INITIAL_TIME_GUESS_OF_WRKLD_PER_TASK_ITEM as f32);
        self.avg_ns_per_task_item.push(avg_ns_per_item);

        self.task_rc_cbs.append_new_vec_entries_for_task_callbacks()
    }

    pub fn register_task_to_run_condition(&mut self, task_id: usize, run_cond_id: RunCondId) -> Result<(), SchedulerInternalError>
    {
        let task_idx = self.get_task_idx_from_task_id(task_id)?;

        if self.registered_run_conds.len() <= run_cond_id
        {
            return Err(SchedulerInternalError::InvalidRunCondId(run_cond_id));
        }

        if self.run_conds_per_task[task_idx].contains(&run_cond_id)
        {
            return Err(SchedulerInternalError::RunCondDupTaskId(run_cond_id, task_id));
        }

        self.registered_run_conds[run_cond_id].reg_task(task_idx, &mut self.run_cond_cb_msg_queues.get_rc_notif_state());
        self.run_conds_per_task[task_idx].push(task_idx);

        let (tot_count, rem_count) = match &mut self.run_cond_types[run_cond_id]
        {
            RunConditionType::Static => (&mut self.task_run_cond_counts[task_id].num_static, &mut self.task_rem_run_conds_counts[task_id].num_static),
            RunConditionType::Dynamic => (&mut self.task_run_cond_counts[task_id].num_dynamic, &mut self.task_rem_run_conds_counts[task_id].num_dynamic)
        };

        *tot_count += 1;
        *rem_count += 1; // Possible bug... 

        self.task_rc_cbs.setup_cbs_for_new_task_for_rc(task_id, run_cond_id);
        self.remove_task_from_bootstrap_list_if_exists(task_id);

        Ok(())
    }

    fn remove_task_from_bootstrap_list_if_exists(&mut self, task_id: usize)
    {
        self.tasks_with_no_run_conds_needing_bootstrap.swap_remove(task_id);
    }

    pub fn get_task_funcs(&self, task_idx: usize) -> &TaskFuncs
    {
        &self.task_funcs[task_idx]
    }

    fn get_curr_avg_time_per_task_item(&self, task_idx: usize) -> TaskTime
    {
        self.avg_ns_per_task_item[task_idx].get_curr_avg() as TaskTime
    }

    fn get_task_ids_with_no_run_conds_that_need_bootstrap<'a>(&'a self) -> impl Iterator<Item=usize> + 'a
    {
        self.tasks_with_no_run_conds_needing_bootstrap.iter().cloned()
    }

    fn process_any_pending_rc_cbs(&mut self)
    {
        self.task_rc_cbs.process_any_pending_rc_cbs(&mut self.registered_run_conds, self.run_cond_cb_msg_queues.get_rc_notif_state());
    }
}

pub struct StaticRcRegInfo { }
impl StaticRcRegInfo
{
    fn new() -> StaticRcRegInfo
    {
        StaticRcRegInfo { }
    }

    pub fn notify_static_rc_conds_met(&self, task_id: usize, notif_state: &mut RcNotificationState)
    {
        notif_state.notif_msg_queue.stat_msg_queue.push_front(task_id);
    }
}

pub struct DynamicRcRegInfo { }
impl DynamicRcRegInfo
{
    fn new() -> DynamicRcRegInfo
    {
        DynamicRcRegInfo { }
    }

    pub fn notify_dynamic_rc_conds_met(task_id: usize, notif_state: &mut RcNotificationState)
    {
        notif_state.notif_msg_queue.dyn_met_msg_queue.push_front(task_id);
    }

    pub fn notify_dynamic_rc_conds_unmet(task_id: usize, notif_state: &mut RcNotificationState)
    {
        notif_state.notif_msg_queue.dyn_unmet_msg_queue.push_back(task_id);
    }
}


struct RcCbRegInfo<'a>
{
    task_rc_cbs: &'a mut TaskRcCallbacks,
    rc_id: RunCondId
}

impl<'a> RcCbRegInfo<'a>
{
    fn new(task_rc_cbs: &'a mut TaskRcCallbacks, rc_id: RunCondId) -> RcCbRegInfo<'a>
    {
        RcCbRegInfo
        {
            task_rc_cbs,
            rc_id
        }
    }
}

impl<'a> RunCondSchedulerCallbackRegistration<'a> for RcCbRegInfo<'a>
{
    fn reg_task_scheduled_callback(&mut self)
    {
        self.task_rc_cbs.reg_cb_type_for_rc(self.rc_id, RcCallbackType::Scheduled);
    }

    fn reg_task_completed_callback(&mut self)
    {
        self.task_rc_cbs.reg_cb_type_for_rc(self.rc_id, RcCallbackType::Completed);
    }

    fn reg_task_starting_callback(&mut self)
    {
        self.task_rc_cbs.reg_cb_type_for_rc(self.rc_id, RcCallbackType::Starting);
    }
}

struct UnassignedTasks
{
    queue: VecDeque<ThreadTask>
}

impl UnassignedTasks
{
    pub fn new() -> UnassignedTasks
    {
        UnassignedTasks
        {
            queue: VecDeque::new()
        }
    }

    pub fn add_new_unassigned_task(&mut self, task: ThreadTask)
    {
        self.queue.push_back(task);
    }

    fn get_task(&mut self) -> Option<ThreadTask>
    {
        self.queue.pop_front()
    }
}

struct ThreadTask
{
    task_idx: usize,
    run_func: Arc<TaskRunFunc>,
    num_items_to_proc: usize,
    est_time_per_item: TaskTime,
    start_idx: usize
}

impl ThreadTask
{
    fn split_task(&mut self, perc: f32) -> ThreadTask
    {
        // Don't want to ceil, but we definitely want to guarantee a least one item in the split
        let num_items_in_split = max(1, (self.num_items_to_proc as f32 * perc) as usize);

        let new_task_start_idx = self.start_idx + self.num_items_to_proc - num_items_in_split;
        self.num_items_to_proc -= num_items_in_split;

        ThreadTask
        {
            task_idx: self.task_idx,
            run_func: self.run_func.clone(),
            num_items_to_proc: num_items_in_split,
            est_time_per_item: self.est_time_per_item,
            start_idx: new_task_start_idx
        }
    }

    fn get_est_wrkld(&self) -> TaskTime
    {
        self.num_items_to_proc as TaskTime * self.est_time_per_item
    }

    fn get_wrk_req(&self) -> RunTaskWorkRequest
    {
        RunTaskWorkRequest::new(self.num_items_to_proc, self.start_idx)
    }
}

impl fmt::Debug for ThreadTask
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        f.debug_struct("TaskInfo")
            .field("task_idx", &self.task_idx)
            .field("run_func", &"[Closure]")
            .field("num_items_to_proc", &self.num_items_to_proc)
            .field("est_time_per_item", &self.est_time_per_item)
            .field("start_idx", &self.start_idx)
            .finish()
    }
}

struct PrivateThreadInfo
{
    t_specific_msgs: Receiver<WorkingThreadMsg>,
    crit_msg_tx: CritMsgsTx,
    pub assigned_tasks_queue: Worker<ThreadTask>,
    t_state: PrivateThreadState
}

impl PrivateThreadInfo
{
    pub fn new(t_specific_msgs: Receiver<WorkingThreadMsg>, worker: Worker<ThreadTask>, crit_msg_tx: Sender<CritMessage>, n_threads_alive: usize) -> PrivateThreadInfo
    {
        PrivateThreadInfo
        {
            t_specific_msgs: t_specific_msgs,
            crit_msg_tx: CritMsgsTx::new(crit_msg_tx),
            assigned_tasks_queue: worker,
            t_state: PrivateThreadState::new(n_threads_alive)
        }
    }

    pub fn get_next_task_to_execute(&self) -> Option<ThreadTask>
    {
        self.assigned_tasks_queue.pop()
    }
}

struct PrivateThreadState
{
    is_paused: bool,
    should_terminate: bool,
    is_curr_dedicated_crit_sect_thread: bool,
    other_thread_state: OtherThreadState
}

impl PrivateThreadState
{
    fn new(n_threads_alive: usize) -> PrivateThreadState
    {
        PrivateThreadState
        {
            is_paused: false,
            should_terminate: false,
            is_curr_dedicated_crit_sect_thread: false,
            other_thread_state: OtherThreadState::new(n_threads_alive)
        }
    }

    fn should_not_block_on_recv_t_msg(&self, already_proced_one_msg: bool, thread_has_tasks: bool) -> bool
    {
        let block_override = self.is_paused;
        let no_block_override = self.is_curr_dedicated_crit_sect_thread || self.should_terminate;

        no_block_override || (!block_override && (already_proced_one_msg || thread_has_tasks))
    }
}

#[derive(Clone, Copy, Debug)]
struct OtherThreadState
{
    n_threads_alive: usize,
    avg_thread_wrkld: TaskTime
}

impl OtherThreadState
{
    fn new(n_threads_alive: usize) -> OtherThreadState
    {
        OtherThreadState
        {
            n_threads_alive,
            avg_thread_wrkld: 0
        }
    }

    fn update_state_from_msg(&mut self, other_thread_update: OtherThreadUpdateInfo)
    {
        match other_thread_update
        {
            OtherThreadUpdateInfo::ThreadAliveChange(alive_change) => self.n_threads_alive = (self.n_threads_alive as i32 + alive_change) as usize,
            OtherThreadUpdateInfo::AvgWrkldUpdate(new_avg) => self.avg_thread_wrkld = new_avg
        }
    }
}

struct LocalThreadInfo
{
    pub task_info: PublicThreadInfo
}

impl LocalThreadInfo
{
    pub fn new(t_specific_msgs: Sender<WorkingThreadMsg>, stealer: Stealer<ThreadTask>) -> LocalThreadInfo
    {
        LocalThreadInfo
        {
            task_info: PublicThreadInfo::new(t_specific_msgs, stealer)
        }
    }
}

struct PublicThreadInfo
{
    t_specific_msgs: Sender<WorkingThreadMsg>,
    pub thread_task_stealer: Stealer<ThreadTask>,
    thread_est_workload: AtomicU64 // Should this be atomic?
}

impl PublicThreadInfo
{
    pub fn new(thread_specific_msgs: Sender<WorkingThreadMsg>, thread_task_stealer: Stealer<ThreadTask>) -> PublicThreadInfo
    {
        PublicThreadInfo
        {
            t_specific_msgs: thread_specific_msgs,
            thread_task_stealer: thread_task_stealer,
            thread_est_workload: AtomicU64::new(0)
        }
    }

    pub fn get_est_workload_ns(&self) -> TaskTime
    {
        self.thread_est_workload.load(Ordering::Relaxed)
    }

    pub fn add_est_workload_ns(&self, wrkld_to_add: TaskTime)
    {
        // I don't think this fetch_update call can ever return an error, so unwrapping
        self.thread_est_workload.fetch_update(Ordering::Relaxed, Ordering::Relaxed, |curr_wrkld| Some(curr_wrkld + wrkld_to_add)).unwrap();
    }

    pub fn remove_est_workload_ns(&self, wrkld_to_remove: TaskTime)
    {
        // I don't think this fetch_update call can ever return an error, so unwrapping
        self.thread_est_workload.fetch_update(Ordering::Relaxed, Ordering::Relaxed, |curr_wrkld| Some(curr_wrkld - wrkld_to_remove)).unwrap();
    }

    pub fn steal_task(&self, crit_msgs: &CritMsgsTx, req_wrkld_size: TaskTime) -> Option<ThreadTask>
    {
        // TODO: Simple impl for now. Later this should be smarter.
        // Note: Prone to race conditions!
        if self.get_est_workload_ns() <= MIN_THREAD_WRLKD_REQ_TO_STEAL_NS
        {
            return None;
        }

        let mut s_task = match self.steal_task_until_succ_or_empty()
        {
            Some(task) => task,
            None => return None
        };

        // If the task is smaller than we are requesting then return the whole thing
        let max_wrkld_of_task_that_we_can_steal = req_wrkld_size + EXTRA_EXTRA_WRKLD_THRESHOLD_NS;
        if s_task.get_est_wrkld() <= max_wrkld_of_task_that_we_can_steal
        {
            return Some(s_task);
        }

        // Task is too big. We need to split it up.
        let perc_of_task_to_take = req_wrkld_size as f32 / s_task.get_est_wrkld() as f32;
        let new_split_task = split_task_and_update_task_state(crit_msgs, &mut s_task, perc_of_task_to_take);
        self.remove_est_workload_ns(new_split_task.get_est_wrkld());

        Some(new_split_task)
    }

    fn steal_task_until_succ_or_empty(&self) -> Option<ThreadTask>
    {
        let mut done = false;
        let mut res = None;

        while !done
        {
            done = true;

            match self.thread_task_stealer.steal()
            {
                Steal::Success(s_task) => res = Some(s_task),
                Steal::Retry => done = false,
                Steal::Empty => ()
            }
        }

        return res;
    }
}

struct CriticalSectionInfo
{
    pub crit_sect_data: Mutex<CritSectData>,
    pub work_available: AtomicBool,
    pub currently_processing_new_tasks: AtomicBool
}

impl CriticalSectionInfo
{
    pub fn new(crit_msgs_rx: Receiver<CritMessage>, num_threads: usize) -> CriticalSectionInfo
    {
        CriticalSectionInfo
        {
            crit_sect_data: Mutex::new(CritSectData::new(crit_msgs_rx, num_threads)),
            work_available: AtomicBool::new(false),
            currently_processing_new_tasks: AtomicBool::new(false)
        }
    }
}

struct CritSectTaskData
{
    pub task_state: TaskState,
    pub unassigned_tasks: UnassignedTasks,
}

impl CritSectTaskData
{
    fn new() -> CritSectTaskData
    {
        CritSectTaskData
        {
            task_state: TaskState::new(),
            unassigned_tasks: UnassignedTasks::new()
        }
    }
}

struct CritSectData
{
    task_data: CritSectTaskData,
    crit_sect_state: CritSectState,
    crit_msgs_rx: Receiver<CritMessage>
}

impl CritSectData
{
    pub fn new(crit_msgs_rx: Receiver<CritMessage>, num_threads: usize) -> CritSectData
    {
        CritSectData
        {
            task_data: CritSectTaskData::new(),
            crit_sect_state: CritSectState::new(num_threads),
            crit_msgs_rx
        }
    }

    fn create_post_crit_sect_data(&self) -> PostCritProcData
    {
        PostCritProcData::new(self.crit_sect_state.assigned_tot_wrkld)
    }
}

struct CritSectState
{
    num_threads: usize,
    num_threads_paused: usize,
    num_threads_terminated: usize,
    should_always_block_when_procing_crit_msgs: bool,
    dedicated_crit_sect_thread_active: bool,
    assigned_tot_wrkld: TaskTime
}

impl CritSectState
{
    // Todo: Moved to a system where threads can be started/destroyed during runtime
    fn new(num_threads: usize) -> CritSectState
    {
        CritSectState
        {
            num_threads: num_threads,
            num_threads_paused: 0,
            num_threads_terminated: 0,
            should_always_block_when_procing_crit_msgs: false,
            dedicated_crit_sect_thread_active: false,
            assigned_tot_wrkld: 0
        }
    }

    fn get_avg_thread_wrkld(&self) -> TaskTime
    {
        (self.assigned_tot_wrkld as f32 / self.num_threads as f32) as TaskTime
    }
}

struct CritMsgsTx
{
    chan: Sender<CritMessage>
}

struct SchedulerSpecificState
{
    sched_run_state: AtomicCell<SchedulerActiveState>,
    scheduler_error: Arc<AtomicCell<Option<SchedulerInternalError>>>
}

impl SchedulerSpecificState
{
    pub fn new(user_shared_err: Arc<AtomicCell<Option<SchedulerInternalError>>>) -> SchedulerSpecificState
    {
        SchedulerSpecificState
        {
            sched_run_state: AtomicCell::new(SchedulerActiveState::Running),
            scheduler_error: user_shared_err
        }
    }

    pub fn is_paused(&self) -> bool
    {
        self.sched_run_state.load() == SchedulerActiveState::Paused
    }

    pub fn is_terminated(&self) -> bool
    {
        self.sched_run_state.load() == SchedulerActiveState::Terminated
    }

    pub fn set_sched_pause_state(&self, state: bool)
    {
        let running_state = match state
        {
            true => SchedulerActiveState::Paused,
            false => SchedulerActiveState::Running
        };

        self.sched_run_state.store(running_state);
    }

    pub fn set_term_state(&self)
    {
        self.sched_run_state.store(SchedulerActiveState::Terminated);
    }

    pub fn set_scheduler_error(&self, err: SchedulerInternalError)
    {
        self.scheduler_error.store(Some(err));
    }
}

// TODO: It may be worth separating the scheduler msgs from the internal msgs
impl CritMsgsTx
{
    pub fn new(tx: Sender<CritMessage>) -> CritMsgsTx
    {
        CritMsgsTx
        {
            chan: tx
        }
    }

    pub fn send_mess(&self, mess: CritMessage)
    {
        self.chan.send(mess).unwrap(); // If the internal crit channel ever closes besides when the sched is terminated, then that is a bug
    }

    fn send_client_msg(&self, msg: ClientCritMsg)
    {
        self.send_mess(CritMessage::Client(msg));
    }

    fn send_intern_msg(&self, msg: InternalCritMsg)
    {
        self.send_mess(CritMessage::Internal(msg));
    }

    pub fn notify_task_completed(&self, task_idx: usize, task_est_wrkld: TaskTime)
    {
        self.send_intern_msg(InternalCritMsg::TaskCompleted(task_idx, task_est_wrkld));
    }

    pub fn notify_task_split(&self, task_idx: usize)
    {
        self.send_intern_msg(InternalCritMsg::TaskSplit(task_idx));
    }

    pub fn notify_task_started(&self, task_idx: usize)
    {
        self.send_intern_msg(InternalCritMsg::TaskStarted(task_idx));
    }

    fn notify_thread_terminated(&self)
    {
        self.send_intern_msg(InternalCritMsg::ThreadTerminated);
    }

    fn notify_thread_paused(&self)
    {
        self.send_intern_msg(InternalCritMsg::ThreadPaused);
    }

    fn notify_thread_unpaused(&self)
    {
        self.send_intern_msg(InternalCritMsg::ThreadUnpaused);
    }

    fn notify_task_panicked(&self, task_idx: usize)
    {
        self.send_intern_msg(InternalCritMsg::TaskPanicked(task_idx))
    }

    pub fn send_reg_task_msg(&self, info: TaskRegInfo, task_id: usize)
    {
        self.send_client_msg(ClientCritMsg::RegisterTask(info, task_id));
    }

    pub fn send_unreg_task_msg(&self, task_id: usize)
    {
        self.send_client_msg(ClientCritMsg::UnregisterTask(task_id));
    }

    pub fn send_reg_static_run_cond_msg(&self, run_cond_new_func: StaticRunCondNewFunc)
    {
        self.send_client_msg(ClientCritMsg::RegisterStaticRunCond(run_cond_new_func));
    }

    pub fn send_reg_dynamic_run_cond_msg(&self, run_cond_new_func: DynamicRunCondNewFunc)
    {
        self.send_client_msg(ClientCritMsg::RegisterDynamicRunCond(run_cond_new_func));
    }

    pub fn send_update_avg_task_item_time_msg(&self, task_idx: usize, avg_time_per_item: TaskTime)
    {
        self.send_intern_msg(InternalCritMsg::AvgTaskItemTimeUpdate(task_idx, avg_time_per_item));
    }

    pub fn send_reg_task_to_run_cond_msg(&self, task_id: usize, run_cond_id: usize)
    {
        self.send_client_msg(ClientCritMsg::RegTaskToRunCond(task_id, run_cond_id));
    }

    pub fn send_start_message(&self)
    {
        self.send_client_msg(ClientCritMsg::Unpause);
    }

    pub fn send_pause_message(&self)
    {
        self.send_client_msg(ClientCritMsg::Pause);
    }

    pub fn send_termination_message(&self)
    {
        self.send_client_msg(ClientCritMsg::Terminate);
    }
}

fn task_thread_run(mut t_args: ThreadArgs)
{
    debug!("Thread {} starting...", t_args.t_id);

    while !t_args.private_tinfo.t_state.should_terminate
    {
        if should_process_thread_crit_sect_data(&t_args)
        {
            try_lock_and_process_crit_sect_data(&mut t_args);
        }

        process_thread_msgs(t_args.t_id, &mut t_args.private_tinfo);
        if t_args.private_tinfo.t_state.should_terminate { break; }
        steal_tasks_from_other_threads_if_needed(&t_args);

        let curr_task = t_args.private_tinfo.get_next_task_to_execute();
        match curr_task
        {
            Some(task) =>
            {
                debug!("Thread {} is about to execute a task (task_id: {})", t_args.t_id, task.task_idx);
                let task_idx = task.task_idx;
                let task_est_wrkld = task.get_est_wrkld();

                t_args.private_tinfo.crit_msg_tx.notify_task_started(task_idx);
                execute_task_run_func_and_update_time_per_item(&t_args.private_tinfo.crit_msg_tx, task);
                t_args.private_tinfo.crit_msg_tx.notify_task_completed(task_idx, task_est_wrkld);
                t_args.get_thread_active_info().task_info.remove_est_workload_ns(task_est_wrkld);
            },
            None =>
            {
                
            }
        }
    }

    debug!("Thread {} is terminating", t_args.t_id);

    if t_args.private_tinfo.t_state.other_thread_state.n_threads_alive == 1
    {
        info!("All threads terminated! Sending terminated msg back to sched client t_id: {}", t_args.t_id);
        send_outbnd_msg_and_handle_if_chnl_closed_but_dont_term(&t_args, SchedOutboundMsg::Terminated);
    }

    t_args.private_tinfo.crit_msg_tx.notify_thread_terminated();
}

// Anytime a task thread finishes a task it will enter this function if new tasks have become available.
// Evaluates what new tasks have become available and adds them to the shared task queue.
// Only one thread can be doing this at any given time.
fn try_lock_and_process_crit_sect_data(t_args: &mut ThreadArgs)
{
    let tsk_proc_data_lock = t_args.crit_sect_info.crit_sect_data.try_lock();
    // Check if another thread is already in this critical section. Just return and keep processing if so.
    if tsk_proc_data_lock.is_err() { return }
    trace!("Thread {} has entered the sched crit sect", t_args.t_id);

    let c_data = &mut tsk_proc_data_lock.unwrap();
    proc_crit_sect_data_and_loop_if_ded_thread(t_args, c_data);

    trace!("Thread {} has left the sched crit sect", t_args.t_id);
    t_args.crit_sect_info.work_available.store(false, Ordering::Relaxed);
    t_args.crit_sect_info.currently_processing_new_tasks.store(false, Ordering::Relaxed);
}

fn proc_crit_sect_data_and_loop_if_ded_thread(t_args: &mut ThreadArgs, c_data: &mut CritSectData)
{
    elevate_or_deelevate_ded_crit_thread_check(t_args.t_id, &mut c_data.crit_sect_state, &mut t_args.private_tinfo);

    match t_args.private_tinfo.t_state.is_curr_dedicated_crit_sect_thread
    {
        false => proc_crit_sect_data(t_args, c_data),
        true =>
        {
            while t_args.private_tinfo.t_specific_msgs.is_empty()
            {
                proc_crit_sect_data(t_args, c_data);
            }
        }
    }

    elevate_or_deelevate_ded_crit_thread_check(t_args.t_id, &mut c_data.crit_sect_state, &mut t_args.private_tinfo);
}

fn proc_crit_sect_data(t_args: &mut ThreadArgs, c_data: &mut CritSectData)
{
    c_data.task_data.task_state.process_any_pending_rc_cbs();
    process_task_status_change_msgs(t_args, c_data);

    let p_data = c_data.create_post_crit_sect_data();
    process_crit_sect_msgs(t_args, c_data);

    if t_args.sched_spec_state.is_terminated() { return; }

    distribute_new_tasks_to_threads(t_args, c_data);
    post_proc_crit_msgs_checks(t_args, c_data, p_data);
}

fn process_task_status_change_msgs(t_args: &ThreadArgs, c_data: &mut CritSectData)
{
    t_args.crit_sect_info.currently_processing_new_tasks.store(true, Ordering::Relaxed);

    process_completed_static_run_cond_msgs_and_upgrade_tasks(c_data);
    process_completed_dynamic_run_conds_and_upgrade_tasks(c_data);
}

fn process_crit_sect_msgs(t_args: &ThreadArgs, c_data: &mut CritSectData)
{
    match t_args.private_tinfo.t_state.is_curr_dedicated_crit_sect_thread
    {
        false => process_crit_sect_msgs_and_term_sched_on_err(t_args, c_data, process_any_crit_sect_msgs_no_block),
        true => process_crit_sect_msgs_and_term_sched_on_err(t_args, c_data, process_any_crit_sect_msgs_block_until_one_msg_recvd)
    };
}

fn process_crit_sect_msgs_and_term_sched_on_error_block_on_cond(t_args: &ThreadArgs, c_data: &mut CritSectData)
{
    let res = process_any_crit_sect_msgs_with_block_until_cond_met(t_args, c_data);
    term_sched_if_err(res, t_args);
}

fn term_sched_if_err(res: Result<(), SchedulerInternalError>, t_args: &ThreadArgs)
{
    if let Err(err) = res
    {
        error!("Got the following error while processing critical section messages: {}", err);
        terminate_sched_and_setup_error_state(t_args, err);
    }
}

fn should_process_thread_crit_sect_data(t_args: &ThreadArgs) -> bool
{
    t_args.get_thread_active_info().task_info.get_est_workload_ns() <= MIN_WRKLD_THRESH_TO_ENTER_CRIT_SECT_NS
}

fn process_thread_msgs(t_id: usize, p_t_info: &mut PrivateThreadInfo)
{
    let mut proced_at_least_one_msg = false;
    while let Some(msg) = get_t_msg_and_block_if_needed(&p_t_info.t_specific_msgs, proced_at_least_one_msg, &p_t_info.t_state)
    {
        proced_at_least_one_msg = true;

        trace!("Thread {} processing a {:?} msg...", t_id, msg);
        match msg
        {
            WorkingThreadMsg::TaskAssigned(task) => handle_thread_task_assigned(task, &mut p_t_info.assigned_tasks_queue),
            WorkingThreadMsg::StateUpdate(msg) => process_thread_state_change_msg(msg, p_t_info)
        }
    }
}

fn process_thread_state_change_msg(msg: WorkingThreadSchedStateUpdate, p_t_info: &mut PrivateThreadInfo)
{
    match msg
    {
        WorkingThreadSchedStateUpdate::Pause => handle_thread_pause_msg(p_t_info),
        WorkingThreadSchedStateUpdate::Unpause => handle_thread_unpaused_msg(p_t_info),
        WorkingThreadSchedStateUpdate::Terminate => handle_thread_terminated_msg(p_t_info),
        WorkingThreadSchedStateUpdate::OtherThreadStateUpdate(update) => p_t_info.t_state.other_thread_state.update_state_from_msg(update)
    }
}

fn get_t_msg_and_block_if_needed(t_msgs: &Receiver<WorkingThreadMsg>, already_proced_one_msg: bool, state: &PrivateThreadState) -> Option<WorkingThreadMsg>
{
    get_msg_from_chan_and_block_if_cond_met(t_msgs, || state.should_not_block_on_recv_t_msg(already_proced_one_msg, !t_msgs.is_empty()))
}

fn get_msg_from_chan_and_block_if_cond_met<T, F>(msgs: &Receiver<T>, should_not_block_cond_func: F) -> Option<T>
    where F: Fn() -> bool
{
    let should_not_block = should_not_block_cond_func();
    match should_not_block
    {
        // If the channel disconnects we can still process any rem msgs and is_empty() will catch it
        true => msgs.try_recv().ok(),
        false => msgs.recv().ok()
    }
}

fn handle_thread_task_assigned(task: ThreadTask, ass_tasks: &Worker<ThreadTask>)
{
    ass_tasks.push(task);
}

fn handle_thread_pause_msg(p_t_info: &mut PrivateThreadInfo)
{
    p_t_info.t_state.is_paused = true;
    p_t_info.crit_msg_tx.notify_thread_paused();
}

fn handle_thread_unpaused_msg(p_t_info: &mut PrivateThreadInfo)
{
    p_t_info.t_state.is_paused = false;
    p_t_info.crit_msg_tx.notify_thread_unpaused();
}

fn handle_thread_terminated_msg(p_t_info: &mut PrivateThreadInfo)
{
    p_t_info.t_state.should_terminate = true;
}

fn steal_tasks_from_other_threads_if_needed(t_args: &ThreadArgs)
{
    let mut curr_thread_wrkld = t_args.get_thread_active_info().task_info.get_est_workload_ns();

    if should_steal_tasks_from_other_threads(curr_thread_wrkld, t_args.private_tinfo.t_state.other_thread_state.avg_thread_wrkld)
    {
        steal_tasks_for_thread(t_args, &mut curr_thread_wrkld);
    }
}

fn steal_tasks_for_thread(t_args: &ThreadArgs, curr_thread_wrkld: &mut TaskTime)
{
    // This is not a great implementation, but should be good enough to start with
    let trgt_wrkld = calculate_target_task_wrkld_size(t_args.private_tinfo.t_state.other_thread_state.avg_thread_wrkld);
    let mut rem_wrkld_needed = trgt_wrkld as i64 - *curr_thread_wrkld as i64;

    while rem_wrkld_needed > 0
    {
        let loc_tinfo_iter = t_args.loc_tinfo_table.into_iter();
        let loc_tinfo_and_wrkld_iter = loc_tinfo_iter.map(|x| (x, x.task_info.get_est_workload_ns()));
        let (max_loc_tinfo, max_t_wrkld) = loc_tinfo_and_wrkld_iter.max_by_key(|&(_, wrkld)| wrkld).unwrap();

        let thread_exists_with_sufficient_wrkld_to_steal = trgt_wrkld <= max_t_wrkld;
        if !thread_exists_with_sufficient_wrkld_to_steal
        {
            break;
        }

        match max_loc_tinfo.task_info.steal_task(&t_args.private_tinfo.crit_msg_tx, rem_wrkld_needed as u64)
        {
            Some(task) =>
            {
                rem_wrkld_needed -= task.get_est_wrkld() as i64;
                assign_new_task_to_curr_thread(t_args, task);
            },
            None => break
        };
    }
}

fn elevate_or_deelevate_ded_crit_thread_check(t_id: usize, c_state: &mut CritSectState, p_info: &mut PrivateThreadInfo)
{
    match p_info.t_state.is_curr_dedicated_crit_sect_thread
    {
        false =>
        {
            if p_info.assigned_tasks_queue.is_empty() && !c_state.dedicated_crit_sect_thread_active
            {
                trace!("Thread {} is elevating itself to dedicated crit thread status.", t_id);
                c_state.dedicated_crit_sect_thread_active = true;
                p_info.t_state.is_curr_dedicated_crit_sect_thread = true;
            }
        },
        true =>
        {
            if !p_info.assigned_tasks_queue.is_empty()
            {
                trace!("Thread {} is deelevating itself from dedicated crit thread status.", t_id);
                c_state.dedicated_crit_sect_thread_active = false;
                p_info.t_state.is_curr_dedicated_crit_sect_thread = false;
            }
        }
    }
}

fn process_completed_task(c_data: &mut CritSectData, task_est_wrkld: TaskTime, task_idx: usize)
{
    c_data.crit_sect_state.assigned_tot_wrkld -= task_est_wrkld;
    if c_data.task_data.task_state.notify_task_completed_and_ret_if_all_split_tasks_done(task_idx)
    {
        add_task_to_unass_queue_if_runnable(task_idx, c_data);
    }
}

#[derive(Debug, Eq, PartialEq, PartialOrd, Ord)]
struct ThreadIdAndWrkld
{
    wrkld: TaskTime,
    t_id: usize
}

fn distribute_new_tasks_to_threads(t_args: &ThreadArgs, c_data: &mut CritSectData)
{
    let mut t_wrkld_min_heap = BinaryHeap::new();

    let t_ids_and_wrklds_iter = t_args.loc_tinfo_table.iter().map(|l_info| l_info.task_info.thread_est_workload.load(Ordering::Relaxed)).enumerate();
    let t_ids_and_wrklds_struct_iter = t_ids_and_wrklds_iter.map(|(t_id, wrkld)| Reverse(ThreadIdAndWrkld { wrkld, t_id }));
    t_wrkld_min_heap.extend(t_ids_and_wrklds_struct_iter);


    let new_est_wrkld_from_unass_tasks: TaskTime = c_data.task_data.unassigned_tasks.queue.iter().map(|task| task.get_est_wrkld()).sum();
    let curr_tot_wrkld = c_data.crit_sect_state.assigned_tot_wrkld + new_est_wrkld_from_unass_tasks;
    let trgt_wrkld = curr_tot_wrkld / c_data.crit_sect_state.num_threads as TaskTime;
    let max_wrkld_threads_can_have = (trgt_wrkld as f32 * TRGT_WRKLD_OVERSHOOT_THRESHOLD_PERC) as TaskTime;

    let mut tasks_with_no_items = Vec::new();

    while let Some(mut task) = c_data.task_data.unassigned_tasks.get_task()
    {
        if task.num_items_to_proc == 0
        {
            tasks_with_no_items.push(task);
            continue;
        }

        let Reverse(mut thread_with_smallest_wrkld) = t_wrkld_min_heap.pop().unwrap();

        let max_additional_wrkld_thread_can_take = max_wrkld_threads_can_have - thread_with_smallest_wrkld.wrkld;

        let task_est_wrkld = task.get_est_wrkld();
        let task_to_assign =
        if task_est_wrkld < max_additional_wrkld_thread_can_take || task.num_items_to_proc == 1
        {
            trace!("Assigning full task (task_id: {}, est_wrkld: {}) to thread {}...", task.task_idx, task_est_wrkld, thread_with_smallest_wrkld.t_id);
            task
        }
        else
        {
            // Can't fit entire task into thread
            let perc_of_task_to_take = max_additional_wrkld_thread_can_take as f32 / task_est_wrkld as f32;
            let left_over_task = split_task_and_update_task_state(&t_args.private_tinfo.crit_msg_tx, &mut task, perc_of_task_to_take);
            
            trace!("Assigning split task (task_id: {}, est_wrkld: {}) to thread {}...", left_over_task.task_idx, left_over_task.get_est_wrkld(), left_over_task.task_idx);
            c_data.task_data.unassigned_tasks.add_new_unassigned_task(task); // Put remaining task back

            left_over_task
        };

        thread_with_smallest_wrkld.wrkld += task_to_assign.get_est_wrkld();
        c_data.crit_sect_state.assigned_tot_wrkld += thread_with_smallest_wrkld.wrkld;
        assign_task_to_thread(thread_with_smallest_wrkld.t_id, task_to_assign, t_args.loc_tinfo_table);

        t_wrkld_min_heap.push(Reverse(thread_with_smallest_wrkld)); // Put back into min-heap
    }

    // Could be made nicer...
    for no_item_task in tasks_with_no_items
    {
        c_data.task_data.unassigned_tasks.add_new_unassigned_task(no_item_task);
    }
}

fn should_steal_tasks_from_other_threads(curr_thread_wrkld: TaskTime, avg_thread_wrkld: TaskTime) -> bool
{
    // The threshold is probably not going to work very good.
    // Should really change this...
    let trgt_task_wrkld = calculate_target_task_wrkld_size(avg_thread_wrkld);

    // Don't start stealing until the sched has enough work
    if trgt_task_wrkld < SHOULD_TAKE_TASKS_THRESHOLD_NS { return false; }

    let max_wrkld_thresh_req_for_stealing = (trgt_task_wrkld as f32 * SHOULD_STEAL_IF_T_WRKLD_BELOW_TRGT_RATIO) as TaskTime;
    curr_thread_wrkld < max_wrkld_thresh_req_for_stealing
}

fn calculate_target_task_wrkld_size(avg_thread_wrkld: TaskTime) -> TaskTime
{
    avg_thread_wrkld
}

fn process_crit_sect_msgs_and_term_sched_on_err<F>(t_args: &ThreadArgs, c_data: &mut CritSectData, proc_crit_msgs_func: F)
    where F: Fn(&ThreadArgs, &mut CritSectData) -> Result<(), SchedulerInternalError>
{
    let res = proc_crit_msgs_func(t_args, c_data);
    term_sched_if_err(res, t_args);
}

fn process_any_crit_sect_msgs_no_block(t_args: &ThreadArgs, c_data: &mut CritSectData) -> Result<(), SchedulerInternalError>
{
    let mut num_client_msgs_proced = 0;
    while let Some(msg) = get_msg_from_channel_no_block_and_no_disconn_check(&c_data.crit_msgs_rx)
    {
        process_crit_msg(msg, &mut num_client_msgs_proced, t_args, c_data)?;
    }

    Ok(())
}

fn process_any_crit_sect_msgs_block_until_one_msg_recvd(t_args: &ThreadArgs, c_data: &mut CritSectData) -> Result<(), SchedulerInternalError>
{
    let mut num_client_msgs_proced = 0;
    let mut proced_one_msg = false;
    while let Some(msg) = get_msg_from_chan_and_block_if_cond_met(&c_data.crit_msgs_rx, || proced_one_msg == true)
    {
        process_crit_msg(msg, &mut num_client_msgs_proced, t_args, c_data)?;
        proced_one_msg = true;
    }

    send_num_client_msgs_processed_msg_if_we_processed_any(t_args, &mut num_client_msgs_proced);
    Ok(())
}

fn process_any_crit_sect_msgs_with_block_until_cond_met(t_args: &ThreadArgs, c_data: &mut CritSectData) -> Result<(), SchedulerInternalError>
{
    while c_data.crit_sect_state.should_always_block_when_procing_crit_msgs
    {
        let mut num_client_msgs_proced = 0;
        let msg = c_data.crit_msgs_rx.recv().unwrap(); // This should never disconn happen and is a bug if it does.
        process_crit_msg(msg, &mut num_client_msgs_proced, t_args, c_data)?;
        send_num_client_msgs_processed_msg_if_we_processed_any(t_args, &mut num_client_msgs_proced);
    }

    Ok(())
}

fn process_crit_msg(msg: CritMessage, num_client_msgs_proced: &mut usize, t_args: &ThreadArgs, c_data: &mut CritSectData) -> Result<(), SchedulerInternalError>
{
    trace!("Processing a {:?} msg...", msg);
    match msg
    {
        CritMessage::Client(msg) => process_client_crit_msg(t_args, c_data, msg, num_client_msgs_proced)?,
        CritMessage::Internal(msg) => process_intern_crit_msg(t_args, c_data, msg)?
    }

    Ok(())
}

fn process_client_crit_msg(t_args: &ThreadArgs, c_data: &mut CritSectData, msg: ClientCritMsg, num_client_msgs_proced: &mut usize) -> Result<(), SchedulerInternalError>
{
    *num_client_msgs_proced += 1;
    match msg
    {
        ClientCritMsg::RegisterTask(task_reg_info, task_id) => register_task_internal(t_args, c_data, task_reg_info, task_id),
        ClientCritMsg::UnregisterTask(task_id) => unregister_task_internal(&mut c_data.task_data.task_state, task_id)?,
        ClientCritMsg::RegTaskToRunCond(task_id, run_cond_id) => c_data.task_data.task_state.register_task_to_run_condition(task_id, run_cond_id)?,
        ClientCritMsg::RegisterStaticRunCond(run_cond_new_func) => c_data.task_data.task_state.register_static_run_condition_type(run_cond_new_func),
        ClientCritMsg::RegisterDynamicRunCond(run_cond_new_func) => c_data.task_data.task_state.register_dynamic_run_condition_type(run_cond_new_func),
        ClientCritMsg::Unpause => unpause_sched_internal(t_args, c_data),
        ClientCritMsg::Pause => pause_scheduler_and_send_buffered_client_msgs_proced_early(t_args, c_data, num_client_msgs_proced)?,
        ClientCritMsg::Terminate => term_sched_internal(t_args, c_data)?
    }

    Ok(())
}

fn pause_scheduler_and_send_buffered_client_msgs_proced_early(t_args: &ThreadArgs, c_data: &mut CritSectData, num_client_msgs_proced: &mut usize) -> Result<(), SchedulerInternalError>
{
    send_num_client_msgs_processed_msg_if_we_processed_any(t_args, num_client_msgs_proced);
    *num_client_msgs_proced = 0;
    pause_all_threads_and_wait_for_any_new_msgs(t_args, c_data)
}

fn process_intern_crit_msg(t_args: &ThreadArgs, c_data: &mut CritSectData, msg: InternalCritMsg) -> Result<(), SchedulerInternalError>
{
    match msg
    {
        InternalCritMsg::TaskSplit(task_idx) => c_data.task_data.task_state.handle_task_split(task_idx),
        InternalCritMsg::TaskStarted(task_idx) => c_data.task_data.task_state.notify_task_starting(task_idx),
        InternalCritMsg::TaskCompleted(task_idx, task_est_wrkld) => process_completed_task(c_data, task_est_wrkld, task_idx),
        InternalCritMsg::AvgTaskItemTimeUpdate(task_idx, avg_time_per_item) => c_data.task_data.task_state.handle_avg_time_update_per_task(task_idx, avg_time_per_item),
        InternalCritMsg::ThreadPaused => handle_intern_thread_paused_msg(t_args, &mut c_data.crit_sect_state),
        InternalCritMsg::ThreadUnpaused => handle_intern_thread_unpaused_msg(t_args, &mut c_data.crit_sect_state),
        InternalCritMsg::ThreadTerminated => handle_intern_thread_terminated(t_args, &mut c_data.crit_sect_state, t_args.sched_spec_state),
        InternalCritMsg::TaskPanicked(task_idx) => handle_task_panic(task_idx, t_args)
    }

    Ok(())
}

fn handle_intern_thread_paused_msg(t_args: &ThreadArgs, c_state: &mut CritSectState)
{
    c_state.num_threads_paused += 1;
    trace!("Remaining unpaused threads: {}", c_state.num_threads - c_state.num_threads_paused);

    if c_state.num_threads_paused == c_state.num_threads - 1 && t_args.sched_spec_state.is_paused()
    {
        debug!("All threads but crit sect thread paused");
        send_outbnd_msg_and_handle_if_chnl_closed(t_args, SchedOutboundMsg::Paused);
        c_state.should_always_block_when_procing_crit_msgs = false;
    }
}

fn handle_intern_thread_unpaused_msg(t_args: &ThreadArgs, c_state: &mut CritSectState)
{
    c_state.num_threads_paused -= 1;
    trace!("Remaining paused threads: {}", c_state.num_threads_paused);

    if c_state.num_threads_paused == c_state.num_threads && !t_args.sched_spec_state.is_paused()
    {
        debug!("All threads unpaused");
        send_outbnd_msg_and_handle_if_chnl_closed(t_args, SchedOutboundMsg::Started);
        c_state.should_always_block_when_procing_crit_msgs = false;
    }
}

fn handle_intern_thread_terminated(t_args: &ThreadArgs, c_state: &mut CritSectState, s_state: &SchedulerSpecificState)
{
    c_state.num_threads_terminated += 1;
    trace!("Waiting on {} threads to terminate...", c_state.num_threads - (c_state.num_threads_terminated + 1)); // Crit sect thread is "kind of" terminated at this point

    if c_state.num_threads_terminated == c_state.num_threads - 1 && s_state.is_terminated()
    {
        // Allow the crit thread to stop blocking on msgs and term itself
        c_state.should_always_block_when_procing_crit_msgs = false;

        // Gross hack. Replace once we have dynamic threads
        let t_alive_update = OtherThreadUpdateInfo::ThreadAliveChange(-(c_state.num_threads as i32) + 1);
        let t_alive_update_msg = WorkingThreadSchedStateUpdate::OtherThreadStateUpdate(t_alive_update);
        t_args.loc_tinfo_table[t_args.t_id].task_info.t_specific_msgs.send(WorkingThreadMsg::StateUpdate(t_alive_update_msg)).unwrap();
    }
}

fn handle_task_panic(task_idx: usize, t_args: &ThreadArgs)
{
    terminate_sched_and_setup_error_state(t_args, SchedulerInternalError::TaskPanicked(task_idx));
}

fn send_num_client_msgs_processed_msg_if_we_processed_any(t_args: &ThreadArgs, num_client_msgs_proced: &mut usize)
{
    if *num_client_msgs_proced > 0
    {
        trace!("Sending msg that {} msgs were processed", num_client_msgs_proced);
        send_outbnd_msg_and_handle_if_chnl_closed(t_args, SchedOutboundMsg::MsgsProcessed(*num_client_msgs_proced));
    }
}

fn post_proc_crit_msgs_checks(t_args: &ThreadArgs, c_data: &mut CritSectData, post_data: PostCritProcData)
{
    if post_data.prev_avg_t_wrkld != c_data.crit_sect_state.assigned_tot_wrkld
    {
        let avg_wrkld_update_msg = OtherThreadUpdateInfo::AvgWrkldUpdate(c_data.crit_sect_state.get_avg_thread_wrkld());
        send_msg_to_all_threads(t_args.loc_tinfo_table, WorkingThreadSchedStateUpdate::OtherThreadStateUpdate(avg_wrkld_update_msg));
    }
}

fn execute_task_run_func_and_update_time_per_item(crit_msgs: &CritMsgsTx, task: ThreadTask)
{
    let task_start_time = cpu_time::ThreadTime::now();
    
    if let Err(_) = execute_task_run_func_and_report_if_task_panicked(&task)
    {
        crit_msgs.notify_task_panicked(task.task_idx)
    }
    
    let tot_thread_time = task_start_time.elapsed().as_nanos() as f32;

    let time_per_item = (tot_thread_time / task.num_items_to_proc as f32) as TaskTime;
    crit_msgs.send_update_avg_task_item_time_msg(task.task_idx, time_per_item);
}

fn execute_task_run_func_and_report_if_task_panicked(task: &ThreadTask) -> Result<(), ()>
{
    let wrapped_task = panic::AssertUnwindSafe(&task);
    panic::catch_unwind(||
    {
        (wrapped_task.run_func)(wrapped_task.get_wrk_req())
    }).map_err(|_| ())
}

fn terminate_sched_and_setup_error_state(t_args: &ThreadArgs, err: SchedulerInternalError)
{
    t_args.sched_spec_state.set_term_state();
    t_args.sched_spec_state.set_scheduler_error(err);
    t_args.private_tinfo.crit_msg_tx.send_termination_message();
    send_outbnd_msg_and_handle_if_chnl_closed(t_args, SchedOutboundMsg::Errored(err));
}

fn send_outbnd_msg_and_handle_if_chnl_closed(t_args: &ThreadArgs, outbnd_msg: SchedOutboundMsg)
{
    if let Err(_) = t_args.outbnd_msgs.send(outbnd_msg)
    {
        // Note: We can't call terminate_sched_and_setup_error_state since it will try sending another msg on the closed channel
        t_args.sched_spec_state.set_term_state();
        set_error_state_for_client_channel_closed(t_args, &outbnd_msg);
    }
}

fn send_outbnd_msg_and_handle_if_chnl_closed_but_dont_term(t_args: &ThreadArgs, outbnd_msg: SchedOutboundMsg)
{
    if let Err(_) = t_args.outbnd_msgs.send(outbnd_msg)
    {
        set_error_state_for_client_channel_closed(t_args, &outbnd_msg);
    }
}

fn set_error_state_for_client_channel_closed(t_args: &ThreadArgs, outbnd_msg: &SchedOutboundMsg)
{
    error!("Scheduler tried sending out a client message but the channel was closed! (msg: {:#?})", outbnd_msg);
    t_args.sched_spec_state.set_scheduler_error(SchedulerInternalError::ClientChannelClosed);
}

fn process_completed_static_run_cond_msgs_and_upgrade_tasks(c_data: &mut CritSectData)
{
    trace!("Processing any completed static run conditions");
    while !c_data.task_data.task_state.run_cond_cb_msg_queues.stat_msg_queue.is_empty()
    {
        // Guaranteed to not be empty (and no PopError) since only one thread is popping
        let task_idx = c_data.task_data.task_state.run_cond_cb_msg_queues.stat_msg_queue.pop_front().unwrap();

        c_data.task_data.task_state.task_rem_run_conds_counts[task_idx].num_static -= 1;
        c_data.task_data.task_state.update_task_status_if_needed(task_idx);
        add_task_to_unass_queue_if_runnable(task_idx, c_data);
    }
}

fn process_completed_dynamic_run_conds_and_upgrade_tasks(c_data: &mut CritSectData)
{
    trace!("Processing any completed dynamic run conditions");
    while !c_data.task_data.task_state.run_cond_cb_msg_queues.dyn_met_msg_queue.is_empty() ||
          !c_data.task_data.task_state.run_cond_cb_msg_queues.dyn_unmet_msg_queue.is_empty()
    {
        while !c_data.task_data.task_state.run_cond_cb_msg_queues.dyn_unmet_msg_queue.is_empty()
        {
            let task_idx = c_data.task_data.task_state.run_cond_cb_msg_queues.dyn_unmet_msg_queue.pop_front().unwrap();
            c_data.task_data.task_state.task_rem_run_conds_counts[task_idx].num_dynamic += 1;
        }

        while !c_data.task_data.task_state.run_cond_cb_msg_queues.dyn_met_msg_queue.is_empty()
        {
            let task_idx = c_data.task_data.task_state.run_cond_cb_msg_queues.dyn_met_msg_queue.pop_front().unwrap();
            c_data.task_data.task_state.task_rem_run_conds_counts[task_idx].num_dynamic -= 1;

            c_data.task_data.task_state.update_task_status_if_needed(task_idx);
            add_task_to_unass_queue_if_runnable(task_idx, c_data);
        }
    }
}

fn add_task_to_unass_queue_if_runnable(task_idx: usize, c_data: &mut CritSectData)
{
    if c_data.task_data.task_state.get_task_status(task_idx) == TaskStatus::Runnable
    {
        let task = create_thread_task_from_task_state(&c_data.task_data.task_state, task_idx);
        c_data.task_data.unassigned_tasks.add_new_unassigned_task(task);
    
        c_data.task_data.task_state.notify_task_scheduled(task_idx);
    }
}

fn create_thread_task_from_task_state(state: &TaskState, task_idx: usize) -> ThreadTask
{
    let task_funcs = state.get_task_funcs(task_idx);
    let num_items_in_task = (task_funcs.get_num_items_func)();

    ThreadTask
    {
        task_idx: task_idx,
        run_func: task_funcs.run_func.clone(),
        num_items_to_proc: num_items_in_task,
        est_time_per_item: state.get_curr_avg_time_per_task_item(task_idx),
        start_idx: 0
    }
}

fn assign_new_task_to_curr_thread(t_args: &ThreadArgs, task: ThreadTask)
{
    t_args.get_thread_active_info().task_info.add_est_workload_ns(task.get_est_wrkld());
    t_args.private_tinfo.assigned_tasks_queue.push(task);
}

fn register_task_internal(t_args: &ThreadArgs, c_data: &mut CritSectData, reg_info: TaskRegInfo, tsk_id: usize)
{
    let sched_paused = t_args.sched_spec_state.is_paused();
    c_data.task_data.task_state.register_task(reg_info, tsk_id, sched_paused);
    
    if !sched_paused
    {
        // Since the sched is running and we have no run conds for this task, it should currently run indefinitely and needs to be scheduled.
        bootstrap_task_with_no_run_conds(c_data, tsk_id);
    }
}

fn unregister_task_internal(tsk_state: &mut TaskState, task_id: usize) -> Result<(), SchedulerInternalError>
{
    tsk_state.unregister_task(task_id)
}

fn unpause_sched_internal(t_args: &ThreadArgs, c_data: &mut CritSectData)
{
    if !t_args.sched_spec_state.is_paused()
    {
        warn!("Got a unpause message while the scheduler is already running!");
        return;
    }

    trace!("Preparing to alert threads to unpause...");
    t_args.sched_spec_state.set_sched_pause_state(false);
    send_msg_to_all_threads(t_args.loc_tinfo_table, WorkingThreadSchedStateUpdate::Unpause);

    // Collect feels wrong, but just to get this working...
    for tsk_id in c_data.task_data.task_state.get_task_ids_with_no_run_conds_that_need_bootstrap().collect::<Vec<_>>()
    {
        bootstrap_task_with_no_run_conds(c_data, tsk_id);
    }
}

fn term_sched_internal(t_args: &ThreadArgs, c_data: &mut CritSectData) -> Result<(), SchedulerInternalError>
{
    info!("Beginning scheduler termination...");

    c_data.crit_sect_state.should_always_block_when_procing_crit_msgs = true;
    t_args.sched_spec_state.set_term_state();
    send_msg_to_all_threads(t_args.loc_tinfo_table, WorkingThreadSchedStateUpdate::Terminate);

    process_any_crit_sect_msgs_with_block_until_cond_met(t_args, c_data)?;
    
    info!("All scheduler threads except crit sect thread have terminated");
    Ok(())
}

fn bootstrap_task_with_no_run_conds(c_data: &mut CritSectData, tsk_id: usize)
{
    trace!("Bootstrapping task id {} because it has no run conditions", tsk_id);
    c_data.task_data.task_state.set_task_status(tsk_id, TaskStatus::Runnable);
    add_task_to_unass_queue_if_runnable(tsk_id, c_data);
}

fn split_task_and_update_task_state(crit_msgs: &CritMsgsTx, task: &mut ThreadTask, split_perc: f32) -> ThreadTask
{
    let new_task = task.split_task(split_perc);
    crit_msgs.notify_task_split(new_task.task_idx);
    new_task
}

fn pause_all_threads_and_wait_for_any_new_msgs(t_args: &ThreadArgs, c_data: &mut CritSectData) -> Result<(), SchedulerInternalError>
{
    if t_args.sched_spec_state.is_paused()
    {
        warn!("Got a pause message while the scheduler is already paused!");
        return Ok(());
    }

    t_args.sched_spec_state.set_sched_pause_state(true);
    c_data.crit_sect_state.should_always_block_when_procing_crit_msgs = true;
    send_msg_to_all_threads(t_args.loc_tinfo_table, WorkingThreadSchedStateUpdate::Pause);

    // We need one thread to remain active while paused and continually process any new messages
    // We will pretend that it is paused :)
    process_crit_sect_msgs_and_term_sched_on_error_block_on_cond(t_args, c_data);

    Ok(())
}

fn assign_task_to_thread(t_id: usize, task: ThreadTask, pub_thread_info: &Vec<LocalThreadInfo>)
{
    let t_info = &pub_thread_info[t_id].task_info;
    t_info.add_est_workload_ns(task.get_est_wrkld());
    t_info.t_specific_msgs.send(WorkingThreadMsg::TaskAssigned(task)).unwrap();
}

fn send_msg_to_all_threads(pub_thread_info: &Vec<LocalThreadInfo>, msg: WorkingThreadSchedStateUpdate)
{
    for t_info in pub_thread_info.iter()
    {
        t_info.task_info.t_specific_msgs.send(WorkingThreadMsg::StateUpdate(msg)).unwrap();
    }
}

fn get_msg_from_channel_no_block_and_no_disconn_check<T>(chan: &Receiver<T>) -> Option<T>
{
    match chan.try_recv()
    {
        Ok(val) => Some(val),
        Err(err) =>
        {
            match err
            {
                TryRecvError::Empty => None,
                TryRecvError::Disconnected => panic!(err)
            }
        }
    }
}

#[cfg(test)]
mod scheduler_tests
{
    use log::{trace, error};
    use std::cmp::max;
    use std::time::{Duration, Instant};
    use std::sync::{Arc, Mutex};

    use std::collections::HashMap;

    use crate::pecs_core::scheduler::{Scheduler, SchedulerInitInfo, SchedOutboundMsg, SchedulerResult, TaskRegistration, TaskRegInfo};
    use crate::pecs_core::scheduler::{RcNotificationState, StaticRunCondNewFunc, DynamicRunCondNewFunc, StaticRcRegInfo, DynamicRcRegInfo};
    use crate::pecs_core::run_conditions::run_condition::RunCondition;
    use pecs_common::types::{RunCondId, RunTaskWorkRequest};
    
    const MAX_WAIT_MSEC_FOR_EVT_WAIT_MS: u64 = 500;
    const WAIT_MSEC_FOR_TASK_EXEC_MS: u64 = 500;

    const DEFAULT_RUN_FUNC_ITEMS_PER_TASK: usize = 1;
    const LIGHT_RUN_FUNC_ITEMS_PER_TASK: usize = 100;

    const DEFAULT_RUN_COND_MAX_RUNS: usize = 0;

    const SLEEP_DELAY_PER_TASK_ITEM_MS: u64 = 20;

    struct DummyRunCondTestParams
    {
        max_runs: usize
    }

    impl DummyRunCondTestParams
    {
        fn new() -> DummyRunCondTestParams
        {
            DummyRunCondTestParams
            {
                max_runs: DEFAULT_RUN_COND_MAX_RUNS
            }
        }

        fn set_max_runs(mut self, max_runs: usize) -> Self
        {
            self.max_runs = max_runs;
            self
        }
    }

    struct DummyRunCondCommonParams
    {
        runs_rem_for_tasks: HashMap<usize, usize>,
        max_runs_per_task: usize
    }

    impl DummyRunCondCommonParams
    {
        fn new(t_prms: &DummyRunCondTestParams) -> DummyRunCondCommonParams
        {
            DummyRunCondCommonParams
            {
                runs_rem_for_tasks: HashMap::new(),
                max_runs_per_task: t_prms.max_runs
            }
        }

        fn check_if_task_can_run(&mut self, task_id: usize, _: &mut RcNotificationState) -> bool
        {
            let num_runs_rem = self.runs_rem_for_tasks.get_mut(&task_id).unwrap();
            trace!("Procing Task {} completed... (Runs rem: {})", task_id, *num_runs_rem);
            let mut should_run_again = false;

            if *num_runs_rem > 0
            {
                trace!("Num runs rem is > 0. Notifying!");
                should_run_again = true;
                *num_runs_rem -= 1;
            }

            return should_run_again
        }
    }

    struct DummyStaticRunCond
    {
        cmn: DummyRunCondCommonParams,
        reg_info: StaticRcRegInfo
    }

    impl RunCondition for DummyStaticRunCond
    {
        fn reg_task(&mut self, task_id: usize, notif_state: &mut RcNotificationState)
        {
            self.cmn.runs_rem_for_tasks.insert(task_id, self.cmn.max_runs_per_task);
            self.check_if_task_can_run_and_notify(task_id, notif_state);
        }

        fn unreg_task(&mut self, task_id: usize)
        {
            
        }

        fn task_completed(&mut self, task_id: usize, notif_state: &mut RcNotificationState)
        {
            self.check_if_task_can_run_and_notify(task_id, notif_state);
        }
    }

    impl DummyStaticRunCond
    {
        fn check_if_task_can_run_and_notify(&mut self, task_id: usize, notif_state: &mut RcNotificationState)
        {
            if self.cmn.check_if_task_can_run(task_id, notif_state)
            {
                self.reg_info.notify_static_rc_conds_met(task_id, notif_state);
            }
        }
    }

    fn create_static_rc_new_func(t_prms: DummyRunCondTestParams) -> StaticRunCondNewFunc
    {
        Box::new(move |cb_reg, stat_rc_reg_info|
        {
            cb_reg.reg_task_completed_callback();

            Box::new(DummyStaticRunCond
            {
                cmn: DummyRunCondCommonParams::new(&t_prms),
                reg_info: stat_rc_reg_info
            })
        })
    }

    fn create_dynamic_rc_new_func(t_prms: DummyRunCondTestParams) -> DynamicRunCondNewFunc
    {
        Box::new(move |cb_reg, dyn_rc_reg_info|
        {
            Box::new(DummyDynamicRunCond
            {
                cmn: DummyRunCondCommonParams::new(&t_prms),
                reg_info: dyn_rc_reg_info
            })
        })
    }

    struct DummyDynamicRunCond
    {
        cmn: DummyRunCondCommonParams,
        reg_info: DynamicRcRegInfo
    }

    impl RunCondition for DummyDynamicRunCond
    {
        fn reg_task(&mut self, task_id: usize, notif_state: &mut RcNotificationState)
        {

        }

        fn unreg_task(&mut self, task_id: usize)
        {

        }

        fn task_completed(&mut self, task_id: usize, notif_state: &mut RcNotificationState)
        {

        }
    }

    #[derive(Clone, Debug)]
    struct RunDebugFuncSharedInfo
    {
        tot_run_count: u64,
        tot_items_proced: usize,
        curr_threads_running: usize,
        max_threads_that_ran_task_concurrently: usize
    }

    impl RunDebugFuncSharedInfo
    {
        fn multiple_threads_ran_same_run_func_simult(&self) -> bool
        {
            self.max_threads_that_ran_task_concurrently > 1
        }
    }

    struct RegRunFuncInfo
    {
        items_per_task: usize,
        sleep_delay_per_task: u64,
        is_panic_task: bool
    }

    impl RegRunFuncInfo
    {
        fn new() -> RegRunFuncInfo
        {
            RegRunFuncInfo
            {
                items_per_task: DEFAULT_RUN_FUNC_ITEMS_PER_TASK,
                sleep_delay_per_task: 0,
                is_panic_task: false
            }
        }

        fn set_items_per_task(mut self, items_per_task: usize) -> RegRunFuncInfo
        {
            self.items_per_task = items_per_task;
            self
        }

        fn set_sleep_delay_per_item(mut self, delay: u64) -> RegRunFuncInfo
        {
            self.sleep_delay_per_task = delay;
            self
        }

        fn set_is_panic_task(mut self) -> RegRunFuncInfo
        {
            self.is_panic_task = true;
            self
        } 
    }

    impl RunDebugFuncSharedInfo
    {
        fn new() -> RunDebugFuncSharedInfo
        {  
            RunDebugFuncSharedInfo
            {
                tot_run_count: 0,
                tot_items_proced: 0,
                curr_threads_running: 0,
                max_threads_that_ran_task_concurrently: 0,
            }
        }
    }

    pub fn wait_for_specific_event_for_duration(sched: &mut Scheduler, msg: SchedOutboundMsg, dur: Duration, ignore_errs: bool) -> bool
    {
        let mut got_req_msg = false;
        let time_to_give_up_at = Instant::now() + dur;

        while !got_req_msg
        {
            let max_wait_dur = time_to_give_up_at - Instant::now();
            let out_msg_opt = sched.wait_for_event_for_duration(max_wait_dur);

            match out_msg_opt
            {
                Ok(out_msg) =>
                {
                    got_req_msg = msg == out_msg;
                },
                Err(err) =>
                {
                    if !ignore_errs
                    {
                        error!("Got the following error when waiting for a specific event from the scheduler: {}", err);
                    }
                    break;
                }
            }
        }

        got_req_msg
    }

    fn wait_for_all_crit_msgs_to_be_processed(sched: &mut Scheduler) -> SchedulerResult<()>
    {
        sched.wait_for_all_msgs_to_be_processed_for_duration(Duration::from_millis( MAX_WAIT_MSEC_FOR_EVT_WAIT_MS))
    }

    fn wait_for_specific_event(sched: &mut Scheduler, msg: SchedOutboundMsg, ignore_errs: bool) -> bool
    {
        wait_for_specific_event_for_duration(sched, msg, Duration::from_millis(MAX_WAIT_MSEC_FOR_EVT_WAIT_MS), ignore_errs)
    }

    fn sleep_for_duration(time_ms: u64)
    {
        let dur = Duration::from_millis(time_ms);
        std::thread::sleep(dur);
    }

    fn setup_env_logger()
    {
        // Initing more than once causes a failure.
        // Later clean this up.
        let _ =  pretty_env_logger::try_init();
    }

    fn init_scheduler() -> Scheduler
    {
        let sched_init_info = SchedulerInitInfo
        {
            num_task_threads: num_cpus::get()
        };

        // Calling new starts the sched in a paused state
        Scheduler::new(sched_init_info).unwrap()
    }

    fn init_sched_and_logger() -> Scheduler
    {
        setup_env_logger();
        init_scheduler()
    }

    fn init_and_wait_for_paused_event() -> (Scheduler, bool)
    {
        let mut sched = init_sched_and_logger();
        let got_event = wait_for_specific_event(&mut sched, SchedOutboundMsg::Paused, false);

        (sched, got_event)
    }

    fn send_pause_event_and_wait_for_response(sched: &mut Scheduler, ignore_errs: bool) -> bool
    {
        sched.pause().unwrap();
        wait_for_specific_event(sched, SchedOutboundMsg::Paused, ignore_errs)
    }

    #[test]
    fn pausing_should_output_a_pause_event()
    {
        let (_, got_paused_event) = init_and_wait_for_paused_event();
        assert!(got_paused_event);
    }

    #[test]
    fn pausing_should_cause_the_scheduler_to_report_that_its_paused()
    {
        let (sched, _) = init_and_wait_for_paused_event();
        assert!(sched.is_paused());
    }

    #[test]
    fn pausing_should_not_have_raised_an_error()
    {
        let (sched, _) = init_and_wait_for_paused_event();
        assert_eq!(sched.has_errored(), false);
    }

    #[test]
    fn pausing_sending_a_second_pause_msg_should_not_should_not_return_a_second_pause_event()
    {
        let (mut sched, _) = init_and_wait_for_paused_event();
        let got_second_pause_event = send_pause_event_and_wait_for_response(&mut sched, true);
        assert_eq!(got_second_pause_event, false);
    }

    #[test]
    fn pausing_sending_a_second_pause_msg_should_still_leave_the_sched_in_a_paused_state()
    {
        let (mut sched, _) = init_and_wait_for_paused_event();
        sched.pause().unwrap();
        wait_for_all_crit_msgs_to_be_processed(&mut sched).unwrap();

        assert!(sched.is_paused());
    }

    #[test]
    fn pausing_should_not_allow_any_run_funcs_to_run()
    {
        let mut sched = init_sched_and_logger();
        let (_, run_func_debug_info) = reg_run_debug_func(&mut sched, RegRunFuncInfo::new());
        sleep_for_duration(WAIT_MSEC_FOR_TASK_EXEC_MS);

        assert_eq!(run_func_debug_info.lock().unwrap().tot_run_count, 0);
    }

    fn init_sched_term_and_wait_for_term_event() -> (Scheduler, bool)
    {
        let mut sched = init_sched_and_logger();
        let res = sched.terminate();
        let got_event = !res.is_err();

        (sched, got_event)
    }

    #[test]
    fn termination_sending_msgs_after_a_term_msg_is_sent_should_error()
    {
        let (mut sched, _) = init_sched_term_and_wait_for_term_event();
        sched.start().ok();
        wait_for_all_crit_msgs_to_be_processed(&mut sched).unwrap();
        assert!(sched.has_errored())
    }

    #[test]
    fn termination_should_output_a_termination_event()
    {
        let (_, got_event) = init_sched_term_and_wait_for_term_event();
        assert!(got_event);
    }

    #[test]
    fn termination_should_return_an_error_if_a_task_is_registered_after_term_msg_sent()
    {
        let (mut sched, _) = init_sched_term_and_wait_for_term_event();
        reg_run_func_that_reports_run_count_expect_err(&mut sched);
        assert!(sched.has_errored());
    }

    fn reg_run_debug_func(sched: &mut Scheduler, run_func_info: RegRunFuncInfo) -> (Option<usize>, Arc<Mutex<RunDebugFuncSharedInfo>>)
    {
        // For the time being, run funcs don't allow mutation, but we really need a way to verify that run funcs are run for tests
        let debug_info = Arc::new(Mutex::new(RunDebugFuncSharedInfo::new()));
        let debug_info_clone = debug_info.clone();

        let delay_per_item = run_func_info.sleep_delay_per_task;
        let should_panic = run_func_info.is_panic_task;

        let run_func = Box::new(move |wrk_req: RunTaskWorkRequest|
        {
            if should_panic
            {
                panic!("Panic task is panicking!")
            }

            {
                let mut debug_info = debug_info_clone.lock().unwrap();
                debug_info.curr_threads_running += 1;
                debug_info.max_threads_that_ran_task_concurrently = max(debug_info.curr_threads_running, debug_info.max_threads_that_ran_task_concurrently);
            }

            let num_items_to_proc = wrk_req.get_idx_range_and_update_rqst().count();
            sleep_for_duration(delay_per_item * num_items_to_proc as u64);

            let mut debug_info = debug_info_clone.lock().unwrap();

            debug_info.tot_run_count += 1;
            debug_info.tot_items_proced += num_items_to_proc;
            debug_info.curr_threads_running -= 1;
        });

        let get_num_items_func = Box::new(move || run_func_info.items_per_task);

        let reg_info = TaskRegInfo { run_func, get_num_items_func };
        let task_id = sched.register_task(reg_info).ok();

        (task_id, debug_info)
    }

    fn reg_run_func_that_reports_run_count_expect_err(sched: &mut Scheduler)
    {
        reg_run_debug_func(sched, RegRunFuncInfo::new());
    }

    fn reg_run_func_that_reports_run_count_no_err(sched: &mut Scheduler, run_func_info: RegRunFuncInfo) -> (usize, Arc<Mutex<RunDebugFuncSharedInfo>>)
    {
        let (task_id_opt, run_func_debug_info) = reg_run_debug_func(sched, run_func_info);
        (task_id_opt.unwrap(), run_func_debug_info)
    }

    fn init_and_reg_run_cond_and_task(run_func_info: RegRunFuncInfo) -> (Scheduler, usize, RunCondId)
    {
        let (mut sched, _) = init_and_wait_for_paused_event();
        let (task_id, _) = reg_run_func_that_reports_run_count_no_err(&mut sched, run_func_info);
        let run_cond_id = sched.register_static_run_condition(Box::new(create_static_rc_new_func(DummyRunCondTestParams::new()))).unwrap();
        sched.register_task_with_run_condition(run_cond_id, task_id).unwrap();

        (sched, task_id, run_cond_id)
    }

    #[test]
    fn task_registration_should_not_raise_an_error_when_a_task_is_registered_to_a_run_condition()
    {
        let (mut sched, _, _) = init_and_reg_run_cond_and_task(RegRunFuncInfo::new());
        let res = wait_for_all_crit_msgs_to_be_processed(&mut sched);

        assert!(res.is_ok());
        assert!(!sched.has_errored());
    }

    #[test]
    fn task_registration_should_raise_an_error_if_we_add_a_non_existent_run_condition()
    {
        let (mut sched, _) = init_and_wait_for_paused_event();
        let (task_id, _) = reg_run_func_that_reports_run_count_no_err(&mut sched, RegRunFuncInfo::new());
        sched.register_task_with_run_condition(0, task_id).unwrap();
        let res = wait_for_all_crit_msgs_to_be_processed(&mut sched);

        assert!(res.is_err());
        assert!(sched.has_errored());
    }

    #[test]
    fn task_registration_should_raise_an_error_if_we_add_the_same_task_twice_to_a_run_condition()
    {
        let (mut sched, task_id, run_cond_id) = init_and_reg_run_cond_and_task(RegRunFuncInfo::new());
        sched.register_task_with_run_condition(run_cond_id, task_id).unwrap();
        let res = wait_for_all_crit_msgs_to_be_processed(&mut sched);

        assert!(res.is_err());
        assert!(sched.has_errored());
    }

    #[test]
    fn task_registration_should_raise_an_error_if_we_add_a_non_existent_task_id_to_a_run_condition()
    {
        let (mut sched, _) = init_and_wait_for_paused_event();
        let run_cond_id = sched.register_static_run_condition(Box::new(create_static_rc_new_func(DummyRunCondTestParams::new()))).unwrap();
        sched.register_task_with_run_condition(run_cond_id, 0).unwrap();
        let res = wait_for_all_crit_msgs_to_be_processed(&mut sched);

        assert!(res.is_err());
        assert!(sched.has_errored());
    }

    fn init_and_exec_debug_run_func_no_run_conds(run_func_info: RegRunFuncInfo) -> (Scheduler, Arc<Mutex<RunDebugFuncSharedInfo>>)
    {
        init_and_exec_debug_run_func(Vec::new(), Vec::new(), run_func_info)
    }

    fn init_and_exec_debug_run_func_no_run_conds_no_term(run_func_info: RegRunFuncInfo) -> (Scheduler, Arc<Mutex<RunDebugFuncSharedInfo>>)
    {
        init_and_exec_debug_run_func_no_sched_term(Vec::new(), Vec::new(), run_func_info)
    }

    fn init_and_exec_debug_run_func(s_rcs: Vec<StaticRunCondNewFunc>, d_rcs: Vec<DynamicRunCondNewFunc>, run_func_info: RegRunFuncInfo) -> (Scheduler, Arc<Mutex<RunDebugFuncSharedInfo>>)
    {
        let (mut sched, run_func_debug_info) = init_and_exec_debug_run_func_no_sched_term(s_rcs, d_rcs, run_func_info);
        sched.terminate().unwrap();

        (sched, run_func_debug_info)
    }

    fn init_and_exec_debug_run_func_no_sched_term(s_rcs: Vec<StaticRunCondNewFunc>, d_rcs: Vec<DynamicRunCondNewFunc>, run_func_info: RegRunFuncInfo) -> (Scheduler, Arc<Mutex<RunDebugFuncSharedInfo>>)
    {
        let (mut sched, _) = init_and_wait_for_paused_event();
        let (t_id, run_func_debug_info) = reg_run_func_that_reports_run_count_no_err(&mut sched, run_func_info);
        reg_rcs_and_tasks_with_rcs(&mut sched, s_rcs, d_rcs, vec![t_id]);
        sched.start().unwrap();
        wait_for_all_crit_msgs_to_be_processed(&mut sched).unwrap();
        sleep_for_duration(WAIT_MSEC_FOR_TASK_EXEC_MS);

        (sched, run_func_debug_info)
    }

    fn reg_rcs_and_tasks_with_rcs(sched: &mut Scheduler, s_rcs: Vec<StaticRunCondNewFunc>, d_rcs: Vec<DynamicRunCondNewFunc>, task_ids: Vec<usize>)
    {
        let mut rc_ids = Vec::new();

        for new_func in s_rcs
        {
            let rc_id = sched.register_static_run_condition(new_func).unwrap();
            rc_ids.push(rc_id);
        }

        for new_func in d_rcs
        {
            let rc_id = sched.register_dynamic_run_condition(new_func).unwrap();
            rc_ids.push(rc_id);
        }

        for t_id in task_ids
        {
            for rc_id in rc_ids.iter()
            {
                sched.register_task_with_run_condition(*rc_id, t_id).unwrap();
            }
        }
    }

    #[test]
    fn running_a_single_task_should_run_a_task_with_no_run_conditions_indefinitely()
    {
        let (_, run_func_debug_info) = init_and_exec_debug_run_func_no_run_conds(RegRunFuncInfo::new());
        let tot_run_count = run_func_debug_info.lock().unwrap().tot_run_count;

        assert!(tot_run_count > 1);
    }

    #[test]
    fn running_a_single_task_with_only_one_item_should_not_be_given_to_multiple_threads_simultaneously()
    {
        let (_, run_func_debug_info) = init_and_exec_debug_run_func_no_run_conds(RegRunFuncInfo::new());
        assert_eq!(run_func_debug_info.lock().unwrap().multiple_threads_ran_same_run_func_simult(), false);
    }

    #[test]
    fn running_a_single_task_should_not_run_the_task_if_a_static_run_condition_is_not_satisfied()
    {
        let s_new_func = create_static_rc_new_func(DummyRunCondTestParams::new());
        let s_rcs: Vec<StaticRunCondNewFunc> = vec![Box::new(s_new_func)];
        let (_, run_func_debug_info) = init_and_exec_debug_run_func(s_rcs, Vec::new(), RegRunFuncInfo::new());
        assert_eq!(run_func_debug_info.lock().unwrap().tot_run_count, 0);
    }
    
    #[test]
    fn running_a_single_task_should_not_run_the_task_if_a_dynamic_run_condition_is_not_satisfied()
    {
        let d_new_func = create_dynamic_rc_new_func(DummyRunCondTestParams::new());
        let d_rcs: Vec<DynamicRunCondNewFunc> = vec![d_new_func];
        let (_, run_func_debug_info) = init_and_exec_debug_run_func(Vec::new(), d_rcs, RegRunFuncInfo::new());
        assert_eq!(run_func_debug_info.lock().unwrap().tot_run_count, 0);
    }
    
    #[test]
    fn running_a_single_task_should_process_all_items_in_a_task()
    {
        let run_func_info = RegRunFuncInfo::new()
            .set_items_per_task(LIGHT_RUN_FUNC_ITEMS_PER_TASK);
        
        let dummy_test_parms = DummyRunCondTestParams::new()
            .set_max_runs(1);
        let s_new_func = create_static_rc_new_func(dummy_test_parms);

        let (_, run_func_debug_info) = init_and_exec_debug_run_func(vec![s_new_func], Vec::new(), run_func_info);
        assert_eq!(run_func_debug_info.lock().unwrap().tot_items_proced, LIGHT_RUN_FUNC_ITEMS_PER_TASK);
    }
    
    #[test]
    fn running_a_single_task_should_not_run_a_task_with_no_items()
    {
        let (_, run_func_debug_info) = init_and_exec_debug_run_func_no_run_conds(RegRunFuncInfo::new().set_items_per_task(0));
        let tot_run_count = run_func_debug_info.lock().unwrap().tot_run_count;

        assert_eq!(tot_run_count, 0);
    }

    #[test]
    fn running_a_single_task_should_schedule_all_threads_to_a_very_heavy_task()
    {
        let run_func_info = RegRunFuncInfo::new()
            .set_items_per_task(LIGHT_RUN_FUNC_ITEMS_PER_TASK)
            .set_sleep_delay_per_item(SLEEP_DELAY_PER_TASK_ITEM_MS);

        let (_, run_func_debug_info) = init_and_exec_debug_run_func_no_run_conds(run_func_info);
        let max_active_threads = run_func_debug_info.lock().unwrap().max_threads_that_ran_task_concurrently;

        assert_eq!(num_cpus::get(), max_active_threads);
    }

    #[test]
    fn running_a_single_task_should_terminate_the_scheduler_gracefully_and_return_an_error_if_a_task_run_func_panics()
    {
        let run_func_info = RegRunFuncInfo::new()
            .set_is_panic_task();

        let (sched, _) = init_and_exec_debug_run_func_no_run_conds_no_term(run_func_info);
        assert!(sched.has_errored());
    }

    #[ignore]
    #[test]
    fn multiple_tasks_should_process_the_run_functions_of_multiple_tasks()
    {
        assert!(false);
    }

    #[ignore]
    #[test]
    fn multiple_tasks_should_allow_independent_tasks_to_run_concurrently()
    {
        assert!(false);
    }

    #[ignore]
    #[test]
    fn run_conditions_should_notify_all_registered_run_conditions_of_relevant_task_events()
    {
        assert!(false);
    }

    #[ignore]
    #[test]
    fn run_conditions_should_not_notify_run_conditions_that_of_tasks_that_have_become_unregistered()
    {
        assert!(false);
    }

    #[ignore]
    #[test]
    fn run_conditions_should_schedule_a_task_when_its_only_run_cond_static_is_satisfied()
    {
        assert!(false);
    }

    #[ignore]
    #[test]
    fn run_conditions_should_schedule_a_task_when_its_only_run_cond_dynamic_is_satisfied()
    {
        assert!(false);
    }

    #[ignore]
    #[test]
    fn run_conditions_should_not_schedule_a_task_when_all_static_run_conds_are_satisfied_but_not_all_dynamic_run_conds_are()
    {
        assert!(false);
    }

    #[ignore]
    #[test]
    fn run_conditions_should_not_schedule_a_task_when_all_dynamic_are_satisfied_but_not_all_static_ones_are()
    {
        assert!(false);
    }
}
