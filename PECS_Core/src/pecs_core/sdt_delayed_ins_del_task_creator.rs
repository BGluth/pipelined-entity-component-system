use crate::pecs_core::scheduler::Scheduler;

struct DelayedInsDelTaskCreator<'a>
{
    sched: &'a Scheduler
}

impl<'a> DelayedInsDelTaskCreator<'a>
{
    pub fn new() -> DelayedInsDelTaskCreator<'a>
    {
        unimplemented!();
    }

    pub fn sdt_unable_to_insert_because_of_lock(sdt_id: usize, sect_id: usize, ins_delayed_sdt_func: Box<dyn Fn()>)
    {
        unimplemented!();
    }

    pub fn sdt_unable_to_delete_because_of_lock(sdt_id: usize, sect_id: usize, del_delayed_sdt_func: Box<dyn Fn()>)
    {
        unimplemented!();
    }

}

