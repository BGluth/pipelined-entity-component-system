use crate::pecs_core::scheduler::{StaticRcRegInfo, DynamicRcRegInfo};
use crate::pecs_core::sdt_data::SdtData;
use crate::pecs_core::run_conditions::run_condition::{RunCondition, RunCondSchedulerCallbackRegistration};

use crate::pecs_common::types::{CompItemSectIdAndIdx, EID, SectId};

pub type StaticRunCondNewFunc = Box<dyn Fn(&mut dyn RunCondSchedulerCallbackRegistration, StaticRcRegInfo) -> Box<dyn RunCondition + Send> + Send>;
pub type DynamicRunCondNewFunc = Box<dyn Fn(&mut dyn RunCondSchedulerCallbackRegistration, DynamicRcRegInfo) -> Box<dyn RunCondition + Send> + Send>;

pub enum RunConditionType
{
    Static,
    Dynamic
}

pub type MoveFunc = dyn Fn(SdtData, EID, CompItemSectIdAndIdx, SectId);
pub type MoveFuncId = usize;