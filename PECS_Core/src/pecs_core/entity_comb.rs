use crate::pecs_core::common::MoveFuncId;
use crate::pecs_core::sdt_move_func_state::SdtMoveFuncState;
use crate::pecs_core::utils::ReuseableIds;
use crate::pecs_core::sdt_sect_item_idx_tracking::{SdtSectItemIdxTracking, EntitySectDelKey, EntitySectInsKey, EntitySectMergeKey, EntitySectSplitKey};

use pecs_common::types::{EID, SdtId, SectId, CombId, CollId, SdtType};

use std::collections::hash_map::HashMap;
use std::ops::Range;

const EMPTY_COLL_IDX: usize = 0;

#[derive(Clone, Debug)]
pub enum SdtInsertChangeOutcome
{
    CombsFormed(MoveFuncId, EntitySectMergeKey),
    NoChange(EntitySectInsKey)
}

#[derive(Clone, Debug)]
pub enum SdtDeleteChangeOutcome
{
    CombsSplit(MoveFuncId, EntitySectSplitKey),
    NoChange(EntitySectDelKey)
}

pub struct CombFormedChange
{
    combs_merged: Vec<CombId>, // TODO: Don't use a vec here!
    new_comb: CombId,
}

pub struct CombSplitChange
{
    old_comb: CombId,
    new_split_combs: Vec<CombId>, // TODO: Don't use a vec here!
}

struct CombState
{
    comb_sdts: Vec<SdtId>,
    comb_end_idxs: Vec<usize>,
    comb_id_to_combs_ltable: Vec<Range<usize>>,

    sect_state: SectState,
}

struct SectState
{
    comb_id_to_sect_id: HashMap<CombId, SectId>,
    next_sect_id: SectId
}

impl SectState
{
    fn new() -> SectState
    {
        SectState
        {
            comb_id_to_sect_id: HashMap::new(),
            next_sect_id: 1, // 0 is the special "unused" SectId
        }
    }

    fn reg_new_sys_comb_as_sect(&mut self, comb_id: CombId)
    {
        let new_sect_id = self.next_sect_id;
        self.next_sect_id += 1;
        self.comb_id_to_sect_id.insert(comb_id, new_sect_id);
    }

    fn get_sect_id_of_comb(&self, comb_id: CombId) -> SectId
    {
        self.comb_id_to_sect_id[&comb_id]
    }
}

impl CombState
{
    fn new() -> CombState
    {
        unimplemented!();
    }

    fn add_comb(&mut self, comb_sdts: Box<[SdtId]>)
    {
        unimplemented!();
    }

    fn get_comb_from_comb_id(&self, comb_id: CombId) -> Combination
    {
        unimplemented!();
    }

    fn get_comb_id_from_sdts(&self, sdts: Box<[SdtId]>) -> SdtId
    {
        unimplemented!();
    }

    fn get_all_combinations<'a>(&'a self) -> impl Iterator<Item=CombinationAndId<'a>>
    {
        // TODO
        std::iter::empty()
    }

    fn get_id_of_comb(&self, comb: &Combination) -> CombId
    {
        unimplemented!();
    }

    fn get_sect_id_for_comb(&self, comb_id: CombId) -> SectId
    {
        self.sect_state.get_sect_id_of_comb(comb_id)
    }

    fn get_sect_and_subsect_ids_for_comb(&self, comb: &Combination) -> Box<[SectId]>
    {
        unimplemented!()
    }
}

struct CombinationAndId<'a>
{
    comb: &'a Combination<'a>,
    comb_id: CombId
}

impl<'a> CombinationAndId<'a>
{
    fn new(comb: &'a Combination, comb_id: CombId) -> CombinationAndId<'a>
    {
        CombinationAndId
        {
            comb,
            comb_id
        }
    }
}

struct CollState
{
    colls: Vec<Collection>,
    colls_prod_from_sdt_changes: CollProdFromSdtChangeLookup,
}

impl CollState
{
    fn new() -> CollState
    {
        unimplemented!();
    }

    fn get_coll(&self, coll_id: CollId) -> &Collection
    {
        unimplemented!();
    }

    fn get_new_coll_info_after_sdt_add<'a>(&'a mut self, sdt_id: SdtId, coll_id: CollId, comb_state: &'a CombState, move_func_state: &mut SdtMoveFuncState) -> NewCollIdAndUpdateInfoFromSdtIns
    {
        // TODO: I feel like we can get by with returning a reference instead...
        if let Some(info) = self.colls_prod_from_sdt_changes.known_colls_from_add[sdt_id].get(&coll_id)
        {
            return info.clone();
        }

        self.compute_and_insert_new_coll_and_move_func_after_sdt_add(coll_id, sdt_id, comb_state, move_func_state);
        self.colls_prod_from_sdt_changes.known_colls_from_add[sdt_id][&coll_id].clone()
    }

    fn get_coll_new_coll_info_after_sdt_del(&mut self, sdt_id: SdtId, coll: CollId) -> Option<NewCollIdAndUpdateInfoFromSdtIns>
    {
        unimplemented!();
    }

    fn compute_and_insert_new_coll_and_move_func_after_sdt_add(&mut self, coll_id: CollId, sdt_id: SdtId, comb_state: &CombState, move_func_state: &mut SdtMoveFuncState)
    {
        let old_coll = self.get_coll(coll_id);
        let mut new_coll = old_coll.clone();
        let empty_sdt_comb_id = comb_state.get_comb_id_from_sdts([sdt_id].into());
        new_coll.add_comb(empty_sdt_comb_id);

        let sect_ids_of_old_coll: Box<[SectId]> = get_sect_ids_of_coll(old_coll, comb_state).collect();
        let mut outcome_info;

        if let Some(merge_info) = find_largest_sys_comb_that_can_be_formed_from_coll_sdts_and_new_sdt(coll_id, sdt_id, empty_sdt_comb_id, comb_state, &self)
        {
            new_coll.merge_combs(&merge_info);
            let comb_formed = comb_state.get_comb_from_comb_id(merge_info.comb_id_formed);
            let sdt_ids_of_comb_formed = comb_formed.get_sdt_slice();

            let merged_sect_ids: Box<[SectId]> = merge_info.comb_ids_merged.into_iter().map(|comb_id| comb_state.sect_state.get_sect_id_of_comb(*comb_id)).collect();
            let new_sect_id = comb_state.sect_state.get_sect_id_of_comb(merge_info.comb_id_formed);
            let move_func_id = move_func_state.get_merge_group_move_func_id(sdt_ids_of_comb_formed, merged_sect_ids, new_sect_id);

            let sect_ids_of_new_coll: Box<[SectId]> = get_sect_ids_of_coll(&new_coll, comb_state).collect();
            let sect_merge_key = SdtSectItemIdxTracking::get_merge_key_for_sect_ids_section(&sect_ids_of_old_coll, &sect_ids_of_new_coll);

            outcome_info = SdtInsertChangeOutcome::CombsFormed(move_func_id, sect_merge_key);
        }
        else
        {
            let unused_comb_id_for_new_sdt = comb_state.get_id_of_comb(&Combination::new(&[sdt_id]));
            let unused_sect_id_for_new_sdt = comb_state.get_sect_id_for_comb(unused_comb_id_for_new_sdt);
            let mut sect_ids_after_ins_vec = sect_ids_of_old_coll.to_vec();
            sect_ids_after_ins_vec.push(unused_sect_id_for_new_sdt);
            let sect_ids_after_ins: Box<[SectId]> = sect_ids_after_ins_vec.into();

            let ins_key = SdtSectItemIdxTracking::get_ins_key_for_sect_ids_section(&sect_ids_of_old_coll, &sect_ids_after_ins);
            outcome_info = SdtInsertChangeOutcome::NoChange(ins_key);
        }

        let new_coll_id = self.add_new_coll_if_not_unique_or_get_id_of_existing(new_coll);
        let cached_info = NewCollIdAndUpdateInfoFromSdtIns
        {
            new_coll_id,
            outcome_info
        };
        self.add_cached_entry_for_sdt_change(new_coll_id, cached_info);
    }

    fn add_new_coll_if_not_unique_or_get_id_of_existing(&mut self, coll: Collection) -> CollId
    {
        unimplemented!();
    }

    fn add_cached_entry_for_sdt_change(&mut self, coll_id: CollId, cached_info: NewCollIdAndUpdateInfoFromSdtIns)
    {
        unimplemented!();
    }
}

pub struct EntityCombs
{
    entity_to_coll_ltable: Vec<CollId>,

    comb_state: CombState,
    coll_state: CollState,

    eid_usage_tracking: ReuseableIds<EID>,

    pub debug_info: CombDebugInfo
}

impl EntityCombs
{
    pub fn new() -> EntityCombs
    {
        unimplemented!();
    }

    pub fn add_sdt_to_entity(&mut self, eid: EID, sdt_id: SdtId, move_func_state: &mut SdtMoveFuncState) -> SdtInsertChangeOutcome
    {
        let e_coll_id = self.entity_to_coll_ltable[eid];
        self.insert_sdt_and_update_coll_info(eid, sdt_id, e_coll_id, move_func_state)
    }

    pub fn del_sdt_from_entity(&mut self, eid: EID, sdt_id: SdtId, move_func_state: &mut SdtMoveFuncState) -> SdtDeleteChangeOutcome
    {
        unimplemented!();
    }

    pub fn create_entity(&mut self) -> EID
    {
        self.eid_usage_tracking.get_next_id()
    }

    pub fn batch_create_entities(&mut self, amt: usize) -> Vec<EID>
    {
        unimplemented!();
    }

    pub fn batch_del_entities(&mut self, eids: impl Iterator<Item=EID>)
    {
        unimplemented!();
    }

    pub fn delete_entity(&mut self, eid: EID)
    {
        self.eid_usage_tracking.notify_id_unused(eid);
    }

    pub fn reg_sdt(&mut self, sdt_id: SdtId)
    {
        unimplemented!();
    }

    pub fn reg_sys_comb(&mut self, sys_comb: Box<[SdtId]>)
    {
        self.comb_state.add_comb(sys_comb)
    }

    pub fn get_sdts_on_entity(&self, eid: EID) -> impl Iterator<Item=SdtId>
    {
        Vec::new().into_iter()
    }

    pub fn get_combs_of_entity(&self, eid: EID) -> impl Iterator<Item=CombId>
    {
        Vec::new().into_iter()
    }

    pub fn get_coll_of_entity(&self, eid: EID) -> CollId
    {
        unimplemented!();
    }

    pub fn get_entity_info(&self, eid: EID) -> EntityInfo
    {
        unimplemented!();
    }

    pub fn entity_exists(&self, eid: EID) -> bool
    {
        unimplemented!();
    }

    pub fn entity_contains_sdt(&self, eid: EID, sdt_id: SdtId) -> bool
    {
        unimplemented!();
    }

    pub fn get_tot_entity_count(&self) -> usize
    {
        unimplemented!();
    }

    pub fn get_num_sdts_on_entity(&self, eid: EID) -> usize
    {
        unimplemented!();
    }

    fn insert_sdt_and_update_coll_info(&mut self, eid: EID, sdt_id: SdtId, coll_id: CollId, move_func_state: &mut SdtMoveFuncState) -> SdtInsertChangeOutcome
    {
        let coll_info = self.coll_state.get_new_coll_info_after_sdt_add(sdt_id, coll_id, &self.comb_state, move_func_state);
        self.entity_to_coll_ltable[eid] = coll_info.new_coll_id;

        // TODO: This extra match is not really needed and probably kind of wasteful...
        coll_info.outcome_info
    }
}

struct CombsMergedInfo
{
    comb_ids_merged: Box<[CombId]>,
    comb_id_formed: CombId,
}

fn find_largest_sys_comb_that_can_be_formed_from_coll_sdts_and_new_sdt(coll_id: CollId, new_sdt: SdtId, new_sdt_empty_comb_id: CombId, comb_state: &CombState, coll_state: &CollState) -> Option<CombsMergedInfo>
{
    // Note: The collections does not yet contain the new sdt.
    let coll_combs: Vec<CombinationAndId> = get_combinations_of_coll(coll_id, coll_state).into_vec();

    let sys_combs: Vec<CombinationAndId> = comb_state.get_all_combinations().collect();
    let sys_combs_containing_new_sdt = sys_combs.into_iter().filter(|c_and_id| c_and_id.comb.contains_sdt(new_sdt));
    let sys_combs_with_coll_sdts = sys_combs_containing_new_sdt
        .filter(|c_and_id_with_new_id| c_and_id_with_new_id.comb.get_sdt_slice().into_iter()
        .all(|sdt| coll_combs.iter().any(|c_and_id| c_and_id.comb.contains_sdt(*sdt))));

    let mut new_combs_formable_after_sdt_add: Vec<CombinationAndId> = sys_combs_with_coll_sdts.collect();
    new_combs_formable_after_sdt_add.sort_unstable_by_key(|c_and_id| c_and_id.comb.num_sdts());

    if new_combs_formable_after_sdt_add.len() == 0 { return None }
    let comb_formed = &new_combs_formable_after_sdt_add[0];

    let mut comb_ids_merged: Vec<CombId> = coll_combs.into_iter()
        .filter(|c_and_id| c_and_id.comb.get_sdt_slice().into_iter()
            .any(|coll_sdt| comb_formed.comb.contains_sdt(*coll_sdt)))
        .map(|c_and_id_merged| c_and_id_merged.comb_id).collect();
    comb_ids_merged.push(new_sdt_empty_comb_id);
    let comb_ids_merged = comb_ids_merged.into();

    let merged_info = CombsMergedInfo { comb_ids_merged, comb_id_formed: comb_formed.comb_id };
    Some(merged_info)
}

fn get_combinations_of_coll(coll_id: CollId, coll_state: &CollState) -> Box<[CombinationAndId]>
{
    unimplemented!();
}

#[derive(Clone, Debug)]
pub struct Collection
{
    comb_ids: Box<[CombId]>,
}

impl Collection
{
    fn new(comb_ids: Box<[CombId]>) -> Collection
    {
        Collection
        {
            comb_ids
        }
    }

    fn add_comb(&mut self, comb_id: CombId)
    {
        unimplemented!();
    }

    fn merge_combs(&mut self, info: &CombsMergedInfo)
    {
        unimplemented!();
    }
}

#[derive(Clone, Debug)]
struct SdtsAndCombs
{
    sdts: Vec<usize>,
    comb_end_idxs: Vec<usize>,
}

impl SdtsAndCombs
{
    fn new() -> SdtsAndCombs
    {
        SdtsAndCombs
        {
            sdts: Vec::new(),
            comb_end_idxs: Vec::new(),
        }
    }

    fn get_sdts(&self) -> impl Iterator<Item=&SdtId>
    {
        self.sdts.iter()
    }

    fn get_combs<'a>(&'a self) -> impl Iterator<Item=Combination<'a>>
    {
        let mut curr_start_idx = 0;

        self.comb_end_idxs.iter()
        .map(move |comb_end_idx|
        {
            let start_idx = curr_start_idx;
            let end_idx = curr_start_idx + *comb_end_idx;
            curr_start_idx = end_idx;

            (&self.sdts[std::ops::Range{ start: start_idx, end: end_idx }]).into()
        })
    }
}

struct Combination<'a>
{
    sdts: &'a [SdtId]
}

impl<'a> Combination<'a>
{
    fn new(sdts: &'a [SdtId]) -> Combination<'a>
    {
        Combination
        {
            sdts
        }
    }

    fn get_sdt_slice(&'a self) -> &'a [SdtId]
    {
        unimplemented!();
    }

    fn get_sdt_iter(&'a self) -> impl Iterator<Item=SdtId> + 'a
    {
        self.sdts.into_iter().cloned()
    }

    fn num_sdts(&self) -> usize
    {
        self.sdts.len()
    }

    fn contains_sdt(&self, sdt_id: SdtId) -> bool
    {
        self.sdts.contains(&sdt_id)
    }
}

impl<'a> From<&'a [SdtId]> for Combination<'a>
{
    fn from(sdts: &'a [SdtId]) -> Self
    {
        Self::new(sdts)
    }
}

struct CollProdFromSdtChangeLookup
{
    known_colls_from_add: Vec<HashMap<SdtId, NewCollIdAndUpdateInfoFromSdtIns>>,
    known_colls_from_del: Vec<HashMap<SdtId, NewCollIdAndUpdateInfoFromSdtDel>>,
}

impl CollProdFromSdtChangeLookup
{
    fn new() -> CollProdFromSdtChangeLookup
    {
        unimplemented!();
    }
}

#[derive(Clone, Debug)]
struct NewCollIdAndUpdateInfoFromSdtIns
{
    new_coll_id: CollId,
    outcome_info: SdtInsertChangeOutcome,
}

#[derive(Clone, Debug)]
struct NewCollIdAndUpdateInfoFromSdtDel
{
    new_coll_id: CollId,
    outcome_info: SdtDeleteChangeOutcome,
}

pub struct CombDebugInfo
{
    sdt_id_to_name: HashMap<SdtId, String>
}

impl CombDebugInfo
{
    pub fn get_sdt_info_for_sdt_id(&self, sdt_id: SdtId) -> SdtInfo
    {
        unimplemented!();
    }

    pub fn get_comb_info_for_comb_id(&self, comb_id: CombId) -> CombInfo
    {
        unimplemented!();
    }

    pub fn get_coll_info_for_coll_id(&self, coll_id: CollId) -> CollInfo
    {
        unimplemented!();
    }
}

pub struct SdtInfo
{
    pub id: SdtId,
    pub name: String,
    pub sdt_type: SdtType
}

pub struct CombInfo
{
    pub id: CombId,
    pub sdts: Vec<SdtInfo>
}

impl CombInfo
{
    pub fn get_name(&self) -> String
    {
        unimplemented!();
    }
}

pub struct CollInfo
{
    pub id: CollId,
    pub combs: Vec<CombInfo>
}

impl CollInfo
{
    pub fn get_name(&self) -> String
    {
        unimplemented!();
    }
}

pub struct EntityInfo
{
    pub eid: EID,
    pub coll_info: CollInfo
}

fn get_sect_ids_of_coll<'a>(coll: &'a Collection, comb_state: &'a CombState) -> impl Iterator<Item=SectId> + 'a
{
    coll.comb_ids.into_iter().map(move |comb_id| comb_state.get_sect_id_for_comb(*comb_id))
}

#[cfg(test)]
mod entity_comb_tests
{
    #[test]
    fn adding_an_sdt_to_an_entity_should_add_the_sdt()
    {
        unimplemented!();
    }

    #[test]
    fn removing_an_sdt_from_an_entity_should_remove_the_sdt()
    {
        unimplemented!();
    }

    #[test]
    fn adding_an_sdt_type_twice_to_an_entity_should_be_ignored()
    {
        unimplemented!();
    }

    #[test]
    fn removing_a_non_existing_sdt_on_an_entity_should_be_ignored()
    {
        unimplemented!();
    }

    #[test]
    fn removing_a_non_existing_entity_should_be_ignored()
    {
        unimplemented!();
    }

    #[test]
    fn adding_an_sdt_that_forms_a_comb_should_form_a_comb()
    {
        unimplemented!();
    }

    #[test]
    fn removing_an_sdt_that_is_part_of_a_comb_should_break_apart_the_comb()
    {
        unimplemented!();
    }

    #[test]
    fn an_entity_with_sdts_that_can_form_multiple_combs_should_form_a_superset_comb()
    {
        unimplemented!();
    }

    #[test]
    fn removing_an_empty_entity_should_succeed()
    {
        unimplemented!();
    }

    #[test]
    fn creating_an_entity_should_inc_the_tot_entity_count()
    {
        unimplemented!();
    }

    #[test]
    fn creating_an_entity_should_report_that_the_eid_exists()
    {
        unimplemented!();
    }

    #[test]
    fn deleting_an_entity_should_report_that_it_no_longer_exists()
    {
        unimplemented!();
    }

    #[test]
    fn adding_an_entity_should_inc_the_tot_entity_count()
    {
        unimplemented!();
    }

    #[test]
    fn removing_an_entity_should_dec_the_tot_entity_count()
    {
        unimplemented!();
    }
}