use crate::pecs_core::common::{StaticRunCondNewFunc, DynamicRunCondNewFunc};
use crate::pecs_common::types::SysId;

pub struct RunCondData
{
    debug_info: RunCondDebugInfo,
}

impl RunCondData
{
    pub fn new() -> RunCondData
    {
        unimplemented!();
    }

    pub fn add_run_cond(&mut self, builder: RunCondBuilder)
    {
        unimplemented!();
    }

    pub fn get_rc_new_funcs() -> RunCondSchedInfo
    {
        unimplemented!();
    }
}

pub struct RunCondBuilder {}

impl RunCondBuilder
{
    pub fn new_static(new_func: StaticRunCondNewFunc) -> RunCondBuilder
    {
        unimplemented!();
    }

    pub fn new_dynamic(new_func: DynamicRunCondNewFunc) -> RunCondBuilder
    {
        unimplemented!();
    }

    pub fn reg_sys_id(self, sys_id: SysId) -> Self
    {
        unimplemented!();
    }

    pub fn name(self, name: String) -> Self
    {
        unimplemented!();
    }
}

pub struct RunCondSchedInfo
{
    pub static_info: Vec<(StaticRunCondNewFunc, SysesRegedToRc)>,
    pub dynamic_info: Vec<(DynamicRunCondNewFunc, SysesRegedToRc)>,
}

pub struct SysesRegedToRc
{
    pub sys_ids: Vec<SysId>,
}

struct RunCondDebugInfo
{
    names: Vec<String>,
}