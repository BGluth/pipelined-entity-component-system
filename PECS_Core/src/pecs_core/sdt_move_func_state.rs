use crate::pecs_common::types::{CompItemSectIdAndIdx, EID, SdtId, SectId, SectItemIdx};
use crate::pecs_core::common::MoveFuncId;
use crate::pecs_core::sdt_data::SdtData;

use std::collections::HashMap;

type TypelessMoveFunc = dyn Fn(&SdtData, EID, CompItemSectIdAndIdx, SectId);
type MergeGroupMoveFunc = dyn Fn(&Vec<Box<TypelessMoveFunc>>, &SdtData, EID, &[SectItemIdx]);
type SplitGroupMoveFunc = dyn Fn(&Vec<Box<TypelessMoveFunc>>, &SdtData, EID, SectItemIdx);

pub struct SdtMoveFuncState
{
    indiv_sdt_typeless_move_funcs: Vec<Box<TypelessMoveFunc>>,
    sdts_to_group_move_funcs: HashMap<Box<[SdtId]>, MoveFuncId>,

    merge_group_move_funcs: Vec<Box<MergeGroupMoveFunc>>,
    split_group_move_funcs: Vec<Box<SplitGroupMoveFunc>>,
}

impl SdtMoveFuncState
{
    pub fn new(num_sdts: usize) -> SdtMoveFuncState
    {
        let mut indiv_sdt_typeless_move_funcs: Vec<Box<TypelessMoveFunc>> = Vec::with_capacity(num_sdts);
        for _ in 0..num_sdts
        {
            indiv_sdt_typeless_move_funcs.push(Box::new(unset_move_func));
        }

        SdtMoveFuncState
        {
            indiv_sdt_typeless_move_funcs: indiv_sdt_typeless_move_funcs,
            sdts_to_group_move_funcs: HashMap::new(),

            merge_group_move_funcs: Vec::new(),
            split_group_move_funcs: Vec::new(),
        }
    }

    pub fn reg_typeless_move_func<T: 'static>(&mut self, sdt_id: SdtId)
    {
        let typeless_move_func  = 
        |sdt_data: &SdtData, eid, prev_sect_id_and_idx, new_sect_id|
        {
            sdt_data.move_sdt_to_new_sect::<T>(eid, prev_sect_id_and_idx, new_sect_id);
        };

        self.indiv_sdt_typeless_move_funcs[sdt_id] = Box::new(typeless_move_func);
    }

    pub fn get_merge_group_move_func_id(&mut self, sdt_ids: &[SdtId], prev_sect_ids: Box<[SectId]>, new_sect_id: SectId) -> MoveFuncId
    {
        match self.sdts_to_group_move_funcs.get(sdt_ids)
        {
            Some(id) => *id,
            None => self.register_new_group_merge_move_func(sdt_ids, prev_sect_ids, new_sect_id)
        }
    }

    pub fn get_split_group_move_func_id(&mut self, sdt_ids: &[SdtId], prev_sect_id: SectId, new_sect_ids: Box<[SectId]>) -> MoveFuncId
    {
        match self.sdts_to_group_move_funcs.get(sdt_ids)
        {
            Some(id) => *id,
            None => self.register_new_group_split_move_func(sdt_ids, prev_sect_id, new_sect_ids)
        }
    }

    pub fn call_merge_group_move_func(&self, move_func_id: MoveFuncId, sdt_data: &SdtData, eid: EID, prev_item_idxs: &[SectItemIdx])
    {
        (self.merge_group_move_funcs[move_func_id])(&self.indiv_sdt_typeless_move_funcs, sdt_data, eid, prev_item_idxs);
    }

    pub fn call_split_group_move_func(&self, move_func_id: MoveFuncId, sdt_data: &SdtData, eid: EID, curr_item_idx: SectItemIdx)
    {
        unimplemented!();
    }

    fn register_new_group_merge_move_func(&mut self, sdt_ids: &[SdtId], prev_sect_ids: Box<[SectId]>, new_sect_id: SectId) -> MoveFuncId
    {
        self.merge_group_move_funcs.push(Self::create_merge_group_move_func(sdt_ids, prev_sect_ids, new_sect_id));
        self.create_and_register_new_group_func_common(sdt_ids)
    }

    fn register_new_group_split_move_func(&mut self, sdt_ids: &[SdtId], prev_sect_id: SectId, new_sect_ids: Box<[SectId]>) -> MoveFuncId
    {
        self.split_group_move_funcs.push(Self::create_split_group_move_func(sdt_ids, prev_sect_id, new_sect_ids));
        self.create_and_register_new_group_func_common(sdt_ids)
    }

    fn create_and_register_new_group_func_common(&mut self, sdt_ids: &[SdtId]) -> MoveFuncId
    {
        let group_func_id = self.merge_group_move_funcs.len();
        self.sdts_to_group_move_funcs.insert(sdt_ids.into(), group_func_id);

        group_func_id
    }

    fn create_merge_group_move_func(sdt_ids: &[SdtId], prev_sect_ids: Box<[SectId]>, new_sect_id: SectId) -> Box<MergeGroupMoveFunc>
    {
        let sdt_ids: Box<[SdtId]> = sdt_ids.into();

        Box::new(move |indiv_sdt_move_funcs: &Vec<Box<TypelessMoveFunc>>, sdt_data: &SdtData, eid, prev_item_idxs|
        {
            for (idx, sdt_id) in sdt_ids.into_iter().enumerate()
            {
                let info = CompItemSectIdAndIdx::new(prev_sect_ids[idx], prev_item_idxs[idx]);
                (indiv_sdt_move_funcs[*sdt_id])(sdt_data, eid, info, new_sect_id);
            }
        })
    }

    fn create_split_group_move_func(sdt_ids: &[SdtId], prev_sect_id: SectId, new_sect_ids: Box<[SectId]>) -> Box<SplitGroupMoveFunc>
    {
        let sdt_ids: Box<[SdtId]> = sdt_ids.into();

        Box::new(move |indiv_sdt_move_funcs: &Vec<Box<TypelessMoveFunc>>, sdt_data: &SdtData, eid, prev_item_idx|
        {
            let info = CompItemSectIdAndIdx::new(prev_sect_id, prev_item_idx);
            for (idx, sdt_id) in sdt_ids.into_iter().enumerate()
            {
                (indiv_sdt_move_funcs[*sdt_id])(sdt_data, eid, info, new_sect_ids[idx]);
            }
        })
    }
}

fn unset_move_func(sdt_data: &SdtData, eid: EID, old: CompItemSectIdAndIdx, new: SectId)
{
    unreachable!();
}