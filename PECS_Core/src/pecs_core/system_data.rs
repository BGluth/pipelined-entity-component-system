use crate::pecs_core::common::MoveFunc;
use crate::pecs_core::world::World;

use pecs_common::types::{RegisteredSDT, SdtId, SysId, SysSdtMutability};

type WrappedRunFunc = dyn Fn();
pub type SysRunFunc<I> = dyn Fn(I);

pub struct SystemData
{
    wrapped_run_funcs: Vec<Box<WrappedRunFunc>>,
    debug_info: SystemDebugInfo,
}   

impl SystemData
{
    pub fn new() -> SystemData
    {
        unimplemented!();
    }

    pub fn reg_sys(&mut self, reg_builder: SystemRegBuilder) -> SysId
    {
        unimplemented!();
    }

    pub fn get_name_of_sys(&self, sys_id: SysId) -> &str
    {
        unimplemented!()
    }
}

pub struct SystemRegBuilder
{
    wrapped_run_func: Option<Box<WrappedRunFunc>>,
    name: Option<String>,
    sdt_and_mutability: Vec<(SdtId, SysSdtMutability)>,
}

impl SystemRegBuilder
{
    pub fn new() -> SystemRegBuilder
    {
        unimplemented!();
    }

    pub fn name(self, name: String) -> Self
    {
        unimplemented!();
    }

    pub fn run_func_and_sys_iter<T, I>(mut self, run_func: Box<SysRunFunc<I>>, get_sys_sdt_iter_func: Box<dyn Fn() -> I>) -> Self
        where I: Iterator<Item=T> + 'static
    {
        self.wrapped_run_func = Some(Box::new(move ||
        {
            let sys_sdt_iter = get_sys_sdt_iter_func();
            run_func(sys_sdt_iter);
        }));

        self
    }

    pub fn move_func(self, move_func: Box<MoveFunc>)
    {
        unimplemented!();
    }

    pub fn reg_sys_sdt<T: RegisteredSDT>(self) -> Self
    {
        unimplemented!();
    }

    pub fn reg_sys_mut_sdt<T: RegisteredSDT>(self) -> Self
    {
        unimplemented!();
    }
}

struct SystemDebugInfo
{
    sys_names: Vec<String>,
}

impl SystemDebugInfo
{
    pub fn new() -> SystemDebugInfo
    {
        unimplemented!();
    }
}

//Assume everything is a component for now...
#[macro_export]
macro_rules! reg_run_func_sdts
{
    ($world_builder:ident, $run_func:expr, $($sdt_and_mutability:tt)*) =>
    {
        let mut sys_reg_builder = SystemRegBuilder::new();
        $(reg_sys_sdt_type!(sys_reg_builder, $sdt_and_mutability);)*

        let move_func = |sdt_data, eid, old_pos, new_sect_id|
        {
            $(reg_move_func!(sdt_data, eid, old_pos, new_sect_id, $sdt_and_mutability))*
        };
        sys_reg_builder.move_func(move_func);

        world_builder.reg_sys(sys_reg_builder);

        |w|
        {
            let get_sys_sdt_iter_func = itertools::izip!($(get_iter_for_sdt_type!($sdt_and_mutability)),*);
            w.run_func_and_sys_iter($run_func, get_sys_sdt_iter_func);
        }
    };
}

#[macro_export]
macro_rules! reg_sys_sdt_type
{
    ($sys_reg_builder:ident, I: $sdt:ty) => { $world_builder.reg_sys_sdt::<$sdt>() };
    ($sys_reg_builder:ident, M: $sdt:ty) => { $world_builder.reg_sys_mut_sdt::<$sdt>() };
}

// TODO: Support isds and tags later
#[macro_export]
macro_rules! get_iter_for_sdt_type
{
    ($world:ident, I: $sdt:ty) => { $world.get_comp_iter::<$sdt>() };
    ($world:ident, M: $sdt:ty) => { $world.get_comp_mut_iter::<$sdt>() };
}

#[macro_export]
macro_rules! reg_move_func
{
    ($sdt_data:ident, $eid:ident, $old_pos:ident, $new_sect_id:ident, $unused:literal, $sdt:ty) =>
    {
        $sdt_data.move_sdt_to_new_sect::<$sdt>(eid, old_pos, new_sect_id);
    };
}