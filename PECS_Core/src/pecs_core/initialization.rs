use std::collections::HashMap;
use std::thread;

use crate::pecs_common::types::SysId;

use crate::pecs_core::world;
use crate::pecs_core::world::WorldBuilder;
use crate::pecs_core::run_cond_data::RunCondSchedInfo;
use crate::pecs_core::scheduler::{TaskRegistration, TaskRegInfo, SchedOutboundMsg, Scheduler, SchedulerInitInfo, SchedulerError};
use crate::pecs_core::scheduler::{TaskRunFunc, TaskGetNumItemsFunc};

use pecs_common::types::RunTaskWorkRequest;

pub type InitResult<T> = Result<T, PECSError>;

pub enum PECSError
{
    SchedulerError(SchedulerError)
}

pub struct SystemRegInfo
{
    pub init_sys_run_func: fn() -> TaskRunFunc,
    pub get_task_num_item_func: fn() -> TaskGetNumItemsFunc
}

pub struct SyntaxExtensionsGeneratedCalls
{
    pub reg_isds_calls: Box<dyn Fn(&mut WorldBuilder)>,
    pub reg_sys_calls: Box<dyn Fn(&mut WorldBuilder)>,
    pub reg_rc_calls: Box<dyn Fn(&mut WorldBuilder)>,
}

pub fn run(data: SyntaxExtensionsGeneratedCalls) -> InitResult<(WorldBuilder, Scheduler)>
{
    let (world_builder, mut scheduler) = init(data)?;
    scheduler.start().map_err(PECSError::SchedulerError)?;

    Ok((world_builder, scheduler))
}

pub fn run_and_block(data: SyntaxExtensionsGeneratedCalls) -> InitResult<()>
{
    let (_, mut scheduler) = init(data)?;
    scheduler.start().map_err(PECSError::SchedulerError)?; // Main thread will block until PECS finishes

    loop
    {
        let event = scheduler.wait_for_event().map_err(PECSError::SchedulerError)?;
        match event
        {
            SchedOutboundMsg::Terminated => break,
            _ => ()
        }
    }

    Ok(())
}

fn init(data: SyntaxExtensionsGeneratedCalls) -> InitResult<(WorldBuilder, Scheduler)>
{
    let mut world_builder = world::init_world();

    (data.reg_isds_calls)(&mut world_builder);
    (data.reg_sys_calls)(&mut world_builder);
    (data.reg_rc_calls)(&mut world_builder);

    let sched_init_info = SchedulerInitInfo
    {
        num_task_threads: determine_num_task_threads(),
    };

    let mut scheduler = Scheduler::new(sched_init_info).map_err(PECSError::SchedulerError)?;

    let mut sys_id_to_task_id_map = HashMap::new();
    for sys_reg_info in world_builder.get_sys_run_func_info()
    {
        let reg_info = TaskRegInfo
        {
            run_func: sys_reg_info.run,
            get_num_items_func: sys_reg_info.get_task_num_items,
        };

        let task_id = scheduler.register_task(reg_info).map_err(PECSError::SchedulerError)?;
        sys_id_to_task_id_map.insert(sys_reg_info.sys_id, task_id);
    }

    reg_rc_data_with_sched(&mut scheduler, &world_builder, &sys_id_to_task_id_map)?;
    Ok((world_builder, scheduler))
}

fn reg_rc_data_with_sched(sched: &mut Scheduler, world_builder: &WorldBuilder, sys_id_to_task_id_map: &HashMap<SysId, usize>) -> InitResult<()>
{
    let rc_sched_info = world_builder.get_run_cond_sched_info();

    for (func, reg_syses) in rc_sched_info.static_info
    {
        let rc_id = sched.register_static_run_condition(func).map_err(PECSError::SchedulerError)?;
        reg_tasks_with_rc(sched, rc_id, reg_syses.sys_ids, &sys_id_to_task_id_map)?;
    }

    for (func, reg_syses) in rc_sched_info.dynamic_info
    {
        let rc_id = sched.register_dynamic_run_condition(func).map_err(PECSError::SchedulerError)?;
        reg_tasks_with_rc(sched, rc_id, reg_syses.sys_ids, &sys_id_to_task_id_map)?;
    }

    Ok(())
}

fn reg_tasks_with_rc(scheduler: &mut Scheduler, rc_id: usize, sys_ids: Vec<SysId>, sys_id_to_task_id_map: &HashMap<SysId, usize>) -> InitResult<()>
{
    for sys_id in sys_ids
    {
        let sys_task_id = sys_id_to_task_id_map[&sys_id];
        scheduler.register_task_with_run_condition(rc_id, sys_task_id).map_err(PECSError::SchedulerError)?;
    }

    Ok(())
}

fn determine_num_task_threads() -> usize
{
    unimplemented!();
}

