use std::any;
use std::collections::HashMap;
use std::marker::PhantomData;
use std::ops::Range;

use pecs_common::types::{CompItemSectIdAndIdx, EID, SdtId, SectId, SectItemIdx, SysId, CombId, CollId, RegisteredSDT};
use crate::pecs_core::entity_comb::{SdtInfo, CombInfo, CollInfo, EntityInfo, EntityCombs, SdtInsertChangeOutcome, SdtDeleteChangeOutcome,
    CombFormedChange, CombSplitChange};

use crate::pecs_common::specialized_collections::system_sdt_containers::{ComponentContainerInitDataProvider, CompContainerType};
use crate::pecs_core::common::MoveFuncId;
use crate::pecs_core::run_cond_data::{RunCondSchedInfo, RunCondBuilder};
use crate::pecs_core::system_data::SystemData;
use crate::pecs_core::sdt_data::SdtData;
use crate::pecs_core::system_data::SystemRegBuilder;
use crate::pecs_core::scheduler::{TaskRunFunc, TaskGetNumItemsFunc};
use crate::pecs_core::comb_sect_data::{CombSectData, CombSectDataBuilder};
use crate::pecs_core::sdt_move_func_state::SdtMoveFuncState;
use crate::pecs_core::sdt_sect_item_idx_tracking::SdtSectItemIdxTracking;

pub struct World
{
    comb_sect_data: CombSectData,
    e_combs: EntityCombs,
    move_func_state: SdtMoveFuncState,
    sdt_data: SdtData,
    sdt_sect_item_idx_tracking: SdtSectItemIdxTracking,
    sys_data: SystemData,
}

pub struct WorldBuilder
{
    comb_sect_data_builder: CombSectDataBuilder,
    e_combs: EntityCombs,
    move_func_state: SdtMoveFuncState,
    sdt_data: SdtData,
    debug_info: WorldDebugInfo,
}

impl WorldBuilder
{
    pub fn reg_comp<T: RegisteredSDT>(mut self) -> Self
    {
        self.sdt_data.reg_comp::<T>(CompContainerType::<T>::provide_cont_init_data());
        self.e_combs.reg_sdt(T::get_sdt_id());
        self.debug_info.reg_sdt_name(T::get_sdt_id(), any::type_name::<T>().into());
        self
    }

    pub fn reg_isd<T: RegisteredSDT>(self) -> Self
    {
        unimplemented!();
    }

    pub fn reg_tag<T: RegisteredSDT>(self) -> Self
    {
        unimplemented!();
    }

    pub fn reg_sys(self, reg_builder: SystemRegBuilder) -> Self
    {
        unimplemented!();
    }

    pub fn reg_run_cond(self, rc_builder: RunCondBuilder) -> Self
    {
        unimplemented!();
    }

    pub fn reg_sys_comb(self, sys_comb: Box<[SdtId]>) -> Self
    {
        unimplemented!();
    }

    pub fn get_sys_run_func_info(&self) -> Vec<SysRunFunc>
    {
        unimplemented!();
    }

    pub fn get_run_cond_sched_info(&self) -> RunCondSchedInfo
    {
        unimplemented!();
    }

    pub fn build(self) -> World
    {
        unimplemented!();
    }
}

pub fn init_world() -> WorldBuilder
{
    unimplemented!();
}

pub struct SysRunFunc
{
    pub run: TaskRunFunc,
    pub get_task_num_items: TaskGetNumItemsFunc,
    pub sys_id: SysId,
}

// TODO: Since this needs to be multi-threaded, we can't be using &mut everywhere here.
// Likely unsafe will be needed here since the scheduler we know exactly what will be modified and when.
impl World
{
    pub fn insert_sdt_into_ent<T>(&mut self, eid: EID, sdt: T)
        where T: RegisteredSDT
    {
        let outcome = self.e_combs.add_sdt_to_entity(eid, T::get_sdt_id(), &mut self.move_func_state);

        match outcome
        {
            SdtInsertChangeOutcome::CombsFormed(move_func_id, merge_key) =>
            {
                // Box for now. We don't need to do this though.
                let prev_item_idxs: Box<[SectItemIdx]> = self.sdt_sect_item_idx_tracking.merge_sect_ids_for_entity(eid, &merge_key).collect();
                self.move_func_state.call_merge_group_move_func(move_func_id, &self.sdt_data, eid, &prev_item_idxs);
            },
            SdtInsertChangeOutcome::NoChange(ins_key) => self.sdt_sect_item_idx_tracking.ins_sect_id_for_entity(eid, ins_key)
        }

        unimplemented!();
    }

    pub fn delete_sdt_from_ent(&mut self, eid: EID, sdt_id: usize)
    {
        let outcome = self.e_combs.del_sdt_from_entity(eid, sdt_id, &mut self.move_func_state);

        match outcome
        {
            SdtDeleteChangeOutcome::CombsSplit(move_func_id, split_key) =>
            {
                let prev_item_idx = self.sdt_sect_item_idx_tracking.split_sect_id_for_entity(eid, split_key);
                self.move_func_state.call_split_group_move_func(move_func_id, &self.sdt_data, eid, prev_item_idx);
            }
            SdtDeleteChangeOutcome::NoChange(del_key) => self.sdt_sect_item_idx_tracking.del_sect_id_for_entity(eid, del_key)
        }

        unimplemented!();
    }

    pub fn create_entity(&mut self) -> EID
    {
        self.e_combs.create_entity()
    }

    pub fn batch_create_entities(&mut self, amt: usize) -> Range<EID>
    {
        self.e_combs.batch_create_entities(amt);
        unimplemented!();
    }

    pub fn delete_entity(&mut self, eid: EID)
    {
        self.e_combs.delete_entity(eid)
    }

    pub fn batch_delete_entities(&mut self, eids: impl Iterator<Item=EID>)
    {
        self.e_combs.batch_del_entities(eids)
    }

    pub fn entity_exists(&self, eid: EID) -> bool
    {
        self.e_combs.entity_exists(eid)
    }

    pub fn get_entity_info(&self, eid: EID) -> EntityInfo
    {
        self.e_combs.get_entity_info(eid)
    }

    pub fn get_coll_info(&self, coll_id: CollId) -> CollInfo
    {
        unimplemented!();
    }

    pub fn get_comb_info(&self, comb_id: CombId) -> CombInfo
    {
        unimplemented!();
    }

    pub fn get_sdt_info(&self, sdt_id: SdtId) -> SdtInfo
    {
        unimplemented!();
    }

    pub fn entity_contains_sdt(&self, eid: EID, sdt_id: usize) -> bool
    {
        self.e_combs.entity_contains_sdt(eid, sdt_id)
    }

    pub fn get_num_sdts_on_entity(&self, eid: EID) -> usize
    {
        self.e_combs.get_num_sdts_on_entity(eid)
    }

    pub(crate) fn get_comp_iter<T: 'static>(&self) -> impl Iterator<Item=&T>
    {
        // TODO
        Vec::new().into_iter()
    }

    pub(crate) fn get_comp_mut_iter<T: 'static>(&self) -> impl Iterator<Item=&mut T>
    {
        // TODO
        Vec::new().into_iter()
    }
}

struct WorldDebugInfo
{
    sdt_id_to_name: HashMap<SdtId, String>
}

impl WorldDebugInfo
{
    pub fn new() -> WorldDebugInfo
    {
        unimplemented!();
    }

    pub fn reg_sdt_name(&mut self, sdt_id: SdtId, sdt_name: String)
    {
        unimplemented!();
    }
}

pub trait SDTContainerInterface<T>
{
    fn container_insert_sdt(&mut self, eid: EID, sdt: T, comb_id: usize);
    fn container_delete_sdt(&mut self, eid: EID, sdt_type: PhantomData<T>, comb_id: usize);
}