use crate::pecs_common::types::CompItemSectIdAndIdx;

use pecs_common::specialized_collections::system_sdt_containers::{CompContainerInitData, ComponentContainerFuncs, ComponentContainerAddNewSysCombFunc, ComponentContainerInsertCompFunc, ComponentContainerDeleteCompFunc, CompContainerType, ComponentContainerGetIterFunc, ComponentContainerGetMutIterFunc, ComponentContainerMoveSdtToNewSectFunc, SdtItemIterator, SdtItemMutIterator};
use pecs_common::types::{EID, SdtId, CompId, RegisteredSDT, SectId, SectIdx};

use std::any::Any;
use std::collections::HashMap;


pub struct SdtData
{
    sdt_containers: Vec<Box<dyn Any>>,
    comp_funcs: CompFuncs,
    comp_eid_to_pos_ltable: HashMap<EID, CompItemSectIdAndIdx>
}

struct CompFuncs
{
    insert_comp: Vec<Box<dyn Any + 'static>>,
    delete_comp: Vec<Box<dyn Any + 'static>>,
    move_sdt_to_new_sect: Vec<Box<dyn Any + 'static>>,
    get_iter: Vec<Box<dyn Any + 'static>>,
    get_mut_iter: Vec<Box<dyn Any + 'static>>,
    add_new_sys_comb_sect_ids: Vec<Box<dyn Any + 'static>>,
}

impl CompFuncs
{
    fn reg_comp_funcs(&mut self, funcs: ComponentContainerFuncs)
    {
        unimplemented!();
    }
}

impl SdtData
{
    pub fn new() -> SdtData
    {
        unimplemented!();
    }

    pub fn reg_comp_cont(&mut self, comp_cont_init_data: CompContainerInitData)
    {
        unimplemented!();
    }

    pub fn reg_comp<T>(&mut self, cont_init_data: CompContainerInitData) -> SdtId
        where T: RegisteredSDT
    {
        let sdt_id = T::get_sdt_id();
        let comp_id = T::get_container_type_id();
        self.reg_comp_intern(sdt_id, comp_id, cont_init_data);

        sdt_id
    }

    fn reg_comp_intern(&mut self, sdt_id: SdtId, comp_id: CompId, cont_init_data: CompContainerInitData)
    {
        self.sdt_containers[sdt_id] = cont_init_data.comp_container;
        self.comp_funcs.reg_comp_funcs(cont_init_data.funcs);
    }

    pub fn reg_isd<T>(&mut self) -> SdtId
    {
        unimplemented!();
    }

    pub fn reg_tag<T>(&mut self) -> SdtId
    {
        unimplemented!();
    }

    pub fn insert_comp<T>(&self, eid: EID, sect_idx: SectIdx, comp: T)
        where T: RegisteredSDT + 'static
    {
        let cont = Self::get_comp_cont(&self.sdt_containers);
        let insert_func: &ComponentContainerInsertCompFunc<T> = Self::get_comp_func(&self.comp_funcs.insert_comp);

        insert_func(cont, sect_idx, comp);
    }

    pub fn insert_isd<T>(&self, eid: EID, isd: T)
        where T: RegisteredSDT
    {
        unimplemented!();
    }

    pub fn insert_tag<T>(&self, eid: EID, isd: T)
        where T: RegisteredSDT
    {
        unimplemented!();
    }

    pub fn delete_comp<T>(&self, eid: EID, comp_pos: CompItemSectIdAndIdx)
        where T: RegisteredSDT + 'static
    {
        let cont = Self::get_comp_cont(&self.sdt_containers);
        let del_func: &ComponentContainerDeleteCompFunc<T> = Self::get_comp_func(&self.comp_funcs.delete_comp);

        del_func(cont, comp_pos);
    }

    pub fn delete_isd<T>(&self, eid: EID)
        where T: RegisteredSDT
    {
        unimplemented!();
    }

    pub fn delete_tag<T>(&self, eid: EID)
        where T: RegisteredSDT
    {
        unimplemented!();
    }

    pub fn move_sdt_to_new_sect<T: 'static>(&self, eid: EID, old: CompItemSectIdAndIdx, new: SectId)
        where T: RegisteredSDT
    {
        let cont = Self::get_comp_cont(&self.sdt_containers);
        let move_func: &ComponentContainerMoveSdtToNewSectFunc<T> = Self::get_comp_func(&self.comp_funcs.move_sdt_to_new_sect);

        move_func(cont, old, new);
    }

    pub fn get_comp_iter_for_sect_ids<T>(&self, sect_ids: &[SectId]) -> SdtItemIterator<T>
        where T: RegisteredSDT + 'static
    {
        let cont = Self::get_comp_cont(&self.sdt_containers);
        let get_iter_func: &ComponentContainerGetIterFunc<T> = Self::get_comp_func(&self.comp_funcs.insert_comp);

        get_iter_func(cont, sect_ids)
    }

    pub fn get_mut_comp_iter_for_sect_ids<T>(&self, sect_ids: &[SectId]) -> SdtItemMutIterator<T>
        where T: RegisteredSDT + 'static
    {
        let cont = Self::get_comp_cont(&self.sdt_containers);
        let get_mut_iter_func: &ComponentContainerGetMutIterFunc<T> = Self::get_comp_func(&self.comp_funcs.get_iter);

        get_mut_iter_func(cont, sect_ids)
    }

    pub fn add_new_sys_comb_sect_ids_for_cont<T>(&mut self, sys_comb_sect_ids: Box<[SectId]>)
        where T: RegisteredSDT + 'static
    {
        let cont = Self::get_mut_comp_cont(&mut self.sdt_containers);
        let add_sys_new_comb_func: &ComponentContainerAddNewSysCombFunc<T> = Self::get_comp_func(&self.comp_funcs.add_new_sys_comb_sect_ids);

        add_sys_new_comb_func(cont, sys_comb_sect_ids);
    }

    fn get_comp_func<T: 'static>(funcs: &[Box<dyn Any>]) -> &T
    {
        let func_any = &funcs[T::get_container_type_id()];
        func_any.downcast_ref::<&T>().unwrap()
    }

    fn get_comp_cont<T: 'static>(sdt_containers: &Vec<Box<dyn Any>>) -> &CompContainerType<T>
    {
        let cont_any = &sdt_containers[T::get_sdt_id()];
        cont_any.downcast_ref::<CompContainerType<T>>().unwrap()
    }

    fn get_mut_comp_cont<T: 'static>(sdt_containers: &mut Vec<Box<dyn Any>>) -> &mut CompContainerType<T>
    {
        let cont_any = &mut sdt_containers[T::get_sdt_id()];
        cont_any.downcast_mut::<CompContainerType<T>>().unwrap()
    }
}