use crate::pecs_common::types::{EID, SectId, SectItemIdx};
use crate::pecs_core::utils::JaggedVec;

pub struct SdtSectItemIdxTracking
{
    sect_idxs_per_entity: JaggedVec<SectItemIdx>,
    entity_j_arr_offsets: Vec<usize>,
    eid_at_i_arr_offset: Vec<Vec<EID>>,
    num_items_in_sect: Vec<usize>,
    num_sects_per_entity: Vec<usize>,
}

impl SdtSectItemIdxTracking
{
    pub fn new(max_sect_ids_on_ent: usize, num_sects: usize) -> SdtSectItemIdxTracking
    {
        let mut sect_idxs_per_entity = JaggedVec::new();
        sect_idxs_per_entity.init_intern_vec_size(max_sect_ids_on_ent);

        let mut eid_at_i_arr_offset = Vec::new();
        eid_at_i_arr_offset.resize(max_sect_ids_on_ent, Vec::new());

        let mut num_items_in_sect = Vec::new();
        num_items_in_sect.resize(num_sects, 0);

        SdtSectItemIdxTracking
        {
            sect_idxs_per_entity,
            entity_j_arr_offsets: Vec::new(),
            eid_at_i_arr_offset,
            num_items_in_sect,
            num_sects_per_entity: Vec::new(),
        }
    }

    pub fn reg_entity(&mut self, eid: EID)
    {
        // I don't really like this, but I can't think of a better way for now
        if self.entity_j_arr_offsets.len() > eid
        {
            self.entity_j_arr_offsets[eid] = 0;
            self.num_sects_per_entity[eid] = 0;
            return;
        }

        self.entity_j_arr_offsets.resize(eid + 1, 0);
        self.num_sects_per_entity.resize(eid + 1, 0);
    }

    pub fn get_specific_sect_item_idxs_for_entity<'a>(&'a self, eid: EID, sect_key: EntitySectLookupKey<'a>) -> impl Iterator<Item=SectItemIdx> + 'a
    {
        let ent_sect_idxs = self.get_sect_idxs_for_entity_intern(eid, sect_key.num_sects_on_entity);
        sect_key.sect_idx_offsets.into_iter().map(move |offset| ent_sect_idxs[*offset])
    }

    pub fn update_sect_item_idx_for_entity(&mut self, eid: EID, sect_key: EntitySectUpdateKey, new_sect_idx: SectItemIdx)
    {
        let ent_sect_idxs = self.get_sect_idxs_for_entity_mut_intern(eid, sect_key.num_sects_on_entity);
        ent_sect_idxs[sect_key.sect_idx_offset] = new_sect_idx;
    }

    // Rets the prev merged sdt item idxs
    pub fn merge_sect_ids_for_entity<'a>(&'a mut self, eid: EID, sect_key: &'a EntitySectMergeKey) -> impl Iterator<Item=SectItemIdx> + 'a
    {
        let old_entity_offset = self.entity_j_arr_offsets[eid];
        let new_sect_id_sect_idx = self.inc_and_get_next_sect_idx_for_sect(sect_key.new_sect_id);

        for merged_sect_id in sect_key.sect_ids_merged.into_iter()
        {
            self.num_items_in_sect[*merged_sect_id] -= 1;
        }

        let ent_num_sect_idxs_after = sect_key.sects_idx_offsets_to_copy_to_new_entry.len() + 1;
        self.entity_j_arr_offsets[eid] = ent_num_sect_idxs_after;
        self.sect_idxs_per_entity.push_default(ent_num_sect_idxs_after);

        // TODO: This is pretty bad. There's no reason to allocate another vec with collect besides as a quick fix to lifetime issues. Fix at some point!
        let ent_sect_idxs_before: Vec<SectItemIdx> = self.sect_idxs_per_entity.get(sect_key.num_sects_on_entity_before, old_entity_offset).into_iter().cloned().collect();
        let ent_sect_idxs_after = self.get_sect_idxs_for_entity_mut_intern(eid, ent_num_sect_idxs_after);

        for OldToNewEntitySectsMapping { old_idx, new_idx } in sect_key.sects_idx_offsets_to_copy_to_new_entry.into_iter()
        {
            ent_sect_idxs_after[*new_idx] = ent_sect_idxs_before[*old_idx];
        }

        ent_sect_idxs_after[sect_key.new_sect_id_offset] = new_sect_id_sect_idx;

        let eids_at_old_offset = &mut self.eid_at_i_arr_offset[sect_key.num_sects_on_entity_before];
        let eid_that_will_replace_with_swap_remove = eids_at_old_offset[eids_at_old_offset.len()];
        self.sect_idxs_per_entity.swap_remove(sect_key.num_sects_on_entity_before, old_entity_offset);

        eids_at_old_offset[old_entity_offset] = eid_that_will_replace_with_swap_remove;
        self.num_sects_per_entity[eid] = ent_num_sect_idxs_after;

        sect_key.offsets_of_sect_ids_merged.into_iter().map(move |offset| ent_sect_idxs_before[*offset])
    }

    pub fn split_sect_id_for_entity(&mut self, eid: EID, sect_key: EntitySectSplitKey) -> SectItemIdx
    {
        unimplemented!();
    }

    pub fn ins_sect_id_for_entity(&mut self, eid: EID, sect_key: EntitySectInsKey)
    {
        unimplemented!();
    }

    pub fn del_sect_id_for_entity(&mut self, eid: EID, sect_key: EntitySectDelKey)
    {
        unimplemented!();
    }

    pub fn get_sect_idxs_for_entity(&self, eid: EID) -> &[SectItemIdx]
    {
        let eid_offset = self.entity_j_arr_offsets[eid];
        let num_sects_on_ent = self.num_sects_per_entity[eid];
        self.sect_idxs_per_entity.get(num_sects_on_ent, eid_offset)
    }

    pub fn get_update_key_for_sect_ids_section(sect_ids_on_ent: &[SectId], sect_id_for_update: SectId) -> EntitySectUpdateKey
    {
        unimplemented!();
    }

    pub fn get_lookup_key_for_sect_ids_section<'a>(sect_ids_on_ent: &'a [SectId], sect_ids_in_lookup: &'a [SectId]) -> EntitySectLookupKey<'a>
    {
        unimplemented!();
    }

    pub fn get_merge_key_for_sect_ids_section<'a>(sect_ids_on_ent_before: &'a [SectId], sect_ids_on_ent_after: &'a [SectId]) -> EntitySectMergeKey
    {
        let num_sects_on_entity = sect_ids_on_ent_before.len();

        let mut new_sect_ids: Vec<SectId> = sect_ids_on_ent_after.into_iter().cloned().collect();
        new_sect_ids.sort();

        let sect_ids_that_did_not_merge = sect_ids_on_ent_before.into_iter().filter(|old_sect_id| sect_ids_on_ent_after.contains(old_sect_id));
        let sect_ids_that_did_not_merge_new_idxs = sect_ids_that_did_not_merge.map(|sect_id| sect_ids_on_ent_after.into_iter().position(|x| x == sect_id).unwrap());
        let old_idx_and_new_idx = sect_ids_that_did_not_merge_new_idxs.enumerate();
        let mappings: Box<[OldToNewEntitySectsMapping]> = old_idx_and_new_idx.map(|(old, new)| OldToNewEntitySectsMapping::new(old, new)).collect();

        let new_sect_id = *sect_ids_on_ent_after.into_iter().filter(|sect_id| sect_ids_on_ent_before.contains(sect_id)).next().unwrap();
        let new_sect_id_offset = sect_ids_on_ent_after.into_iter().position(|sect_id| new_sect_id == *sect_id).unwrap();

        let sect_ids_merged: Box<[usize]> = sect_ids_on_ent_before.into_iter().filter(|old_sect_id| !sect_ids_on_ent_after.contains(old_sect_id)).cloned().collect();
        let offsets_of_sect_ids_merged: Box<[usize]> = sect_ids_merged.into_iter().map(|merged_sect_id| sect_ids_on_ent_before.into_iter().position(|x| x == merged_sect_id).unwrap()).collect();

        EntitySectMergeKey
        {
            num_sects_on_entity_before: num_sects_on_entity,
            sects_idx_offsets_to_copy_to_new_entry: mappings,
            new_sect_id,
            sect_ids_merged,
            new_sect_id_offset,
            offsets_of_sect_ids_merged,
        }
    }

    pub fn get_split_key_for_sect_ids_section<'a>(sect_ids_on_ent_before: &'a [SectId], sect_ids_on_ent_after: &'a [SectId]) -> EntitySectSplitKey
    {
        unimplemented!();
    }

    pub fn get_ins_key_for_sect_ids_section<'a>(old_sect_ids_on_ent: &'a [SectId], new_sect_ids_on_ent: &'a [SectId]) -> EntitySectInsKey
    {
        unimplemented!();
    }

    pub fn get_del_key_for_sect_ids_section<'a>(old_sect_ids_on_ent: &'a [SectId], new_sect_ids_on_ent: &'a [SectId]) -> EntitySectDelKey
    {
        unimplemented!();
    }

    fn get_sect_idxs_for_entity_intern(&self, eid: EID, num_sects_on_entity: usize) -> &[SectItemIdx]
    {
        let eid_offset = self.entity_j_arr_offsets[eid];
        self.sect_idxs_per_entity.get(num_sects_on_entity, eid_offset)
    }

    fn get_sect_idxs_for_entity_mut_intern(&mut self, eid: EID, num_sects_on_entity: usize) -> &mut [SectItemIdx]
    {
        let eid_offset = self.entity_j_arr_offsets[eid];
        self.sect_idxs_per_entity.get_mut(num_sects_on_entity, eid_offset)
    }

    fn inc_and_get_next_sect_idx_for_sect(&mut self, sect_id: SectId) -> SectItemIdx
    {
        let next_sect_idx = self.num_items_in_sect[sect_id];
        self.num_items_in_sect[sect_id] += 1;
        next_sect_idx
    }
}

#[derive(Clone, Copy, Debug)]
struct OldToNewEntitySectsMapping
{
    old_idx: usize,
    new_idx: usize,
}

impl OldToNewEntitySectsMapping
{
    fn new(old_idx: usize, new_idx: usize) -> OldToNewEntitySectsMapping
    {
        OldToNewEntitySectsMapping
        {
            old_idx,
            new_idx,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct EntitySectLookupKey<'a>
{
    num_sects_on_entity: usize,
    sect_idx_offsets: &'a [usize],
}

#[derive(Clone, Copy, Debug)]
pub struct EntitySectUpdateKey
{
    num_sects_on_entity: usize,
    sect_idx_offset: usize,
}

#[derive(Clone, Debug)]
pub struct EntitySectMergeKey
{
    num_sects_on_entity_before: usize,
    sects_idx_offsets_to_copy_to_new_entry: Box<[OldToNewEntitySectsMapping]>,
    new_sect_id: SectId,
    sect_ids_merged: Box<[SectId]>,
    new_sect_id_offset: usize,
    offsets_of_sect_ids_merged: Box<[usize]>,
}

#[derive(Clone, Debug)]
pub struct EntitySectSplitKey
{

}

#[derive(Clone, Debug)]
pub struct EntitySectInsKey
{
    num_sects_on_entity: usize,
    new_sect_id: SectId,
    new_sect_offset: usize
}

#[derive(Clone, Debug)]
pub struct EntitySectDelKey
{

}

#[cfg(test)]
mod sdt_sect_item_idx_tracking_tests
{
    use crate::pecs_core::sdt_sect_item_idx_tracking::SdtSectItemIdxTracking;
    use crate::pecs_common::types::{EID, SectId, SectItemIdx};

    const MAX_SECT_IDS_PER_ENT: usize = 5;
    const NUM_SECT_IDS: usize = 4;

    const EID_1: EID = 0;

    const SECT_ID_1: SectId = 0;
    const SECT_ID_2: SectId = 4;
    const SECT_ID_3: SectId = 82;
    const SECT_ID_4: SectId = 105;

    fn init_sect_idx_tracker() -> SdtSectItemIdxTracking
    {
        SdtSectItemIdxTracking::new(MAX_SECT_IDS_PER_ENT, NUM_SECT_IDS)
    }

    fn init_tracker_and_insert_eids(eids: &[EID]) -> SdtSectItemIdxTracking
    {
        let mut tracker = init_sect_idx_tracker();
        for eid in eids
        {
            tracker.reg_entity(*eid);
        }

        tracker
    }

    fn init_tracker_insert_eid_and_insert_sect_ids_for_eid(eid: EID, sect_ids_to_ins: &[SectId]) -> SdtSectItemIdxTracking
    {
        let mut tracker = init_tracker_and_insert_eids(&[EID_1]);
        insert_sect_ids_for_ent(&mut tracker, eid, sect_ids_to_ins);
        tracker
    }

    fn insert_sect_id_for_ent(tracker: &mut SdtSectItemIdxTracking, eid: EID, new_sect_id: SectId)
    {
        let sect_ids_before = tracker.get_sect_idxs_for_entity(eid);
        let mut sect_ids_after = sect_ids_before.to_vec();
        sect_ids_after.push(new_sect_id);

        let ins_key = SdtSectItemIdxTracking::get_ins_key_for_sect_ids_section(&sect_ids_before, sect_ids_after.as_slice());
        tracker.ins_sect_id_for_entity(eid, ins_key);
    }

    fn insert_sect_ids_for_ent(tracker: &mut SdtSectItemIdxTracking, eid: EID, new_sect_ids: &[SectId])
    {
        for sect_id in new_sect_ids
        {
            insert_sect_id_for_ent(tracker, eid, *sect_id)
        }
    }

    fn del_sect_id_on_ent(tracker: &mut SdtSectItemIdxTracking, eid: EID, sect_id_to_remove: SectId)
    {
        let sect_ids_before = tracker.get_sect_idxs_for_entity(eid);
        let mut sect_ids_after = sect_ids_before.to_vec();
        sect_ids_after.swap_remove(sect_ids_after.iter().position(|x| *x == sect_id_to_remove).unwrap());

        let del_key = SdtSectItemIdxTracking::get_del_key_for_sect_ids_section(&sect_ids_before, &sect_ids_after);
        tracker.del_sect_id_for_entity(eid, del_key);
    }

    fn merge_sect_ids_on_ent<'a>(tracker: &'a mut SdtSectItemIdxTracking, eid: EID, sect_ids_before: &[SectId], sect_ids_after: &[SectId]) -> Box<[SectItemIdx]>
    {
        let merge_key = SdtSectItemIdxTracking::get_merge_key_for_sect_ids_section(sect_ids_before, sect_ids_after);
        tracker.merge_sect_ids_for_entity(eid, &merge_key).collect()
    }

    fn split_sect_id_on_ent(tracker: &mut SdtSectItemIdxTracking, eid: EID, sect_ids_before: &[SectId], sect_ids_after: &[SectId]) -> SectItemIdx
    {
        let split_key = SdtSectItemIdxTracking::get_split_key_for_sect_ids_section(sect_ids_before, sect_ids_after);
        tracker.split_sect_id_for_entity(eid, split_key)
    }

    #[test]
    fn a_new_entity_should_have_no_sect_idxs()
    {
        let tracker = init_tracker_and_insert_eids(&[EID_1]);
        let num_sects_on_ent = tracker.get_sect_idxs_for_entity(EID_1).len();

        assert_eq!(num_sects_on_ent, 0);
    }

    #[test]
    fn inserting_a_sdt_should_add_the_associated_sect_idx()
    {
        let mut tracker = init_tracker_insert_eid_and_insert_sect_ids_for_eid(EID_1, &[SECT_ID_1]);
        let sect_ids_on_ent = tracker.get_sect_idxs_for_entity(EID_1);

        assert_eq!(sect_ids_on_ent, [EID_1]);
    }

    #[test]
    fn deleting_a_sdt_should_remove_the_associated_sect_idx()
    {
        let mut tracker = init_tracker_insert_eid_and_insert_sect_ids_for_eid(EID_1, &[SECT_ID_1]);
        del_sect_id_on_ent(&mut tracker, EID_1, SECT_ID_1);

        let num_sect_on_ent_after_del = tracker.get_sect_idxs_for_entity(EID_1).len();
        assert_eq!(num_sect_on_ent_after_del, 0);
    }

    #[test]
    fn a_lookup_key_for_an_ent_should_return_specific_sect_idxs()
    {
        let mut tracker = init_tracker_insert_eid_and_insert_sect_ids_for_eid(EID_1, &[SECT_ID_1, SECT_ID_2, SECT_ID_3]);

        let sect_ids_on_ent = tracker.get_sect_idxs_for_entity(EID_1);
        let lookup_key = SdtSectItemIdxTracking::get_lookup_key_for_sect_ids_section(&sect_ids_on_ent, &[SECT_ID_1, SECT_ID_3]);
        let sect_idxs: Box<[SectItemIdx]> = tracker.get_specific_sect_item_idxs_for_entity(EID_1, lookup_key).collect();

        assert_eq!(sect_idxs, Box::from([0, 0]));
    }

    #[test]
    fn a_merge_key_should_merge_the_appropriate_sect_ids()
    {
        let mut tracker = init_tracker_insert_eid_and_insert_sect_ids_for_eid(EID_1, &[SECT_ID_1, SECT_ID_2, SECT_ID_3]);
        let _ = merge_sect_ids_on_ent(&mut tracker, EID_1, &[SECT_ID_1, SECT_ID_2, SECT_ID_3], &[SECT_ID_1, SECT_ID_4]);

        let sect_ids_on_ent = tracker.get_sect_idxs_for_entity(EID_1);
        assert_eq!(sect_ids_on_ent, [SECT_ID_1, SECT_ID_4]);
    }

    #[test]
    fn merging_sect_ids_should_return_the_idxs_merged()
    {
        let mut tracker = init_tracker_insert_eid_and_insert_sect_ids_for_eid(EID_1, &[SECT_ID_1, SECT_ID_2, SECT_ID_3]);
        let merged_sect_idxs = merge_sect_ids_on_ent(&mut tracker, EID_1, &[SECT_ID_1, SECT_ID_2, SECT_ID_3], &[SECT_ID_1, SECT_ID_4]);

        assert_eq!(merged_sect_idxs, Box::from([0, 0]));
    }

    #[test]
    fn a_split_key_should_split_the_appropriate_sect_ids()
    {
        let mut tracker = init_tracker_insert_eid_and_insert_sect_ids_for_eid(EID_1, &[SECT_ID_1, SECT_ID_2, SECT_ID_3]);
        let _ = split_sect_id_on_ent(&mut tracker, EID_1, &[SECT_ID_1, SECT_ID_2], &[SECT_ID_1, SECT_ID_3, SECT_ID_4]);

        let sect_ids_on_ent = tracker.get_sect_idxs_for_entity(EID_1);
        assert_eq!(sect_ids_on_ent, [SECT_ID_1, SECT_ID_3, SECT_ID_4]);
    }

    #[test]
    fn splitting_a_sect_id_should_return_the_idx_split()
    {
        let mut tracker = init_tracker_insert_eid_and_insert_sect_ids_for_eid(EID_1, &[SECT_ID_1, SECT_ID_2, SECT_ID_3]);
        let sect_idx_split = split_sect_id_on_ent(&mut tracker, EID_1, &[SECT_ID_1, SECT_ID_2], &[SECT_ID_1, SECT_ID_3, SECT_ID_4]);

        assert_eq!(sect_idx_split, 0);
    }
}