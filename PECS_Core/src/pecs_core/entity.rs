use pecs_common::types::EID;

struct Entity
{
    eid: EID
}

impl Entity
{
    fn add_component<T>(component: T) {}
    fn remove_component<T>() {}
    fn destroy_entity() {}
}