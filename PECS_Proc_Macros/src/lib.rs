#![feature(plugin)]

#![recursion_limit = "128"]

extern crate proc_macro;
#[macro_use]
extern crate quote;
extern crate syn;
extern crate yaml_rust;
extern crate proc_macro2;
#[macro_use]
extern crate itertools;

extern crate pecs_common;

mod registered_data_util_funcs;
mod registration_yaml_parser;
mod token_generation;
mod system_order_generator;
mod tokenizable_types;

use proc_macro::TokenStream;
use quote::Tokens;

use system_order_generator::generate_system_dependencies;
use token_generation::{generate_registered_data_tokens, generate_sdt_struct_and_related_tokens};

//use 

/*
	register_systems_and_system_data_types!("registered_data.yml")
*/
#[proc_macro]
pub fn register_systems_and_system_data_types(input: TokenStream) -> TokenStream
{
	match generate_tokens(input)
	{
		Ok(tokens) => tokens,
		Err(err) => panic!("Panicked when running the register_systems_and_system_data_types procedural macro with the following error: {}", err)
	}
}

fn generate_tokens(input: TokenStream) -> Result<TokenStream, String>
{
	let mut output_tokens = Tokens::new();
	let mut string_holder = Vec::new();
	
	{
		let processed_reg_data = generate_registered_data_tokens(input, &mut output_tokens, &mut string_holder)?;
		generate_sdt_struct_and_related_tokens(&processed_reg_data, &mut output_tokens);
		let system_order_info = generate_system_dependencies(&processed_reg_data.registered_data);
	}

	Ok(output_tokens.into())
}