use std::collections::HashMap;
use std::ops::Range;
use std::iter::{repeat, once};

use syn;
use quote::{Tokens, ToTokens};
use proc_macro::TokenStream;
use itertools;

use pecs_common::types::{RegisteredData, RegisteredSystemDataType, SystemCombinationUsageInstance};
use pecs_common::types::{SystemInputOutput, SystemType};

use registration_yaml_parser::{extract_data_from_file, ProcessedRegistrationDataYAML};
use tokenizable_types::TokenizableString;
use registered_data_util_funcs::{get_all_combination_ids_that_use_a_system_data_type,
	get_system_data_type_ids_used_by_combination, filter_system_combination_instances_by_type,
	get_sdt_records_for_sdt_ids, get_combination_records_iter_for_combination_ids};

macro_rules! create_tokens_of_field_for_struct_iter_clone
{
	($field_name:ident, $iter:expr) =>
	{
		$iter.map(|struct_inst| struct_inst.$field_name.clone())
		.map(|$field_name| quote! { #$field_name });
	}
}

macro_rules! create_tokens_for_iter
{
	($iter:expr) =>
	{
		$iter.map(|item| quote! { #item });
	}
}

pub fn generate_registered_data_tokens<'a>(input: TokenStream, output: &mut Tokens, string_holder: &'a mut Vec<String>) -> Result<ProcessedRegistrationDataYAML<'a>, String>
{
	let path = match syn::parse(input).unwrap()
	{
		syn::Lit::Str(lit_str) => lit_str.value(),
		_ => return Err("Expression evaluated to something other than a string literal!".to_string())
	};
	
	let processed_yaml = extract_data_from_file(path.as_ref(), string_holder)?;
	
	let tokens = generate_tokens(&processed_yaml);
	
    tokens.to_tokens(output);
	return Ok(processed_yaml);

	fn generate_tokens(processed_yaml: &ProcessedRegistrationDataYAML) -> Tokens
	{
		let registered_system_data_type_tokens = processed_yaml.registered_data.registered_system_data_types.iter()
			.map(|reg_sdt| (reg_sdt.name, reg_sdt.data_type.clone(), reg_sdt.id))
			.map(|(name, data_type, id)| quote!{ (#name, #data_type, #id) });

		let reg_comb_tokens = processed_yaml.registered_data.registered_combinations.iter()
			.map(|reg_comb| (reg_comb.name, reg_comb.id))
			.map(|(name, id)| quote!{ (#name, #id) });

		let reg_system_tokens = processed_yaml.registered_data.registered_systems.iter()
			.map(|reg_sys| (reg_sys.name, reg_sys.system_type.clone(), reg_sys.id))
			.map(|(name, system_type, id)| quote!{ (#name, #system_type, #id) });

		let sys_comb_usage_inst_tokens = processed_yaml.registered_data.system_combination_usage_instances.iter()
			.map(|sys_comb_inst| (sys_comb_inst.combination_id, sys_comb_inst.system_id, sys_comb_inst.system_input_output.clone(), sys_comb_inst.group_id))
			.map(|(comb_id, sys_id, input_output, group_id)| quote!{ (#comb_id, #sys_id, #input_output, #group_id) });

		let sdt_comb_inst_tokens = processed_yaml.registered_data.system_data_type_usage_instances.iter()
			.map(|sdt_comb_inst| (sdt_comb_inst.system_data_type_id, sdt_comb_inst.combination_id))
			.map(|(sdt_id, comb_id)| quote!{ (#sdt_id, #comb_id) });

		quote!
		{
			fn get_registered_data<'a>() -> RegisteredData<'a>
			{
				let mut registered_system_data_types = Vec::new();
				let mut registered_systems = Vec::new();
				let mut registered_combinations = Vec::new();
				let mut system_combination_usage_instances = Vec::new();
				let mut system_data_type_combination_usage_instances = Vec::new();

				#(
					let (name, data_type, id) = #registered_system_data_type_tokens;
					registered_system_data_types.push(RegisteredSystemDataType { name, data_type, id });
				)*
				
				#(
					let (name, id) = #reg_comb_tokens;
					registered_combinations.push(RegisteredCombination { name, id});
				)*

				#(
					let (name, system_type, id) = #reg_system_tokens;
					registered_systems.push( RegisteredSystem { name, system_type, id });
				)*
				
				#(
					let (combination_id, system_id, system_input_output, group_id) = #sys_comb_usage_inst_tokens;
					system_combination_usage_instances.push(SystemCombinationUsageInstance { combination_id, system_id, system_input_output, group_id });
				)*
				
				#(
					let (system_data_type_id, combination_id) = #sdt_comb_inst_tokens;
					system_data_type_combination_usage_instances.push(SystemDataTypeCombinationUsageInstance { system_data_type_id, combination_id });
				)*
				
				RegisteredData
				{
					registered_system_data_types: registered_system_data_types,
					registered_combinations: registered_combinations,
					registered_systems: registered_systems,
					system_data_type_usage_instances: system_data_type_combination_usage_instances,
					system_combination_usage_instances: system_combination_usage_instances
				}
			}
		}
	}
}

struct SdtRangesAndSdtCombToSectIdLookup
{
	sect_ranges_for_each_sdt_container: Vec<Vec<Range<usize>>>,
	sdt_comb_id_to_sect_id_lookup: Vec<HashMap<usize, usize>>
}

struct CombSectAndChildrenIdxs
{
	comb_id: usize,
	sect_idx: usize,
	child_sect_idxs: Option<Vec<usize>>
}

pub fn generate_sdt_struct_and_related_tokens(reg_info: &ProcessedRegistrationDataYAML, output: &mut Tokens)
{
	// ISD's not supported yet, but when they are, their core types will also be handled here

	let sdt_ranges_and_sect_lookup = determine_sect_ranges_for_each_sdt(&reg_info.registered_data);

	generate_sdt_struct(reg_info, output);
	generate_sdt_struct_new_func(reg_info, &sdt_ranges_and_sect_lookup, output);
	generate_comb_iter_definitions(reg_info, &sdt_ranges_and_sect_lookup, output);
	generate_system_run_funcs_init_func(&reg_info.registered_data, output);
}

fn generate_sdt_struct(reg_info: &ProcessedRegistrationDataYAML, output: &mut Tokens)
{
	let sdt_core_types_names_tokens = reg_info.sdt_name_to_core_type_lookup.keys();
	let sdt_core_types_tokens = reg_info.sdt_name_to_core_type_lookup.values();

	// Currently all vec types are components and will just use lockstep sub vec storage
	let sdt_struct_tokens = quote!
	{
		struct SdtStruct
		{
			#(#sdt_core_types_names_tokens: SubVecsSystemContainer<#sdt_core_types_tokens>),*
		}
	};

	println!("SdtStruct Definition Tokens:\n{}", sdt_struct_tokens);
	sdt_struct_tokens.to_tokens(output);
}

fn generate_sdt_struct_new_func(reg_info: &ProcessedRegistrationDataYAML,
	ranges_and_lookup: &SdtRangesAndSdtCombToSectIdLookup, output: &mut Tokens)
{
	let component_core_type_names_tokens_iter = reg_info.sdt_name_to_core_type_lookup.keys();

	let mut sdt_ranges_start_for_each_sdt_container: Vec<Vec<usize>> = Vec::new();
	let mut sdt_ranges_end_for_each_sdt_container: Vec<Vec<usize>> = Vec::new();
	for range_vec in ranges_and_lookup.sect_ranges_for_each_sdt_container.iter()
	{
		sdt_ranges_start_for_each_sdt_container.push(range_vec.iter().map(|range| range.start).collect());
		sdt_ranges_end_for_each_sdt_container.push(range_vec.iter().map(|range| range.end).collect());
	}
	let range_start_iter = sdt_ranges_start_for_each_sdt_container.iter();
	let range_end_iter = sdt_ranges_end_for_each_sdt_container.iter();

	let sdt_sect_range_vec_names_tokens: Vec<TokenizableString> = component_core_type_names_tokens_iter.clone()
		.map(|core_type_name| TokenizableString::new(format!("{}_sect_vec", core_type_name))).collect();

	// This feels really really weird...
	let sdt_sect_range_vec_names_iter1 = sdt_sect_range_vec_names_tokens.iter();
	let sdt_sect_range_vec_names_iter2 = sdt_sect_range_vec_names_tokens.iter();
	let sdt_sect_range_vec_names_rep_iter = sdt_sect_range_vec_names_tokens.iter().map(|name| repeat(name));

	let new_func_tokens = quote!
	{
		impl SdtStruct
		{
			pub fn new() -> SdtStruct
			{
				#(
					let mut #sdt_sect_range_vec_names_iter1 = Vec::new();
					#(#sdt_sect_range_vec_names_rep_iter.push(Range { start: #range_start_iter, end: #range_end_iter });)*
				)*

				SdtStruct
				{
					#(#component_core_type_names_tokens_iter: SubVecsSystemContainer::new(#sdt_sect_range_vec_names_iter2),)*
				}
			}
		}
	};

	
	println!("SdtStruct new() tokens:\n{}", new_func_tokens);

	new_func_tokens.to_tokens(output);
}

fn determine_sect_ranges_for_each_sdt(reg_data: &RegisteredData) -> SdtRangesAndSdtCombToSectIdLookup
{
	let mut sect_ranges_for_each_sdt_container = Vec::new();
	let mut sdt_comb_id_to_sect_id_lookup =  Vec::new();
	// Note: sdt's are already be sorted by id.
	for sdt_id in reg_data.registered_system_data_types.iter().map(|sdt| sdt.id)
	{
		let sdts_of_combs_that_use_sdt = get_sdt_ids_of_each_comb_that_involves_sdt_for_all_reg_sdts(reg_data, sdt_id);
		let sys_comb_sect_hierarchy = determine_sect_hierarchy_for_isd_container(&sdts_of_combs_that_use_sdt);
		let section_ranges = determine_section_ranges_of_each_comb_in_sdt_container(&sys_comb_sect_hierarchy);

		let mut comb_id_to_sect_id_lookup = HashMap::new();
		for entry in sys_comb_sect_hierarchy.iter()
		{
			comb_id_to_sect_id_lookup.insert(entry.comb_id, entry.sect_idx);
		}

		sdt_comb_id_to_sect_id_lookup.push(comb_id_to_sect_id_lookup);
		sect_ranges_for_each_sdt_container.push(section_ranges);
	}

	SdtRangesAndSdtCombToSectIdLookup
	{
		sect_ranges_for_each_sdt_container,
		sdt_comb_id_to_sect_id_lookup
	}
}

fn generate_comb_iter_definitions(reg_info: &ProcessedRegistrationDataYAML,
	sdt_ranges_and_lookup: &SdtRangesAndSdtCombToSectIdLookup, output: &mut Tokens)
{
	// We need to generate iterator types for each combination
	// Later we will need to generate iterators that behave differently depending on ISDs and other stuff, but
	// since for now we are only dealing with components, we will only generate input and output iterators.

	let input_combs = get_list_of_unique_combs(&reg_info.registered_data, SystemInputOutput::Input);
	let output_combs = get_list_of_unique_combs(&reg_info.registered_data, SystemInputOutput::Output);

	let standard_input_iter_type_name = repeat("Iterator");
	let standard_input_iter_gen_func = repeat("generate_lockstep_input_iterator");

	// TODO!!!
	let standard_output_iter_type_name = repeat("???"); 
	let standard_output_iter_gen_func = repeat("generate_lockstep_output_iterator");

	for input_comb in input_combs.iter()
	{
		generate_comb_iter_definition(reg_info, sdt_ranges_and_lookup, input_comb,
			standard_input_iter_gen_func.clone(), standard_input_iter_type_name.clone(), output);
	}

	for output_comb in output_combs.iter()
	{
		generate_comb_iter_definition(reg_info, sdt_ranges_and_lookup, output_comb,
			standard_output_iter_gen_func.clone(), standard_output_iter_type_name.clone(), output);
	}

	fn get_list_of_unique_combs(reg_data: &RegisteredData, filter: SystemInputOutput) -> Vec<SystemCombinationUsageInstance>
	{
		let mut combs: Vec<SystemCombinationUsageInstance> = filter_system_combination_instances_by_type(reg_data.system_combination_usage_instances.iter().cloned(), filter).collect();
		combs.sort_unstable_by_key(|comb| comb.combination_id);
		combs.dedup();
		combs
	}
}

fn generate_comb_iter_definition<'a, IT, IF>(reg_info: &ProcessedRegistrationDataYAML,
	ranges_and_lookup: &SdtRangesAndSdtCombToSectIdLookup, comb_inst: &'a SystemCombinationUsageInstance,
	iterator_types: IT, iterator_gen_func_names: IF, output: &'a mut Tokens)
	where IT: Iterator<Item = &'a str>, IF: Iterator<Item = &'a str>
{
	let comb_isd_ids = get_system_data_type_ids_used_by_combination(&reg_info.registered_data, comb_inst.combination_id);
	let sdt_recs: Vec<RegisteredSystemDataType> = get_sdt_records_for_sdt_ids(&reg_info.registered_data, comb_isd_ids).collect();
	let sdt_names: Vec<String> = sdt_recs.iter().map(|sdt_rec| sdt_rec.name.to_string()).collect();
	let sdt_iter_names: Vec<TokenizableString> = sdt_names.iter().map(|name| TokenizableString::new(format!("{}_iter", name))).collect();
	let sect_idxs_iter = sdt_recs.iter().map(|sdt_rec| ranges_and_lookup.sdt_comb_id_to_sect_id_lookup[sdt_rec.id][&comb_inst.combination_id]);
	let iter_gen_func_calls: Vec<TokenizableString> = itertools::izip!(iterator_gen_func_names, sdt_names.iter(), sect_idxs_iter)
		.map(|(func_name, sdt_name, sect_idx)| create_gen_iter_func_call(func_name, sdt_name, sect_idx)).collect();

	let comb_iter_name = generate_comb_iter_name_from_comb(&reg_info.registered_data, &comb_inst);
	let sdt_iter_names_iter_1 = sdt_iter_names.iter();
	let sdt_iter_names_iter_2 = sdt_iter_names.iter();
	let core_types_iter = sdt_names.iter().map(|sdt_name| TokenizableString::new(reg_info.sdt_name_to_core_type_lookup[sdt_name].clone()));
	let iter_gen_func_calls_iter = iter_gen_func_calls.iter();

	let iter_def_tokens = quote!
	{
		struct #comb_iter_name
		{
			#(#sdt_iter_names_iter_1: #iterator_types<Item = #core_types_iter>)*,
		}

		impl #comb_iter_name
		{
			pub fn new(sdt_struct: &SdtStruct) -> #comb_iter_name
			{
				#comb_iter_name
				{
					#(#sdt_iter_names_iter_2: #iter_gen_func_calls_iter)*,
				}
			}
		}
	};

	println!("{}", iter_def_tokens);
	iter_def_tokens.to_tokens(output);


	fn create_gen_iter_func_call(func_name: &str, sdt_name: &str, sect_idx: usize) -> TokenizableString
	{
		TokenizableString::new(format!("{}(sdt_struct.{}.get_sect_slice({}))", func_name, sdt_name, sect_idx))
	}
}

fn generate_system_run_funcs_init_func(reg_data: &RegisteredData, output: &mut Tokens)
{
	// For each system, generate a func that generates the iterators to run the system and then pass these iters to the sys run func.

	let run_func_tokens = generate_run_func_tokens(reg_data);
	let init_run_funcs_func_tokens = quote!
	{
		fn init_run_funcs() -> Vec<Fn(sdt_struct: &SdtStruct)>
		{
			let mut run_funcs = Vec::new();
			#(
				#run_func_tokens
				run_funcs.push(run_func); // run_func is defined in run_func_tokens
			)*
			run_funcs
		}
	};

	init_run_funcs_func_tokens.to_tokens(output);

	fn generate_run_func_tokens(reg_data: &RegisteredData) -> Vec<Tokens>
	{
		let mut init_func_tokens = Vec::new();
		for sys in reg_data.registered_systems.iter()
		{
			let sys_name = sys.name;

			let run_func_tokens;
			match sys.system_type
			{
				// Note that for now we are just using the sys name as the run func name. May change later.
				SystemType::Both => 
				{
					let inp_comb_iter_name = get_comb_iter_type_name_used_by_sys(reg_data, sys.id, SystemInputOutput::Input);
					let out_comb_iter_name = get_comb_iter_type_name_used_by_sys(reg_data, sys.id, SystemInputOutput::Output);
					run_func_tokens = quote!
					{
						let run_func = |sdt_struct|
						{
							input_iter = #inp_comb_iter_name.new(sdt_struct);
							output_iter = #out_comb_iter_name.new(sdt_struct);
							#sys_name(input_iter, output_iter);
						};
					};
				},
				SystemType::OnlyInput =>
				{
					let inp_comb_iter_name = get_comb_iter_type_name_used_by_sys(reg_data, sys.id, SystemInputOutput::Input);
					run_func_tokens = quote!
					{
						let run_func = |sdt_struct|
						{
							input_iter = #inp_comb_iter_name.new(sdt_struct);
							#sys_name(input_iter);
						};
					};
				},
				SystemType::OnlyOutput => 
				{
					let out_comb_iter_name = get_comb_iter_type_name_used_by_sys(reg_data, sys.id, SystemInputOutput::Output);
					run_func_tokens = quote!
					{
						let run_func = |sdt_struct|
						{
							output_iter = #out_comb_iter_name.new(sdt_struct);
							#sys_name(input_iter, output_iter);
						};
					};
				},
				SystemType::NoInputOrOutput =>
				{
					run_func_tokens = quote!
					{
						let run_func = |sdt_struct|
						{
							#sys_name();
						};
					};
				}
			}

			init_func_tokens.push(run_func_tokens);
		}
		init_func_tokens
	}

	fn get_comb_iter_type_name_used_by_sys(reg_data: &RegisteredData, sys_id: usize, input_output: SystemInputOutput) -> TokenizableString
	{
		unimplemented!()
	}
}

fn generate_comb_iter_name_from_comb(reg_data: &RegisteredData, comb_inst: &SystemCombinationUsageInstance) -> String
{
	let comb_name = get_combination_records_iter_for_combination_ids(&reg_data, once(comb_inst.combination_id))
		.map(|comb_rec| comb_rec.name).next().unwrap();

	let input_output_str = match comb_inst.system_input_output
	{
		SystemInputOutput::Input => "input",
		SystemInputOutput::Output => "output"
	};

	format!("{}_{}_iter", input_output_str, comb_name)
}


fn determine_sect_hierarchy_for_isd_container(all_sdts_of_combs_containing_sdt: &Vec<Vec<usize>>) -> Vec<CombSectAndChildrenIdxs>
{
	unimplemented!()
}

fn determine_section_ranges_of_each_comb_in_sdt_container(sdt_ids_of_combs_that_sdt_uses: &Vec<CombSectAndChildrenIdxs>) -> Vec<Range<usize>>
{
	unimplemented!()
}

fn get_sdt_ids_of_each_comb_that_involves_sdt_for_all_reg_sdts(reg_data: &RegisteredData, sdt_id: usize) -> Vec<Vec<usize>>
{
	// AKA: Get all combinations that the sdt appears in and record the sdt ids of each combination
	let mut sdts_of_combs_that_use_sdt = Vec::new();
	for comb_id in get_all_combination_ids_that_use_a_system_data_type(&reg_data, sdt_id)
	{
		let mut sdt_ids_of_comb = Vec::new();
		for sdt_id in get_system_data_type_ids_used_by_combination(&reg_data, comb_id)
		{
			sdt_ids_of_comb.push(sdt_id);
		}
		sdts_of_combs_that_use_sdt.push(sdt_ids_of_comb);
	}

	sdts_of_combs_that_use_sdt
}