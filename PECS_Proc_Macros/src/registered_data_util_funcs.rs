use pecs_common::types::{SdtType, SystemType, SystemInputOutput, RegisteredData, RegisteredSystemDataType, RegisteredCombination,
	RegisteredSystem, SystemCombinationUsageInstance};

pub fn get_sys_comb_insts_with_comb_id<'a>(registered_data: &'a RegisteredData, combination_id: usize) -> impl Iterator<Item = SystemCombinationUsageInstance> + 'a
{
	registered_data.system_combination_usage_instances.iter()
		.filter(move |sys_comb_inst| sys_comb_inst.combination_id == combination_id).cloned()
}

pub fn get_system_combination_instances_used_by_system<'a>(registered_data: &'a RegisteredData, system_id: usize)
	-> impl Iterator<Item = SystemCombinationUsageInstance> + 'a
{
	registered_data.system_combination_usage_instances.iter()
		.filter(move |combination_instance| combination_instance.system_id == system_id).cloned()
}

pub fn get_combination_records_iter_for_combination_ids<'a, I: 'a>(registered_data: &'a RegisteredData<'a>,
	 comb_id_iterator: I) -> impl Iterator<Item = RegisteredCombination<'a>> + 'a
	where I: Iterator<Item = usize>
{
	comb_id_iterator.map(move |comb_id| registered_data.registered_combinations[comb_id].clone())
}

pub fn get_system_combination_instances_for_combination_ids<'a, I: 'a>(registered_data: &'a RegisteredData<'a>,
	 comb_id_iter: I) -> impl Iterator<Item = SystemCombinationUsageInstance> + 'a
	where I: Iterator<Item = usize> + Clone
{
	registered_data.system_combination_usage_instances.iter()
	.filter(move |sys_comb_inst| comb_id_iter.clone()
		.any(|comb_id| sys_comb_inst.combination_id == comb_id)).cloned()
}

pub fn get_system_data_type_ids_used_by_combination<'a>(registered_data: &'a RegisteredData<'a>, combination_id: usize)
	-> impl Iterator<Item = usize> + 'a
{
	registered_data.system_data_type_usage_instances.iter()
		.filter(move |system_data_instance| system_data_instance.combination_id == combination_id)
		.map(|system_data_instance| system_data_instance.system_data_type_id)
}

pub fn system_data_is_entirely_components<'a, I>(registered_data: &'a RegisteredData<'a>, mut system_data_type_ids_iter: I) -> bool
	where I: Iterator<Item = &'a usize>
{
	system_data_type_ids_iter.all(|&system_data_type_id| 
		registered_data.registered_system_data_types[system_data_type_id].data_type == SdtType::Component)
}

pub fn system_inputs_not_outputs_of_other_systems<'a, I>(registered_data: &'a RegisteredData,
	system_input_data_type_ids_iter: I) -> bool
	where I: Iterator<Item = &'a usize>
{
	// Find all combinations that involve the input data types specified
	let mut all_combinations_using_given_system_input_data_types = 
		system_input_data_type_ids_iter.flat_map(|&system_data_type_id|
		get_all_combination_ids_that_use_a_system_data_type(registered_data, system_data_type_id));
	
	// Check if all of the combinations are not outputs
	all_combinations_using_given_system_input_data_types
	.all(|comb_id| registered_data.system_combination_usage_instances.iter()
		.find(|sys_comb_inst| sys_comb_inst.combination_id == comb_id).unwrap().system_input_output != SystemInputOutput::Input)
}

// TODO: Change to return the actual records
pub fn get_all_combination_ids_that_use_a_system_data_type<'a>(registered_data: &'a RegisteredData<'a>,
	system_data_type_id: usize) -> impl Iterator<Item = usize> + 'a
{
	registered_data.system_data_type_usage_instances.iter()
		.filter(move |&system_data_instance| system_data_instance.system_data_type_id == system_data_type_id)
		.map(|comb_rec| comb_rec.combination_id)
}

pub fn get_input_output_system_data_type_ids_for_system<'a>(registered_data: &'a RegisteredData<'a>, system_id: usize, input_output_filter: SystemInputOutput)
	-> impl Iterator<Item = usize> + 'a
{
	let sys_comb_inst_iter = get_system_combination_instances_used_by_system(registered_data, system_id);
	let mut filtered_system_combinations_iter = filter_system_combination_instances_by_type(sys_comb_inst_iter, input_output_filter);
	// NOTE: If we call this on a system that doesn't have an Input/Output it will panic!
	let combination_id = filtered_system_combinations_iter.next().unwrap().combination_id;
	
	// Should never have more than one combination in the iter at this point
	get_system_data_type_ids_used_by_combination(registered_data, combination_id)
}

pub fn get_sdt_records_for_sdt_ids<'a, I>(reg_data: &'a RegisteredData, sdt_id_iter: I) -> impl Iterator<Item = RegisteredSystemDataType<'a>>
	where I: Iterator<Item = usize>
{
	sdt_id_iter.map(move |sdt_id| reg_data.registered_system_data_types.iter().find(|sdt_rec| sdt_rec.id == sdt_id).unwrap().clone())
}

pub fn filter_systems_by_type<'a, I: 'a>(registered_systems_iter: I, system_type_filter: SystemType) -> impl Iterator<Item = RegisteredSystem<'a>> + 'a
	where I: Iterator<Item = &'a RegisteredSystem<'a>>
{
	registered_systems_iter.filter(move |sys_rec| sys_rec.system_type == system_type_filter)
	.cloned()
}

pub fn filter_system_combination_instances_by_type<'a, I: 'a>(
	registered_combination_iter: I, system_type_filter: SystemInputOutput) -> impl Iterator<Item = SystemCombinationUsageInstance> + 'a
	where I: Iterator<Item = SystemCombinationUsageInstance>
{
	registered_combination_iter.filter(move |sys_comb_inst| sys_comb_inst.system_input_output == system_type_filter)
}

pub fn filter_components<'a, I: 'a>(registered_system_data_types_type_iter: I) -> impl Iterator<Item = RegisteredSystemDataType<'a>> + 'a
	where I: Iterator<Item = RegisteredSystemDataType<'a>>
{
	registered_system_data_types_type_iter
		.filter(|system_data_type| system_data_type.data_type == SdtType::Component)
}

pub fn filter_system_data_type_ids_by_component_isd<'a, I: 'a>(registered_data: &'a RegisteredData<'a>, system_data_type_ids_iter: I, component_isd_filter: SdtType)
	-> impl Iterator<Item = usize> + 'a
	where I: Iterator<Item = usize>
{
	system_data_type_ids_iter.filter(move |&system_data_type_id| registered_data.registered_system_data_types[system_data_type_id].data_type == component_isd_filter)
}