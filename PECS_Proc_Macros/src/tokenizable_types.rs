use quote::{Tokens, ToTokens};

use pecs_common::types::convert_type_to_tokens;

#[allow(unused_macros)]
macro_rules! CreateTokenizableType
{
    ($type_name:ident, $type_to_tokenize:ty, $($field_name:ident),*) =>
    {
        pub struct $type_name
        {
            internal_type: $type_to_tokenize
        }

        impl $type_name
        {
            pub fn new(internal_type: $type_to_tokenize) -> $type_name
            {
                $type_name
                {
                    internal_type
                }
            }
        }

        impl ToTokens for $type_name
        {
            fn to_tokens(&self, tokens: &mut Tokens)
            {
                tokens.append(convert_type_to_tokens("TokenizableRange {"));
                $(
                    tokens.append(convert_type_to_tokens(stringify!($field_name)));
                    tokens.append(convert_type_to_tokens(": self.type."));
                    tokens.append(convert_type_to_tokens(stringify!($field_name)));
                    tokens.append(convert_type_to_tokens(","));
                )*
                tokens.append(convert_type_to_tokens("}"));
            }
        }
    }
}

#[derive(Clone)]
pub struct TokenizableString
{
	pub token_string: String
}

impl TokenizableString
{
	pub fn new(token_string: String) -> TokenizableString
	{
		TokenizableString { token_string }
	}
}

impl ToTokens for TokenizableString
{
	fn to_tokens(&self, tokens: &mut Tokens)
	{
		tokens.append(convert_type_to_tokens(self.token_string.as_str()))
	}
}

#[allow(dead_code)]
pub struct TokenizableSlice<'a, T: 'a>
    where T: ToTokens
{
    slice: &'a [T]
}

impl<'a, T: 'a> TokenizableSlice<'a, T>
    where T: ToTokens
{
    #[allow(dead_code)]
    pub fn new(slice: &'a [T]) -> TokenizableSlice<T>
    {
        TokenizableSlice
        {
            slice
        }
    }
}

impl<'a, T> ToTokens for TokenizableSlice<'a, T>
    where T: ToTokens
{
    fn to_tokens(&self, tokens: &mut Tokens)
    {
        tokens.append(convert_type_to_tokens("["));
        for item in self.slice.iter()
        {
            item.to_tokens(tokens);
            tokens.append(convert_type_to_tokens(","));
        }
        tokens.append(convert_type_to_tokens("]"));
    }
}
