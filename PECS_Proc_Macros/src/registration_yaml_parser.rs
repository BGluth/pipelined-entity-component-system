use std::collections::HashMap;
use std::iter;

use yaml_rust::Yaml;

use pecs_common::util_functions::load_in_yaml;
use pecs_common::types::{RegisteredData, SdtType, SystemType, SystemInputOutput};
use pecs_common::types::{RegisteredSystemDataType, RegisteredCombination, RegisteredSystem, SystemCombinationUsageInstance, SystemDataTypeCombinationUsageInstance};

pub struct ProcessedRegistrationDataYAML<'a>
{
	pub registered_data: RegisteredData<'a>,
	pub sdt_name_to_core_type_lookup: HashMap<String, String>
}

pub struct CoreType
{
	pub sdt_name: String,
	pub core_type: String
}

struct ExtractedRegistrationDataYAML
{
	pub system_data_types: Vec<ExtractedSystemDataType>,
	pub systems: Vec<ExtractedSystemType>,
	pub combinations: Vec<ExtractedCombination>,
	pub system_combination_usage_instance: Vec<SystemCombinationUsageInstance>,
	pub system_data_type_combination_usage_instances: Vec<SystemDataTypeCombinationUsageInstance>,
}

struct ExtractedCoreTypesYAML
{
	pub component_core_types_vec: Vec<CoreType>,
	pub isd_core_types_vec: Vec<CoreType>
}

struct ExtractedSystemDataType
{
	pub name: String,
	pub data_type: SdtType,
	pub id: usize
}

struct ExtractedCombination
{
	pub name: String,
	pub id: usize
}

struct ExtractedSystemType
{
	pub name: String,
	pub system_type: SystemType,
	pub id: usize
}

struct CombinationData
{
	sys_ids_that_use_comb_and_comb_usage_input_or_output: Vec<(usize, SystemInputOutput)>,
	system_data_type_ids_used: Vec<usize>
}

pub fn extract_data_from_file<'a, 'b>(registration_yaml_path: &'b str, string_holder: &'a mut Vec<String>) -> Result<ProcessedRegistrationDataYAML<'a>, String>
{
	let (extracted_reg_data, extracted_core_types) = extract_data_from_yaml(registration_yaml_path)?;
	let reg_data = assemble_yaml_data_into_reg_data(extracted_reg_data, string_holder);
	let sdt_name_to_core_type_lookup = create_sdt_id_to_core_type_lookup(extracted_core_types);

	return Ok(ProcessedRegistrationDataYAML
	{
		registered_data: reg_data,
		sdt_name_to_core_type_lookup
	});

	fn extract_data_from_yaml<'a>(registration_yaml_path: &'a str) -> Result<(ExtractedRegistrationDataYAML, ExtractedCoreTypesYAML), String>
	{
		let yaml = load_in_yaml(&registration_yaml_path)?;
		
		let system_data_types_yaml = &yaml["SystemDataTypes"];
		let systems_yaml = &yaml["Systems"];
		
		let components = &system_data_types_yaml["Components"];
		let isds = &system_data_types_yaml["ISDs"];
		
		let (component_names_vec, component_core_types_vec) = load_system_data_type_names_and_core_types(components, "components")?;
		let (isd_names_vec, isd_core_types_vec) = load_system_data_type_names_and_core_types(isds, "ISDs")?;
		
		let all_registered_system_data_types = extract_all_registered_system_data_types(component_names_vec, isd_names_vec);
		let (extracted_systems, comb_names_and_usages) = extract_systems_and_record_comb_and_sys_data_type_usage(&systems_yaml, &all_registered_system_data_types)?;
		
		let extracted_combinations: Vec<ExtractedCombination> = comb_names_and_usages.keys().enumerate().map(
			|(comb_id, string_ref)| ExtractedCombination { name: string_ref.to_string(), id: comb_id }).collect();

		let (sys_comb_usage_insts, sys_data_type_comb_usage_insts) = extract_instances(&extracted_combinations, &comb_names_and_usages)?;

		let reg_data = ExtractedRegistrationDataYAML

		{
			system_data_types: all_registered_system_data_types,
			systems: extracted_systems,
			combinations: extracted_combinations,
			system_combination_usage_instance: sys_comb_usage_insts,
			system_data_type_combination_usage_instances: sys_data_type_comb_usage_insts,
		};

		let core_types = ExtractedCoreTypesYAML
		{
			component_core_types_vec,
			isd_core_types_vec
		};

		return Ok((reg_data, core_types));

		fn extract_systems_and_record_comb_and_sys_data_type_usage(systems_yaml: &Yaml,
		all_registered_system_data_types_vec: &Vec<ExtractedSystemDataType>)
			-> Result<(Vec<ExtractedSystemType>, HashMap<String, CombinationData>), String>
		{
			let mut sys_data_type_name_to_id_lookup = HashMap::new();
			for ref registered_system_type in all_registered_system_data_types_vec
			{
				sys_data_type_name_to_id_lookup.insert(registered_system_type.name.as_str(), registered_system_type.id);
			}
			
			Ok(extract_systems_and_record_comb_and_sys_data_type_usage_from_yaml(systems_yaml, &sys_data_type_name_to_id_lookup)?)
		}

		fn extract_instances(combinations: &Vec<ExtractedCombination>, comb_names_and_usages: &HashMap<String, CombinationData>)
			-> Result<(Vec<SystemCombinationUsageInstance>, Vec<SystemDataTypeCombinationUsageInstance>), String>
		{
			let mut comb_name_to_id_lookup = HashMap::with_capacity(comb_names_and_usages.len());
			for ref ext_comb in combinations.iter()
			{
				comb_name_to_id_lookup.insert(ext_comb.name.as_str(), ext_comb.id);
			}
			
			let sys_comb_usage_insts = create_comb_id_sys_id_and_sys_type_vec(&comb_names_and_usages, &comb_name_to_id_lookup)?;
			let sys_data_type_comb_usage_insts = create_sys_data_type_comb_usage_vec(&comb_names_and_usages, &comb_name_to_id_lookup)?;

			Ok((sys_comb_usage_insts, sys_data_type_comb_usage_insts))
		}
	}

	fn assemble_yaml_data_into_reg_data<'a>(extracted_yaml: ExtractedRegistrationDataYAML, string_holder_vec: &'a mut Vec<String>) -> RegisteredData<'a>
	{
		let mut registered_system_data_types = Vec::new();
		let mut registered_systems = Vec::new();
		let mut registered_combinations = Vec::new();

		{
			let strings_to_add_to_string_holder_iter = 
			extracted_yaml.system_data_types.iter().map(|x| x.name.to_string())
			.chain(extracted_yaml.systems.iter().map(|x| x.name.to_string()))
			.chain(extracted_yaml.combinations.iter().map(|x| x.name.to_string()));

			for string in strings_to_add_to_string_holder_iter
			{
				string_holder_vec.push(string);
			}
		}

		let mut curr_idx = 0;

		for sys_data_type in extracted_yaml.system_data_types
		{
			let reg_sys_type = RegisteredSystemDataType { name: &string_holder_vec[curr_idx], data_type: sys_data_type.data_type, id: sys_data_type.id };
			registered_system_data_types.push(reg_sys_type);
			curr_idx += 1;
		}

		for system in extracted_yaml.systems
		{
			let reg_sys = RegisteredSystem { name: &string_holder_vec[curr_idx], system_type: system.system_type, id: system.id };
			registered_systems.push(reg_sys);
			curr_idx += 1;
		}

		for combination in extracted_yaml.combinations
		{
			let reg_comb = RegisteredCombination { name: &string_holder_vec[curr_idx], id: combination.id };
			registered_combinations.push(reg_comb);
			curr_idx += 1;
		}

		RegisteredData
		{
			registered_system_data_types: registered_system_data_types,
			registered_combinations: registered_combinations,
			registered_systems: registered_systems,
			system_data_type_usage_instances: extracted_yaml.system_data_type_combination_usage_instances,
			system_combination_usage_instances: extracted_yaml.system_combination_usage_instance
		}
	}

	fn create_sdt_id_to_core_type_lookup(extracted_core_types: ExtractedCoreTypesYAML) -> HashMap<String, String>
	{
		let mut sdt_id_to_core_type_lookup = HashMap::new();
		for extracted_core_type in extracted_core_types.component_core_types_vec.into_iter().chain(extracted_core_types.isd_core_types_vec.into_iter())
		{
			sdt_id_to_core_type_lookup.insert(extracted_core_type.sdt_name, extracted_core_type.core_type);
		}

		sdt_id_to_core_type_lookup
	}
}

fn load_system_data_type_names_and_core_types(system_data_types_yaml: &Yaml, components_or_isds: &str) -> Result<(Vec<String>, Vec<CoreType>), String>
{
	let mut system_data_type_names = Vec::new();
	let mut system_data_type_core_type_names = Vec::new();
	
	let system_data_types_yaml_vec = system_data_types_yaml.as_vec().ok_or(format!("Could not find a yaml vec when looking for system data types. {}", components_or_isds))?;
	
	for (idx, system_data_type_yaml) in system_data_types_yaml_vec.iter().enumerate()
	{
		let name = system_data_type_yaml["Name"].as_str().ok_or(format!("Failed to find a Name field in the list of {} at index {}.", components_or_isds, idx))?;
		system_data_type_names.push(name.to_string());
		
		let core_type = system_data_type_yaml["CoreType"].as_str().ok_or(format!("Failed to find a CoreType field in the list of {} for {} at index {}", components_or_isds, name, idx))?;
		system_data_type_core_type_names.push(CoreType { sdt_name: name.to_string(), core_type: core_type.to_string() } );
	}
	
	Ok((system_data_type_names, system_data_type_core_type_names))
}

fn extract_all_registered_system_data_types<'a>(comp_names_vec: Vec<String>, isd_names_vec: Vec<String>)
	-> Vec<ExtractedSystemDataType>
{
	let comp_type_iter = iter::repeat(SdtType::Component).take(comp_names_vec.len());
	let isd_type_iter = iter::repeat(SdtType::Isd).take(isd_names_vec.len());
	let sys_comp_or_isd_iter = comp_type_iter.chain(isd_type_iter);
	
	let num_sys_data_types = comp_names_vec.len() + isd_names_vec.len();
	let sys_data_type_ids_iter = 0..num_sys_data_types;
	
	let comp_names_iter = comp_names_vec.into_iter();
	let isd_names_iter = isd_names_vec.into_iter();
	let sys_data_type_names_iter = comp_names_iter.chain(isd_names_iter);
	
	let mut sys_data_types_vec = Vec::with_capacity(num_sys_data_types);
	for ((sys_name, sys_type), sys_id) in sys_data_type_names_iter.zip(sys_comp_or_isd_iter).zip(sys_data_type_ids_iter)
	{
		sys_data_types_vec.push(ExtractedSystemDataType { name: sys_name, data_type: sys_type, id: sys_id});
	} 
	
	sys_data_types_vec
}

fn extract_systems_and_record_comb_and_sys_data_type_usage_from_yaml<'a>(systems_yaml: &Yaml, sys_data_type_name_to_id_lookup: &HashMap<&str, usize>)
	-> Result<(Vec<ExtractedSystemType>, HashMap<String, CombinationData>), String>
{
	let sys_yaml_vec = systems_yaml.as_vec().ok_or("Could not find the \"Systems\" section in the YAML.")?;
	let mut comb_names_and_usages = HashMap::new();
	let mut registered_systems = Vec::with_capacity(sys_yaml_vec.len());
	
	for (sys_id, sys_yaml) in sys_yaml_vec.into_iter().enumerate()
	{
		let name = sys_yaml["Name"].as_str().ok_or("Name field for system at idx {} is not a string.")?;
		let (system_type, inputs, outputs) = extract_used_sys_data_types(sys_yaml)?;
		
		registered_systems.push(ExtractedSystemType { name: name.to_string(), system_type: system_type, id: sys_id });
		extract_sys_comb_info(inputs, &mut comb_names_and_usages, sys_data_type_name_to_id_lookup, sys_id, SystemInputOutput::Input)?;
		extract_sys_comb_info(outputs, &mut comb_names_and_usages, sys_data_type_name_to_id_lookup, sys_id, SystemInputOutput::Output)?;
	}
	
	Ok((registered_systems, comb_names_and_usages))
}

fn extract_used_sys_data_types(systems_yaml: &Yaml) -> Result<(SystemType, Option<Vec<String>>, Option<Vec<String>>), String>
{
	let (inputs, outputs) = (extract_vec_from_yaml(&systems_yaml, "Input")?, extract_vec_from_yaml(&systems_yaml, "Output")?);
	
	let system_type = match (&inputs, &outputs)
	{
		(&Some(_), &Some(_)) => SystemType::Both,
		(&Some(_), &None) => SystemType::OnlyInput,
		(&None, &Some(_)) => SystemType::OnlyOutput,
		(&None, &None) => SystemType::NoInputOrOutput
	};
	
	Ok((system_type, inputs, outputs))
}

fn extract_vec_from_yaml(yaml: &Yaml, field_name: &str) -> Result<Option<Vec<String>>, String>
{
	// TODO: Maybe pass in more information for error messages to use?
	let mut vec: Result<Option<Vec<String>>, String> = Ok(None);
	let field = &yaml[field_name];
	if !field.is_badvalue()
	{
		let yaml_vec = field.as_vec().ok_or(format!("Failed to parse the \"{}\" field as a vec.", field_name))?;
		
		let mut buff = Vec::with_capacity(yaml_vec.len());
		for (idx, yaml_item) in yaml_vec.into_iter().enumerate()
		{
			let string = yaml_item.as_str().ok_or(format!("Failed to convert the value in the yaml list \"{}\" at index \"{}\" to a string.", field_name, idx))?.to_string();
			buff.push(string);
		}
		
		vec = Ok(Some(buff));
	}
	
	vec
}

fn extract_sys_comb_info(sys_data_type_names_opt: Option<Vec<String>>,
	comb_names_and_usages: &mut HashMap<String, CombinationData>,
	sys_data_type_name_to_id_lookup: &HashMap<&str, usize>, sys_id: usize, system_input_or_output: SystemInputOutput)
	-> Result<(), String>
{
	fn create_entry_for_combination<I>(sys_data_type_names_iter: I, sys_data_type_name_to_id_lookup: &HashMap<&str, usize>)
		-> Result<CombinationData, String>
		where I: Iterator<Item = String> 
	{
		Ok(CombinationData 
		{ 
			sys_ids_that_use_comb_and_comb_usage_input_or_output: Vec::new(),
			system_data_type_ids_used: get_sys_data_type_ids_used_in_comb(sys_data_type_names_iter, sys_data_type_name_to_id_lookup)?
		})
	}
	
	fn get_sys_data_type_ids_used_in_comb<I>(sys_data_type_names_iter: I, sys_data_type_name_to_id_lookup: &HashMap<&str, usize>) -> Result<Vec<usize>, String>
		where I: Iterator<Item = String>
	{
		let mut comb_used_sys_data_type_ids = Vec::new();
		for sys_data_type_name in sys_data_type_names_iter
		{
			let id = sys_data_type_name_to_id_lookup.get(sys_data_type_name.as_str()).ok_or(
				format!("Failed to find an id for the system data type name \"{}\" when looking up the list of system data type ids that make up an combination. \
				\nSystem data type lookup table: {:#?}", sys_data_type_name, sys_data_type_name_to_id_lookup))?;
			comb_used_sys_data_type_ids.push(*id);
		}
		
		Ok(comb_used_sys_data_type_ids)
	}
	
	
	let mut sys_data_type_names = match sys_data_type_names_opt
	{
		Some(sys_data_type_names) => sys_data_type_names,
		None => return Ok(())
	};
	
	sys_data_type_names.sort();
	let combination_key = sys_data_type_names.iter()
		.flat_map(|name| name.chars()).collect();
		
	let &mut CombinationData { ref mut sys_ids_that_use_comb_and_comb_usage_input_or_output, .. } = comb_names_and_usages
		.entry(combination_key).or_insert(create_entry_for_combination(sys_data_type_names.into_iter(), sys_data_type_name_to_id_lookup)?);
	
	// Add sys id to systems that use comb
	sys_ids_that_use_comb_and_comb_usage_input_or_output.push((sys_id, system_input_or_output));
	Ok(())
}

fn create_comb_id_sys_id_and_sys_type_vec<'a>(comb_names_and_usages: &'a HashMap<String, CombinationData>,
	comb_name_to_id_lookup: &'a HashMap<&'a str, usize>) -> Result<Vec<SystemCombinationUsageInstance>, String>
{
	let mut comb_id_sys_id_and_sys_type_vec = Vec::new();
	for (ref comb_name, &CombinationData { ref sys_ids_that_use_comb_and_comb_usage_input_or_output, .. }) in comb_names_and_usages.into_iter()
	{
		let comb_id = comb_name_to_id_lookup.get(comb_name.as_str()).ok_or(format!("Failed to find an id registered for the combination name \"{}\". (create_comb_id_sys_id_and_sys_type_vec)", comb_name))?;
		let comb_id_sys_id_and_sys_type_iter = sys_ids_that_use_comb_and_comb_usage_input_or_output.iter()
		.map(|sys_id_and_sys_type|
		{
			let &(ref sys_id, ref sys_type) = sys_id_and_sys_type;
			SystemCombinationUsageInstance 
			{
				combination_id: *comb_id,
				system_id: *sys_id,
				system_input_output: sys_type.clone(),
				group_id: 0
			}
		});
		comb_id_sys_id_and_sys_type_vec.extend(comb_id_sys_id_and_sys_type_iter);
	}
	
	Ok(comb_id_sys_id_and_sys_type_vec)
}

fn create_sys_data_type_comb_usage_vec(
	comb_names_and_usages_hash: &HashMap<String, CombinationData>,
	comb_name_id_lookup: &HashMap<&str, usize>)
	-> Result<Vec<SystemDataTypeCombinationUsageInstance>, String>
{
	let mut sys_data_type_comb_usage_vec = Vec::new();
	
	for (comb_name, &CombinationData { ref system_data_type_ids_used, .. }) in comb_names_and_usages_hash.into_iter()
	{
		let comb_id = comb_name_id_lookup.get(comb_name.as_str()).ok_or(format!("Failed to find an id registered for the combination name \"{}\" (create_sys_data_type_comb_usage_vec)", comb_name))?;
		let sys_data_type_ids_iter = system_data_type_ids_used.iter()
			.map(|sys_data_type_id| SystemDataTypeCombinationUsageInstance { combination_id: *comb_id, system_data_type_id: *sys_data_type_id });
			sys_data_type_comb_usage_vec.extend(sys_data_type_ids_iter);
	}
	
	Ok(sys_data_type_comb_usage_vec)
}