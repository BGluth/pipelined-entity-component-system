use std::collections::HashSet;
use std::cmp::Ordering;

use pecs_common::types::{SdtType, SystemType, SystemInputOutput, RegisteredData,
	SystemCombinationUsageInstance, SystemOrderInfo, SystemDataTypeSystemId,
	SysDataTypeOutputAndIntakeSystemInstance};

use registered_data_util_funcs::{filter_components, get_all_combination_ids_that_use_a_system_data_type,
	get_system_combination_instances_for_combination_ids, filter_system_combination_instances_by_type,
	get_system_data_type_ids_used_by_combination, get_sys_comb_insts_with_comb_id, system_data_is_entirely_components,
	get_input_output_system_data_type_ids_for_system, get_system_combination_instances_used_by_system};

pub fn generate_system_dependencies(registered_data: &RegisteredData)
	 -> SystemOrderInfo
{
	let mut system_order_info = SystemOrderInfo 
	{
		init_system_ids: Vec::new(),
		system_data_type_orgin_system: Vec::with_capacity(registered_data.registered_combinations.len()),
		system_output_and_intake_system_vec: Vec::new()
	};
	
	determine_init_systems(registered_data, &mut system_order_info);
	determine_non_init_system_run_order(registered_data, &mut system_order_info);
	
	system_order_info
}

fn determine_init_systems<'a>(registered_data: &'a RegisteredData, system_order_info: &mut SystemOrderInfo)
{
	// All systems that only have an output can be run at the very start and are therefore init systems
	let output_only_system_ids_buff: Vec<usize> = registered_data.registered_systems.iter().filter(|&current_system|
		current_system.system_type == SystemType::OnlyOutput).map(|system| system.id).collect();
	system_order_info.init_system_ids.extend(output_only_system_ids_buff.iter()); // TODO: Also change to get a copy of system ID!
	
	let orgin_records_for_output_only_system_iter = create_orgin_records_of_data_type_ids_for_system_ids(
		registered_data, output_only_system_ids_buff.iter().cloned(), SystemInputOutput::Output);
	system_order_info.system_data_type_orgin_system.extend(orgin_records_for_output_only_system_iter);
	
	let mut initial_system_candidate_buff = Vec::new();
	let mut candidate_systems_data_type_id_buff = Vec::new();
	let mut system_data_combination_ids_buff = Vec::new();
	let mut input_combination_system_data_type_ids_buff = Vec::new();
	
	for system_data_type in filter_components(registered_data.registered_system_data_types.iter().cloned())
	{
		initial_system_candidate_buff.clear();
		candidate_systems_data_type_id_buff.clear();
		system_data_combination_ids_buff.clear();
		
		// Get all input combinations that contain the system data type
		system_data_combination_ids_buff.extend(get_all_combination_ids_that_use_a_system_data_type(&registered_data, system_data_type.id));
		let sys_comb_inst_iter = get_system_combination_instances_for_combination_ids(&registered_data, system_data_combination_ids_buff.iter().cloned());
		let input_combination_records_iter = filter_system_combination_instances_by_type(sys_comb_inst_iter, SystemInputOutput::Input);
		
		// Go through each combination and filter out any combinations that do not contain only components or contain types that already have orgin systems
		for sys_comb_inst in input_combination_records_iter
		{
			input_combination_system_data_type_ids_buff.clear();
			input_combination_system_data_type_ids_buff.extend(get_system_data_type_ids_used_by_combination(&registered_data, sys_comb_inst.combination_id));
			
			if system_data_is_entirely_components(&registered_data, input_combination_system_data_type_ids_buff.iter()) &&
				system_data_does_not_have_orgin_systems(input_combination_system_data_type_ids_buff.iter(), system_order_info.system_data_type_orgin_system.as_slice())
			{
				// The system is a candidate. Record the system's input data types and later pick a system from the list of candidates
				let system_ids_that_use_combination_as_input_iter = get_sys_comb_insts_with_comb_id(registered_data, sys_comb_inst.combination_id)
					.map(|sys_comb_inst| sys_comb_inst.system_id);
				
				// Since a combination may be used by multiple systems, be need to include each system as a candiate. (Slightly hacky)
				for system_id in system_ids_that_use_combination_as_input_iter
				{
					let combination_system_data_count = input_combination_system_data_type_ids_buff.len();
					let id_start_idx = candidate_systems_data_type_id_buff.len();
					let id_end_idx = id_start_idx + combination_system_data_count;
					
					// Add the combination's system data type ids to a buffer (duplicate data here) and record the start and end idxs for each system
					candidate_systems_data_type_id_buff.extend(input_combination_system_data_type_ids_buff.iter());
					initial_system_candidate_buff.push((system_id, combination_system_data_count, id_start_idx, id_end_idx))
				}
			}
		}
		
		// Pick the system with the least number of inputs. If there is a tie, pick the last one in the list.
		let (system_id, _, id_start_idx, id_end_idx) = match initial_system_candidate_buff.iter().min_by(|&candidate_x, &candidate_y|
		{
			let (_, system_data_count_x, _, _) = *candidate_x;
			let (_, system_data_count_y, _, _) = *candidate_y;
			
			if system_data_count_x > system_data_count_y  { return Ordering::Greater; }
			else if system_data_count_x < system_data_count_y  { return Ordering::Less; }
			else { return Ordering::Equal }
		})
		{
			Some(init_sys_candiate) => *init_sys_candiate,
			None => continue
		};
		
		// Mark all system data types of the combination as having found an init system
		for orgin_system_data_type_input_id in candidate_systems_data_type_id_buff.get(id_start_idx..id_end_idx).unwrap()
		{
			system_order_info.system_data_type_orgin_system.push(SystemDataTypeSystemId { system_data_type_id: *orgin_system_data_type_input_id, system_id: system_id });
		}
	}
}

struct SystemRunOrderInternalBuffs
{
	system_groups_and_availability_buff: Vec<SystemGroupsAndAvailability>, // Records the number of groups that all of the sys data are have all of the sys data types for 
	// Comb insts that have been outputted by other systems but are not yet ready for use because
	// there are other comb insts groups within the input/output that do not have all of their needed sys data types available
	available_matched_combination_instances_buff: Vec<MatchedCombinationInstance>,
	// When avail matched comb insts become ready they are moved to this buff
	ready_for_run_order_matched_combination_instances_buff: Vec<ReadySystemCombinationGroup>,
	combination_id_and_idxs_pointing_to_used_sys_data_types_buff: Vec<(usize, usize, usize)>, // (comb_id, comb_sys_data_type_start_idx, comb_sys_data_type_end_idx)
	system_data_type_ids_buff: Vec<usize>,
	system_data_type_id_last_output_system_id_buff: Vec<(usize, usize)>, // sys data type id and the last system that output it
	available_system_data_type_ids: Vec<usize>
}

struct ReadySystemCombinationGroup
{
	system_id: usize,
	system_type: SystemType,
	matched_comb_insts_in_group: Vec<MatchedCombinationInstance>,
	input_sys_data_type_id_vec: Vec<usize>
}

struct SystemGroupsAndAvailability
{
	system_id: usize,
	system_input_output: SystemInputOutput,
	number_of_groups: usize,
	number_of_groups_ready: usize
}

#[derive(Clone)]
struct MatchedCombinationInstance
{
	system_id: usize,
	system_type: SystemType, // The system it belongs to type
	instance_input_output: SystemInputOutput, // Is this instance part of an input or output?
	group_id: usize,
	combination_idx: usize, // idx of the used combination in SystemRunOrderInternalBuffs.combination_id_and_idxs_pointing_to_used_sys_data_types_buff
	number_of_system_data_types_in_combination: usize
}

fn determine_non_init_system_run_order<'a>(registered_data: &'a RegisteredData, mut system_order_info: &'a mut SystemOrderInfo)
{
	let mut buffs = SystemRunOrderInternalBuffs 
	{ 
		system_groups_and_availability_buff: Vec::new(),
		available_matched_combination_instances_buff: Vec::new(),
		ready_for_run_order_matched_combination_instances_buff: Vec::new(),
		combination_id_and_idxs_pointing_to_used_sys_data_types_buff: Vec::new(),
		system_data_type_ids_buff: Vec::new(),
		system_data_type_id_last_output_system_id_buff: Vec::new(),
		available_system_data_type_ids: Vec::new()
	};
	
	initialize_and_determine_number_of_groups_each_system_has(registered_data, &mut buffs.system_groups_and_availability_buff);
	
	// Add each orgin record to the last output system buff
	for orgin_rec in system_order_info.system_data_type_orgin_system.iter()
	{
		buffs.system_data_type_id_last_output_system_id_buff.push((orgin_rec.system_data_type_id, orgin_rec.system_id));
		buffs.available_system_data_type_ids.push(orgin_rec.system_data_type_id);
	}
	
	// Find all systems that we have all of their required input systems available and sort
	{
		let ready_component_ids_iter = filter_components(registered_data.registered_system_data_types.iter().cloned())
			.map(|reg_sys_data_type| reg_sys_data_type.id); // All components are always available
		let ready_isd_ids_iter = system_order_info.system_data_type_orgin_system.iter() // But not so for ISDs
		.filter(|orgin_rec| registered_data.registered_system_data_types[orgin_rec.system_data_type_id].data_type == SdtType::Isd)
			.map(|orgin_rec| orgin_rec.system_data_type_id);
		let ready_sys_data_type_ids = ready_component_ids_iter.chain(ready_isd_ids_iter);
			
		add_newly_available_input_system_data_type_ids_to_available_comb_insts_and_update_ready_buff(registered_data, &mut buffs, ready_sys_data_type_ids);
	}
	sort_availaible_systems_by_num_inputs(buffs.ready_for_run_order_matched_combination_instances_buff.as_mut_slice());
	
	let found_system_data_type_ids_for_system_buff = Vec::new();
	
	// Now go through the availaible systems and begin creating the order
	// Using a while loop as buff will likely change size as we iterate
	let mut i = 0;
	while i < buffs.ready_for_run_order_matched_combination_instances_buff.len()
	{
		// Check if we finished processing all of the Both systems, as the InputOnly systems will be behind the Both systems
		// Break out of the loop once we reach the OnlyInput section as we need to process these differently
		//let matched_comb_inst = &buffs.ready_for_run_order_matched_combination_instances_buff[i];
		if buffs.ready_for_run_order_matched_combination_instances_buff[i].system_type == SystemType::OnlyInput
		{
			break;
		}
		
		// j starts at i and goes forward until it finds a system that can currently be run.
		let mut j = i;
		while !currently_have_all_system_input_types_availaible_to_run_system(&buffs, buffs.available_system_data_type_ids.as_slice(), j)
		{
			j += 1;
		}
		
		if j != i
		{
			// Swap the system that is ready with the system at our current pos
			buffs.ready_for_run_order_matched_combination_instances_buff.swap(i, j);
		}
		
		//let ready_sys_comb_groups = &buffs.ready_for_run_order_matched_combination_instances_buff[i];
		
		// Add to order
		direct_previous_system_outputs_to_go_to_system_id(&mut system_order_info, &buffs, found_system_data_type_ids_for_system_buff.iter().cloned(), buffs.ready_for_run_order_matched_combination_instances_buff[i].system_id);
		let sys_id = buffs.ready_for_run_order_matched_combination_instances_buff[i].system_id;
		update_orgin_and_last_used_buff_and_availaible_system_data_types_for_newly_used_system_outputs(registered_data,
			&mut system_order_info, &mut buffs, sys_id);
		add_newly_available_input_system_data_type_ids_to_available_comb_insts_and_update_ready_buff(registered_data, &mut buffs, found_system_data_type_ids_for_system_buff.iter().cloned());
		
		// Don't sort systems that we've already processed
		let num_ready_input_sys_buff_len = buffs.ready_for_run_order_matched_combination_instances_buff.len();
		let ready_input_sys_slice = buffs.ready_for_run_order_matched_combination_instances_buff.get_mut(i + 1..num_ready_input_sys_buff_len).unwrap();
		sort_availaible_systems_by_num_inputs(ready_input_sys_slice);
		
		i += 1;
	}
	
	// We just finished processing all of the Both systems, and all that should remain at this point are the InputOnly systems
	let num_avail_systems = buffs.ready_for_run_order_matched_combination_instances_buff.len(); // Const at this point
	while i < num_avail_systems
	{
		let ready_comb_inst = &buffs.ready_for_run_order_matched_combination_instances_buff[i];		
		direct_previous_system_outputs_to_go_to_system_id(&mut system_order_info, &buffs, ready_comb_inst.input_sys_data_type_id_vec.iter().cloned(), ready_comb_inst.system_id);
		
		i += 1;
	}
}

fn get_unique_system_data_type_id_slice_for_comb_idx_from_internal_buffs<I>(buffs: &SystemRunOrderInternalBuffs, sys_data_type_id_write_buff: &mut Vec<usize>, comb_idxs: I)
	where I: Iterator<Item = usize>
{
	sys_data_type_id_write_buff.clear();
	
	for comb_idx in comb_idxs
	{
		let (_, strt_idx, end_idx) = buffs.combination_id_and_idxs_pointing_to_used_sys_data_types_buff[comb_idx];
		let sys_data_type_ids_iter = buffs.available_system_data_type_ids.get(strt_idx..end_idx).unwrap().into_iter(); // Should never fail!
		sys_data_type_id_write_buff.extend(sys_data_type_ids_iter);
	}
	
	// Remove duplicates
	sys_data_type_id_write_buff.sort();
	sys_data_type_id_write_buff.dedup();
}

fn sort_availaible_systems_by_num_inputs(ready_for_run_order_matched_combination_instances_buff: &mut [ReadySystemCombinationGroup])
{
	// Potentially very slow. Look at later if performance is an issue
	// Sort by the number of inputs taken in
	ready_for_run_order_matched_combination_instances_buff.sort_unstable_by(|ready_sys_comb_groups_a, ready_sys_comb_groups_b|
	{
		// Only two possible types here: Both and OnlyInput
		if ready_sys_comb_groups_a.system_type != ready_sys_comb_groups_b.system_type
		{
			if ready_sys_comb_groups_a.system_type == SystemType::OnlyInput
			{
				return Ordering::Greater;
			}
			else // Both
			{
				return Ordering::Less;
			}
		}
			
		ready_sys_comb_groups_a.input_sys_data_type_id_vec.len().cmp(&ready_sys_comb_groups_b.input_sys_data_type_id_vec.len())
	})
}

fn currently_have_all_system_input_types_availaible_to_run_system(buffs: &SystemRunOrderInternalBuffs, available_system_data_type_ids_slice: &[usize], system_idx_to_run: usize) -> bool
{
	let mut used_sys_data_type_ids_iter = buffs.ready_for_run_order_matched_combination_instances_buff[system_idx_to_run].input_sys_data_type_id_vec.iter();
	
	used_sys_data_type_ids_iter.all(|&used_sys_id|
		 available_system_data_type_ids_slice.into_iter().any(|&avail_sys_id| avail_sys_id == used_sys_id))

}

fn direct_previous_system_outputs_to_go_to_system_id<I>(system_order_info: &mut SystemOrderInfo, buffs: &SystemRunOrderInternalBuffs, system_input_data_type_ids_iter: I, system_id: usize)
	where I: Iterator<Item = usize>
{
	// Set the 
	for sys_data_type_id in system_input_data_type_ids_iter
	{
		let (_, last_sys_id_to_output_data_type) = find_system_id_and_last_idx_that_last_output_system_data_type_id(buffs, sys_data_type_id);
		
		// Record system data type usage
		let usage_instance = SysDataTypeOutputAndIntakeSystemInstance 
		{
			 output_system_id: last_sys_id_to_output_data_type,
			 intake_system_id: system_id,
			 system_data_type_id: sys_data_type_id
		};
		system_order_info.system_output_and_intake_system_vec.push(usage_instance);
	}
}

fn update_orgin_and_last_used_buff_and_availaible_system_data_types_for_newly_used_system_outputs(registered_data: &RegisteredData, system_order_info: &mut SystemOrderInfo, buffs: &mut SystemRunOrderInternalBuffs, system_id: usize)
{
	// Update the last records of the system's outputs to point to this system id
	let output_sys_data_type_ids_iter = get_input_output_system_data_type_ids_for_system(registered_data, system_id, SystemInputOutput::Output);
	
	for out_sys_data_type_id in output_sys_data_type_ids_iter
	{
		let (last_use_idx, _) = find_system_id_and_last_idx_that_last_output_system_data_type_id(buffs, out_sys_data_type_id);
		
		// Update the last output record
		buffs.system_data_type_id_last_output_system_id_buff[last_use_idx] = (out_sys_data_type_id, system_id);
		
		// If an orgin record doesn't exist for this type then add one
		match system_order_info.system_data_type_orgin_system.iter()
			.find(|orgin_rec|
		{
			orgin_rec.system_data_type_id == out_sys_data_type_id
		})
		{
			None => system_order_info.system_data_type_orgin_system.push(SystemDataTypeSystemId { system_data_type_id: out_sys_data_type_id, system_id: system_id } ),
			_ => ()
		}
		
		// Do we already have access to this output? If we don't, add it!
		add_available_system_data_type_id_to_buff_if_previously_unavailable(buffs, out_sys_data_type_id);
	}
}

fn find_system_id_and_last_idx_that_last_output_system_data_type_id(buffs: &SystemRunOrderInternalBuffs, system_data_type_id: usize) -> (usize, usize)
{
	match buffs.system_data_type_id_last_output_system_id_buff.iter().cloned().enumerate()
	.find(|last_use_idx_and_last_out_rec|
	{
		let (_ ,(last_rec_sys_data_type_id, _)) = *last_use_idx_and_last_out_rec;
		system_data_type_id == last_rec_sys_data_type_id
	})
	{
		Some(last_use_idx_and_last_out_rec) => 
		{
			let (last_use_idx, (_, last_sys_id)) = last_use_idx_and_last_out_rec;
			(last_use_idx, last_sys_id)
		},
		None => panic!("ERROR: Failed to find the expected system data type id ({}) while looking for the previous system that output it! Aborting!", system_data_type_id)
	}
}

fn initialize_and_determine_number_of_groups_each_system_has(registered_data: &RegisteredData, system_groups_and_availability_buff: &mut Vec<SystemGroupsAndAvailability>)
{
	fn count_group_usage_if_not_already_counted_for<I>(reg_sys_sys_comb_insts_iter: I,
		num_in_groups: &mut usize, num_out_groups: &mut usize,
		counted_groups_hashset: &mut HashSet<(SystemInputOutput, usize)>)
	where I: Iterator<Item = SystemCombinationUsageInstance>
	{
		for reg_sys_sys_comb_inst in reg_sys_sys_comb_insts_iter
		{
			if counted_groups_hashset.contains(&(reg_sys_sys_comb_inst.system_input_output.clone(), reg_sys_sys_comb_inst.group_id))
			{
				continue;
			}
			
			if reg_sys_sys_comb_inst.system_input_output == SystemInputOutput::Input
			{
				*num_in_groups = *num_in_groups + 1;
			}
			else // Output
			{
				*num_out_groups = *num_out_groups + 1;
			}
			counted_groups_hashset.insert((reg_sys_sys_comb_inst.system_input_output, reg_sys_sys_comb_inst.group_id));
		}
	}
	
	fn add_record_for_inp_out_groups_if_exists(sys_id: usize, sys_inp_out: SystemInputOutput,
		num_groups: usize, system_groups_and_availability_buff: &mut Vec<SystemGroupsAndAvailability>)
	{
		if num_groups > 0
		{
			let system_groups_availibility = SystemGroupsAndAvailability
			{
				system_id: sys_id,
				system_input_output: sys_inp_out,
				number_of_groups: num_groups,
				number_of_groups_ready: 0
			};
			system_groups_and_availability_buff.push(system_groups_availibility);
		}
	}
	
	let mut counted_groups_hashset = HashSet::new();
	for reg_sys in registered_data.registered_systems.iter()
	{
		counted_groups_hashset.clear();
		let reg_sys_sys_comb_insts_iter = get_system_combination_instances_used_by_system(registered_data, reg_sys.id);
		let (mut num_in_groups, mut num_out_groups) = (0, 0);

		count_group_usage_if_not_already_counted_for(reg_sys_sys_comb_insts_iter, &mut num_in_groups, &mut num_out_groups, &mut counted_groups_hashset);
		
		add_record_for_inp_out_groups_if_exists(reg_sys.id, SystemInputOutput::Input, num_in_groups, system_groups_and_availability_buff);
		add_record_for_inp_out_groups_if_exists(reg_sys.id, SystemInputOutput::Output, num_out_groups, system_groups_and_availability_buff);
	}
}

fn add_newly_available_input_system_data_type_ids_to_available_comb_insts_and_update_ready_buff<I>(registered_data: &RegisteredData,
	buffs: &mut SystemRunOrderInternalBuffs, input_data_type_ids_using_system_data_iter: I)
	where I: Iterator<Item = usize>
{
	fn add_sys_data_type_ids_used_by_comb_to_comb_sys_ids_buff(registered_data: &RegisteredData, buffs: &mut SystemRunOrderInternalBuffs, comb_id: usize) -> (usize, usize)
	{
		let comb_sys_data_type_id_iter = get_system_data_type_ids_used_by_combination(registered_data, comb_id);
		let comb_sys_data_type_start_idx = buffs.system_data_type_ids_buff.len();
		buffs.system_data_type_ids_buff.extend(comb_sys_data_type_id_iter);
		let comb_sys_data_type_end_idx = buffs.system_data_type_ids_buff.len();
		
		(comb_sys_data_type_start_idx, comb_sys_data_type_end_idx)
	}
	
	fn add_comb_id_and_start_end_idxs_to_comb_lookup(buffs: &mut SystemRunOrderInternalBuffs, comb_id: usize, comb_sys_data_type_start_idx: usize, comb_sys_data_type_end_idx: usize) -> (usize, usize)
	{
		let comb_idx = buffs.combination_id_and_idxs_pointing_to_used_sys_data_types_buff.len();
		buffs.combination_id_and_idxs_pointing_to_used_sys_data_types_buff.push((comb_id, comb_sys_data_type_start_idx, comb_sys_data_type_end_idx));
		let num_data_types_in_comb = comb_sys_data_type_end_idx - comb_sys_data_type_start_idx;
		
		(comb_idx, num_data_types_in_comb)
	}
	
	fn update_ready_system_groups_and_get_vec_of_new_system_ids_that_are_now_able_to_be_added_to_the_run_order
	<'a, I>(system_groups_and_availability_buff: &mut Vec<SystemGroupsAndAvailability>, newly_available_input_matched_combs_insts_iter: I) -> Vec<usize>
	where I: Iterator<Item = &'a MatchedCombinationInstance>
	{
		let mut input_system_ids_that_are_ready_for_run_order_vec = Vec::new();
		for newly_available_input_matched_combs_inst in newly_available_input_matched_combs_insts_iter
		{
			let mut sys_group_and_availability = system_groups_and_availability_buff.iter_mut()
				.find(|sys_group_and_avail| sys_group_and_avail.system_id == newly_available_input_matched_combs_inst.system_id).unwrap();
			sys_group_and_availability.number_of_groups_ready += 1;
			
			if sys_group_and_availability.number_of_groups == sys_group_and_availability.number_of_groups_ready
			{
				input_system_ids_that_are_ready_for_run_order_vec.push(sys_group_and_availability.system_id);
			}
		}
		
		input_system_ids_that_are_ready_for_run_order_vec
	}
	
	fn move_now_ready_matched_comb_insts_from_available<I>(buffs: &mut SystemRunOrderInternalBuffs, newly_ready_sys_ids_iter: I)
		where I: Iterator<Item = usize>
	{
		for ready_sys_id in newly_ready_sys_ids_iter
		{
			let mut input_sys_data_type_id_write_buff = Vec::new();
			let idxs_of_matched_comb_insts_using_sys_id_vec: Vec<usize> = buffs.available_matched_combination_instances_buff.iter().enumerate()
			.filter(|&idx_and_matched_comb_inst|
			{
				let (_, matched_comb_inst) = idx_and_matched_comb_inst;
				matched_comb_inst.system_id == ready_sys_id
			})
			.map(|idx_and_matched_comb_inst|
			{
				let (idx, _) = idx_and_matched_comb_inst;
				idx
			}).collect();
			
			
			let mut ready_for_run_order_matched_combination_instances_buff = Vec::new();
			for avail_idx in idxs_of_matched_comb_insts_using_sys_id_vec
			{
				let avail_mathed_comb_inst = buffs.available_matched_combination_instances_buff.swap_remove(avail_idx);
				ready_for_run_order_matched_combination_instances_buff.push(avail_mathed_comb_inst);
			}
			
			{
				let comb_idxs_iter = ready_for_run_order_matched_combination_instances_buff.iter()
					.map(|ready_for_run_order_matched_combination_instance| ready_for_run_order_matched_combination_instance.combination_idx);
				get_unique_system_data_type_id_slice_for_comb_idx_from_internal_buffs(buffs, &mut input_sys_data_type_id_write_buff, comb_idxs_iter);
			}
			
			let ready_sys_comb_group = ReadySystemCombinationGroup
			{
				system_id: ready_for_run_order_matched_combination_instances_buff[0].system_id,
				system_type: ready_for_run_order_matched_combination_instances_buff[0].system_type.clone(),
				matched_comb_insts_in_group: ready_for_run_order_matched_combination_instances_buff,
				input_sys_data_type_id_vec: input_sys_data_type_id_write_buff
			};
			buffs.ready_for_run_order_matched_combination_instances_buff.push(ready_sys_comb_group);
		}
	}
	
	let available_matched_combination_instances_buff_initial_size = buffs.available_matched_combination_instances_buff.len();
	let mut comb_ids_using_sys_data_iter_buff = Vec::new();
	for sys_data_type_id in input_data_type_ids_using_system_data_iter
	{
		comb_ids_using_sys_data_iter_buff.clear();
		let comb_ids_using_sys_data_type_iter = get_all_combination_ids_that_use_a_system_data_type(registered_data, sys_data_type_id);
		comb_ids_using_sys_data_iter_buff.extend(comb_ids_using_sys_data_type_iter);
		let sys_comb_insts_using_sys_data_type_iter = get_system_combination_instances_for_combination_ids(registered_data, comb_ids_using_sys_data_iter_buff.iter().cloned());
		let input_comb_ids_using_sys_data_type_iter = filter_system_combination_instances_by_type(sys_comb_insts_using_sys_data_type_iter, SystemInputOutput::Input)
			.map(|sys_comb_inst| sys_comb_inst.combination_id);
		
		for comb_id in input_comb_ids_using_sys_data_type_iter
		{
			match buffs.combination_id_and_idxs_pointing_to_used_sys_data_types_buff.iter().cloned().enumerate()
				.find(|&comb_idx_and_comb_rec|
				{
					let (_, (already_found_comb_id, _, _)) = comb_idx_and_comb_rec;
					comb_id == already_found_comb_id
				})
			{
				None =>
				{
					let (comb_sys_data_type_start_idx, comb_sys_data_type_end_idx) = add_sys_data_type_ids_used_by_comb_to_comb_sys_ids_buff(registered_data, buffs, comb_id);
					let (comb_idx, num_data_types_in_comb) = add_comb_id_and_start_end_idxs_to_comb_lookup(buffs, comb_id, comb_sys_data_type_start_idx, comb_sys_data_type_end_idx);
					
					let sys_ids_using_comb_as_inp_iter = get_sys_comb_insts_with_comb_id(registered_data, comb_id);
					let matched_sys_comb_inst_iter = sys_ids_using_comb_as_inp_iter
						.map(|sys_comb_inst| 
					{
						let sys_type = registered_data.registered_systems[sys_comb_inst.system_id].system_type.clone();
						MatchedCombinationInstance
						{
							system_id: sys_comb_inst.system_id,
							system_type: sys_type,
							instance_input_output: SystemInputOutput::Input,
							group_id: sys_comb_inst.group_id,
							combination_idx: comb_idx,
							number_of_system_data_types_in_combination: num_data_types_in_comb
						}
					});
					buffs.available_matched_combination_instances_buff.extend(matched_sys_comb_inst_iter);
				},
				_ => ()
			}
		}
		
		let num_new_additions = buffs.available_matched_combination_instances_buff.len() - available_matched_combination_instances_buff_initial_size;
		if num_new_additions > 0
		{
			let newly_ready_sys_ids_vec = update_ready_system_groups_and_get_vec_of_new_system_ids_that_are_now_able_to_be_added_to_the_run_order(
				&mut buffs.system_groups_and_availability_buff, buffs.available_matched_combination_instances_buff
				.get(available_matched_combination_instances_buff_initial_size..buffs.available_matched_combination_instances_buff.len()).unwrap().into_iter());
			move_now_ready_matched_comb_insts_from_available(buffs, newly_ready_sys_ids_vec.into_iter());
		}
	}
}

fn add_available_system_data_type_id_to_buff_if_previously_unavailable(buffs: &mut SystemRunOrderInternalBuffs, output_system_data_type_id: usize)
{
	match buffs.available_system_data_type_ids.iter()
		.find(|&type_id|
	{
		*type_id == output_system_data_type_id
	})
	{
		None => buffs.available_system_data_type_ids.push(output_system_data_type_id),
		_ => ()
	}
}

fn create_orgin_records_of_data_type_ids_for_system_ids<'a, I: 'a>(registered_data: &'a RegisteredData<'a>, system_id_iter: I, input_output_filter: SystemInputOutput)
 -> impl Iterator<Item = SystemDataTypeSystemId> + 'a where I: Iterator<Item = usize>
{
	system_id_iter.flat_map(move |system_id|
	{
		let sys_comb_inst_iter = get_system_combination_instances_used_by_system(registered_data, system_id);
		let filtered_sys_comb_inst_iter = filter_system_combination_instances_by_type(sys_comb_inst_iter, input_output_filter.clone());
		// Should only be at most one combination in the iter at this point	
		let combination_ids_iter = filtered_sys_comb_inst_iter.map(|sys_comb_inst| sys_comb_inst.combination_id);
		
		combination_ids_iter.flat_map(move |combination_id|
		{
			get_system_data_type_ids_used_by_combination(registered_data, combination_id)
				.map(move |data_type_id| SystemDataTypeSystemId { system_data_type_id: data_type_id, system_id: system_id })
		})
	})
}

fn system_data_does_not_have_orgin_systems<'a, Iid>(mut system_data_type_id_iter: Iid, data_type_orgin_systems_slice: &[SystemDataTypeSystemId]) -> bool
	where Iid: Iterator<Item = &'a usize>
{
	system_data_type_id_iter.all(|&system_data_type_id|
		{
			data_type_orgin_systems_slice.into_iter().all(|orgin_system| orgin_system.system_data_type_id != system_data_type_id)
		}
	)
}

