// Stainless is no longer maintained.
// Need to later move to a new testing framework.


// /* TODO Tests:
// 	- 
// */

// #![feature(plugin)]
// #![feature(proc_macro)]
// #![cfg_attr(test, plugin(stainless))]

// extern crate pecs_proc_macros;
// extern crate pecs_common;
// extern crate yaml_rust;

// mod common;

// use pecs_proc_macros::register_systems_and_system_data_types;
// use yaml_rust::Yaml;

// use pecs_common::util_functions::load_in_yaml;
// use pecs_common::types::{RegisteredData, RegisteredSystem, RegisteredCombination, RegisteredSystemDataType, SystemCombinationUsageInstance, SystemDataTypeCombinationUsageInstance, SystemDataType, SystemType, SystemInputOutput};
// use pecs_common::specialized_collections::system_sdt_containers::SubVecsSystemContainer;

// use std::collections::{HashSet, HashMap};
// use std::iter;

// use common::types_for_tests::*;

// const SIMPLE_PECS_REGISTRATION_YAML_PATH: &'static str = "tests/simple_registration_test.yaml";

// describe! registrition_data_validity
// {
	
// 	before_each
// 	{
// 		// Using a hardcoded copy of the string until I can figure out how to expand a variable usage inside a proc macro.
// 		register_systems_and_system_data_types!("tests/simple_registration_test.yaml");
		
// 		#[allow(unused_variables)]
// 		let yaml = load_in_yaml(SIMPLE_PECS_REGISTRATION_YAML_PATH).unwrap();
// 		#[allow(unused_variables)]
// 		let registered_data = get_registered_data();
// 	}
	
// 	it "should map an id to each system data type name"
// 	{
// 		let system_data_types_yaml = &yaml["SystemDataTypes"];
// 		let component_names_vec = collect_string_fields_from_yaml_vec(&system_data_types_yaml["Components"], "Name");
// 		let isd_names_vec = collect_string_fields_from_yaml_vec(&system_data_types_yaml["ISDs"], "Name");
		
// 		let mut system_data_type_names_iter = component_names_vec.into_iter().chain(isd_names_vec.into_iter());
// 		let found = system_data_type_names_iter
// 		.all(|system_data_type_name|
// 		{
// 			registered_data.registered_system_data_types.iter()
// 			.any(|sys_data_type| sys_data_type.name == system_data_type_name)
// 		});
		
// 		assert!(found)
// 	}

// 	it "should map a unique id to each system data type"
// 	{
// 		let id_vec = registered_data.registered_system_data_types.iter().map(|sys_data_type| sys_data_type.id).collect();
// 		assert!(all_ids_unique(id_vec))
// 	}
	
// 	it "should map a unique id to each combination"
// 	{
// 		let id_vec = registered_data.registered_combinations.iter().map(|combination| combination.id).collect();
// 		assert!(all_ids_unique(id_vec))
// 	}
	
// 	it "should map an id to each system name"
// 	{
// 		let systems_yaml = &yaml["Systems"];
// 		let system_names_vec = collect_string_fields_from_yaml_vec(&systems_yaml, "Name");
		
// 		let found = system_names_vec.iter()
// 		.all(|system_name|
// 		{
// 			registered_data.registered_systems.iter()
// 			.any(|sys| sys.name == system_name)
// 		});
		
// 		assert!(found)
// 	}
	
// 	it "should map a unique id to each system"
// 	{
// 		let id_vec = registered_data.registered_systems.iter().map(|sys| sys.id).collect();
// 		assert!(all_ids_unique(id_vec))
// 	}
	
// 	it "should create a combination usage instance for each input/output used by each system"
// 	{
// 		let systems_yaml = &yaml["Systems"];
// 		let systems_yaml_vec = systems_yaml.as_vec().unwrap();
		
// 		let system_name_to_id_hash = create_hashmap_of_system_names_to_ids(&registered_data);
// 		let mut sys_id_and_expected_instances = HashMap::new();
		
// 		for system in systems_yaml_vec
// 		{
// 			let name = system["Name"].as_str().unwrap();
// 			let mut expected_usage_instances: i32 = 0;
// 			if !system["Input"].is_badvalue() { expected_usage_instances += 1; }
// 			if !system["Output"].is_badvalue() { expected_usage_instances += 1; }
			
// 			let sys_instance_id = system_name_to_id_hash[name];
// 			sys_id_and_expected_instances.insert(sys_instance_id, expected_usage_instances);
// 		}
		
// 		for sys_instance_id in registered_data.system_combination_usage_instances.iter().map(|instance| instance.system_id)
// 		{
// 			let number_of_occurances_left = sys_id_and_expected_instances.get_mut(&sys_instance_id).unwrap();
// 			*number_of_occurances_left = *number_of_occurances_left - 1;
// 		}
		
// 		for expected_remaining_occurances_left in sys_id_and_expected_instances.values()
// 		{
// 			assert_eq!(*expected_remaining_occurances_left, 0)
// 		}
// 	}
	
// 	it "should create system data type usage instances that all have valid ids"
// 	{
// 		let system_data_type_ids_iter = registered_data.registered_system_data_types.iter()
// 		.map(|sys_data_type| sys_data_type.id);
// 		let mut id_to_name_hash = HashSet::with_capacity(registered_data.system_data_type_usage_instances.len());
// 		id_to_name_hash.extend(system_data_type_ids_iter);
		
// 		for system_data_type_usage in registered_data.system_data_type_usage_instances.iter()
// 		{
// 			assert!(id_to_name_hash.contains(&system_data_type_usage.system_data_type_id),
// 				format!("Found id {} which belongs to no system data type. \
// 				\n Registered system data types: {:#?}
// 				\n system data type combination instances: {:#?}"
// 				, system_data_type_usage.system_data_type_id, registered_data.registered_system_data_types, registered_data.system_data_type_usage_instances));
// 		}
// 	}
	
// 	it "should create a usage instance for each time a system data type is used by a combination"
// 	{	
// 		let systems_data_type_yaml = &yaml["SystemDataTypes"];
// 		let component_names_vec = collect_string_fields_from_yaml_vec(&systems_data_type_yaml["Components"], "Name");
// 		let isd_names_vec = collect_string_fields_from_yaml_vec(&systems_data_type_yaml["ISDs"], "Name");
// 		let sys_data_type_names_iter = component_names_vec.iter().chain(isd_names_vec.iter());
		
// 		let systems_yaml = &yaml["Systems"];
// 		let (input_system_data_type_usages, output_system_data_type_usages) =
// 			collect_input_output_fields_from_systems_vec(&systems_yaml);
// 		let (first_sys_name, _) = input_system_data_type_usages[0].clone();
// 		let all_system_data_type_usages_iter = input_system_data_type_usages.into_iter()
// 			.chain(output_system_data_type_usages.into_iter().chain(iter::once((String::new(), String::new())))); // Dummy element to force the last sys data type in the loop to get counted.
// 		let mut found_combination_names: HashSet<String> = HashSet::new();
		
// 		let mut yaml_system_data_type_usage_manual_counts_hash = HashMap::new();
// 		for system_data_type_name in sys_data_type_names_iter
// 		{
// 			yaml_system_data_type_usage_manual_counts_hash.insert(system_data_type_name.as_str(), 0);
// 		}
		
// 		let mut sys_data_type_name_buff = Vec::new();
// 		let mut last_seen_system_name = first_sys_name;
// 		for (system_name, system_data_type_name) in all_system_data_type_usages_iter
// 		{
// 			if last_seen_system_name == system_name
// 			{
// 				sys_data_type_name_buff.push(system_data_type_name);
// 				continue;
// 			}
			
// 			let comb_name = determine_comb_name_from_comb_sys_data_type_names(&mut sys_data_type_name_buff);
			
// 			if !found_combination_names.contains(&comb_name)
// 			{
// 				for system_data_type_name in sys_data_type_name_buff.iter()
// 				{
// 					let mut found = true;
// 					match yaml_system_data_type_usage_manual_counts_hash.get_mut(system_data_type_name.as_str())
// 					{
// 						Some(usage_count) => *usage_count = *usage_count + 1,
// 						None =>
// 						{
// 							found = false;
// 						}
// 					}
					
// 					if !found
// 					{
// 						assert!(false, "Found a system using a system data type name when manually parsing the \
// 							.yaml (part of the test) that was not a registered system data type. (System \"{}\" \
// 							was using system data type \"{}\". \
// 							\n Parsed .yaml system data types: {:#?}", system_name, system_data_type_name, yaml_system_data_type_usage_manual_counts_hash.keys());
// 					}
// 				}
				
// 				found_combination_names.insert(comb_name);
// 			}
			
// 			last_seen_system_name = system_name;
// 			sys_data_type_name_buff.clear();
// 			sys_data_type_name_buff.push(system_data_type_name);
// 		}
		
// 		// Now count the actual occurances in registered_data
// 		let registered_system_data_type_names_to_id_hash = create_hashmap_of_system_data_type_names_to_ids(&registered_data);
// 		let mut registered_yaml_system_data_type_usage_manual_counts_hash = HashMap::new();
// 		for registered_system in registered_data.registered_system_data_types.iter()
// 		{
// 			registered_yaml_system_data_type_usage_manual_counts_hash.insert(registered_system.id, 0);
// 		}
		
// 		for system_data_type_usage_instance in registered_data.system_data_type_usage_instances.iter()
// 		{
// 			match registered_yaml_system_data_type_usage_manual_counts_hash.get_mut(&system_data_type_usage_instance.system_data_type_id)
// 			{
// 				Some(usage_count) => *usage_count = *usage_count + 1,
// 				None => assert!(false, "This should not be reachable!!")
// 			}
// 		}
		
// 		for (sys_data_type_name, count) in yaml_system_data_type_usage_manual_counts_hash.iter()
// 		{
// 			let id_key = registered_system_data_type_names_to_id_hash[sys_data_type_name];
// 			assert!(*count == registered_yaml_system_data_type_usage_manual_counts_hash[&id_key],
// 				format!("System data type name that had an incorrect number of usages: {} (usages: {}, expected: {}) \
// 				\n System data type id: {} \
// 				\n Manual data counts: {:#?} \
// 				\n Actual registered data counts: {:#?} \
// 				\n Registered data system data types combination instances: {:#?}",
// 				sys_data_type_name, registered_yaml_system_data_type_usage_manual_counts_hash[&id_key], count, id_key, yaml_system_data_type_usage_manual_counts_hash,
// 				registered_yaml_system_data_type_usage_manual_counts_hash, registered_data.system_data_type_usage_instances))
// 		}
// 	}
	
// 	it "should correctly assign system types"
// 	{
// 		let systems_yaml = &yaml["Systems"];
		
// 		for system_yaml in systems_yaml.as_vec().unwrap()
// 		{		
// 			let sys_name = system_yaml["Name"].as_str().unwrap();
// 			let contains_inputs = !system_yaml["Input"].is_badvalue();
// 			let contains_outputs = !system_yaml["Output"].is_badvalue();
			
// 			let expected_sys_type = match (contains_inputs, contains_outputs)
// 			{
// 				(false, false) => SystemType::NoInputOrOutput,
// 				(true, false) => SystemType::OnlyInput,
// 				(false, true) => SystemType::OnlyOutput,
// 				(true, true) => SystemType::Both
// 			};
			
// 			let reg_sys = registered_data.registered_systems.iter()
// 				.find(|&sys| sys.name == sys_name).unwrap();
// 			assert_eq!(reg_sys.system_type, expected_sys_type, "Expected System \"{}\" to have a system type of {:?}, found {:?}.", sys_name, expected_sys_type, reg_sys.system_type)
// 		}
// 	}
// // Test is broken and very hard to read. Will come back later to fix (or totally rewrite...).
// //	it "should create a combination for each unique combination of system data types used by systems"
// //	{		
// //		let systems_yaml = &yaml["Systems"];
// //		let (input_system_data_type_usages_vec, output_system_data_type_usages_vec) =
// //			collect_input_output_fields_from_systems_vec(&systems_yaml);
// //
// //		let mut combination_names_and_system_names_that_use_them_hash: HashMap<String, Vec<String>> = HashMap::new();
// //		{
// //			
// //			let mut sys_data_type_name_buff: Vec<String> = Vec::new();
// //			let mut find_all_combinations_and_record_systems_that_use_them_func = |system_name_and_input_or_output_names_vec: &Vec<(String, String)>|
// //			{
// //				// TODO: Since these two vectors both will have each system's elements consecutavely, we can iterate (map) over each element
// //				// until the system name changes. Once it changes, then we concat the system data type name, and add the system name to the
// //				// value array in the hash.
// //				
// //				fn insert_system_data_type_names_in_buff_to_hash(system_name: &str, sys_data_type_name_buff: &mut Vec<String>,
// //					combination_names_and_system_names_that_use_them_hash: &mut HashMap<String, Vec<String>>)
// //				{
// //					let combination_key = determine_comb_name_from_comb_sys_data_type_names(sys_data_type_name_buff);
// //					
// //					let mut entry = combination_names_and_system_names_that_use_them_hash.entry(combination_key).or_insert(Vec::new());
// //					entry.push(system_name.to_owned());
// //						
// //					sys_data_type_name_buff.clear();
// //				};
// //				
// //				let mut last_seen_name = String::new();
// //				for &(ref system_name, ref system_data_type_name) in system_name_and_input_or_output_names_vec.into_iter()
// //				{
// //					if last_seen_name != *system_name
// //					{
// //						insert_system_data_type_names_in_buff_to_hash(system_name.as_str(), &mut sys_data_type_name_buff, &mut combination_names_and_system_names_that_use_them_hash);
// //						last_seen_name = system_name.to_owned();
// //					}
// //					
// //					sys_data_type_name_buff.push(system_data_type_name.to_owned());
// //				}
// //				insert_system_data_type_names_in_buff_to_hash(last_seen_name.as_str(), &mut sys_data_type_name_buff, &mut combination_names_and_system_names_that_use_them_hash);
// //			};
// //			
// //			find_all_combinations_and_record_systems_that_use_them_func(&input_system_data_type_usages_vec);
// //			find_all_combinations_and_record_systems_that_use_them_func(&output_system_data_type_usages_vec);
// //		}
// //	
// //		let system_name_to_id_hash = create_hashmap_of_system_names_to_ids(&registered_data);
// //		let sys_name_vecs_that_use_each_comb_iter = combination_names_and_system_names_that_use_them_hash.values();
// //		let sys_id_vecs_that_use_each_comb_vec: Vec<Vec<usize>> = sys_name_vecs_that_use_each_comb_iter
// //			.map(|sys_name_vec| sys_name_vec.into_iter().map(|sys_name| system_name_to_id_hash[sys_name.as_str()]).collect()).collect();
// //		
// //		let mut comb_insts_that_involve_sys_ids_buff = Vec::new();
// //		let mut unique_comb_ids_buff = Vec::new();
// //		for sys_id_vec in sys_id_vecs_that_use_each_comb_vec
// //		{
// //			// Get all system combination instances that are used by any of the system ids
// //			let comb_insts_that_involve_sys_ids_iter = registered_data.system_combination_usage_instances.iter()
// //			.filter(|comb_inst|
// //			{
// //				sys_id_vec.iter().any(|sys_id| comb_inst.system_id == *sys_id)
// //			});
// //			
// //			unique_comb_ids_buff.clear();
// //			comb_insts_that_involve_sys_ids_buff.clear();
// //			comb_insts_that_involve_sys_ids_buff.extend(comb_insts_that_involve_sys_ids_iter);
// //			unique_comb_ids_buff.extend(comb_insts_that_involve_sys_ids_buff.iter().map(|comb_inst| comb_inst.combination_id));
// //			
// //			unique_comb_ids_buff.sort_unstable();
// //			unique_comb_ids_buff.dedup();
// //			// Now we need to filter out any combination ids that are not used by all systems.
// //			let comb_ids_that_all_sys_use_sys_ids_iter = unique_comb_ids_buff.iter()
// //				.filter(|&&comb_id| sys_id_vec.iter()
// //					.all(|&sys_id| comb_insts_that_involve_sys_ids_buff.iter()
// //						.any(|comb_inst| comb_inst.combination_id == comb_id && comb_inst.system_id == sys_id))).cloned();
// //			
// //			// Assert that there is exactly one combination id that has records for all of the systems that we found should be used by the combination.
// //			let comb_ids_that_all_sys_use_sys_ids_vec: Vec<usize> = comb_ids_that_all_sys_use_sys_ids_iter.collect();
// //			let num_combs_found = comb_ids_that_all_sys_use_sys_ids_vec.len();
// //			
// //			assert_eq!(num_combs_found , 1,
// //				"Found {} combinations that share the same system data types. \
// //				\n Combination ids that use the same system data types: {:#?} \
// //				\n System data type combination instances: {:#?}",
// //				num_combs_found, comb_ids_that_all_sys_use_sys_ids_vec, registered_data.system_data_type_usage_instances);
// //		}
// //	}
// }

// fn all_ids_unique(id_vec: Vec<usize>) -> bool
// {
// 	id_vec.iter()
// 	.all(|id| 
// 	{
// 		let num_ids_found = id_vec.iter()
// 			.fold(0, |num_ids_found, id_fold|
// 		{
// 			if id == id_fold { num_ids_found + 1 }
// 			else { num_ids_found }
// 		});
			
// 		num_ids_found == 1
// 	})
// }

// fn create_hashmap_of_system_names_to_ids<'a>(registered_data: &RegisteredData<'a>) -> HashMap<&'a str, usize>
// {
// 	let mut system_name_to_id_hash = HashMap::new();
// 	for system in registered_data.registered_systems.iter()
// 	{
// 		system_name_to_id_hash.insert(system.name, system.id);
// 	}
// 	system_name_to_id_hash
// }

// fn create_hashmap_of_system_data_type_names_to_ids<'a>(registered_data: &RegisteredData<'a>) -> HashMap<&'a str, usize>
// {
// 	let mut system_data_type_name_to_id_hash = HashMap::new();
// 	for system_data_type in registered_data.registered_system_data_types.iter()
// 	{
// 		system_data_type_name_to_id_hash.insert(system_data_type.name, system_data_type.id);
// 	}
// 	system_data_type_name_to_id_hash
// }


// fn collect_string_fields_from_yaml_vec(yaml: &Yaml, vec_field_name: &str) -> Vec<String>
// {
// 	let extract_string_closure = |yaml_item: &Yaml| iter::once(yaml_item[vec_field_name].as_str().unwrap().to_string());
// 	collect_fields_from_yaml_vec(yaml, extract_string_closure)
// }

// fn collect_input_output_fields_from_systems_vec(systems_yaml: &Yaml) -> (Vec<(String, String)>, Vec<(String, String)>)
// {
// 	let mut input_names_vec = Vec::new();
// 	let mut output_names_vec = Vec::new();
	
// 	let extract_strings_closure = |system_name: &str, system_input_or_output: &Yaml, string_vec_to_append_to: &mut Vec<(String, String)>|
// 	{
// 		if let Some(yaml_vec) = system_input_or_output.as_vec()
// 		{
// 			let string_iter = yaml_vec.iter().map(|yaml_item| (system_name.to_string(), yaml_item.as_str().unwrap().to_string()));
// 			string_vec_to_append_to.extend(string_iter);
// 		}
// 	};
	
// 	for system_yaml in systems_yaml.as_vec().unwrap()
// 	{
// 		let system_name = system_yaml["Name"].as_str().unwrap();
// 		extract_strings_closure(&system_name, &system_yaml["Input"], &mut input_names_vec);
// 		extract_strings_closure(&system_name, &system_yaml["Output"], &mut output_names_vec);
// 	}
	
// 	(input_names_vec, output_names_vec)
// }

// fn collect_fields_from_yaml_vec<T, F, I>(yaml: &Yaml, extract_field_closure: F) -> Vec<T>
// 	where F: Fn(&Yaml) -> I, I: Iterator<Item = T>
// {
// 	let vec = yaml.as_vec().unwrap();
// 	let collected_fields = vec.iter()
// 		.flat_map(extract_field_closure).collect();
// 	collected_fields
// }

// fn determine_comb_name_from_comb_sys_data_type_names(sys_data_type_name_buff: &mut Vec<String>) -> String
// {
// 	sys_data_type_name_buff.sort();
// 	sys_data_type_name_buff.iter().flat_map(|name| name.chars()).collect()
// }